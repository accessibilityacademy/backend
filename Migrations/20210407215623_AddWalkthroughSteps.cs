﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AccessibilityAcademy.Migrations
{
    public partial class AddWalkthroughSteps : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LearningMaterial",
                table: "Exercises",
                type: "longtext",
                nullable: false);

            migrationBuilder.CreateTable(
                name: "WalkthroughStep",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(type: "longtext", nullable: true),
                    Step = table.Column<string>(type: "longtext", nullable: false),
                    ExerciseId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WalkthroughStep", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WalkthroughStep_Exercises_ExerciseId",
                        column: x => x.ExerciseId,
                        principalTable: "Exercises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WalkthroughStep_ExerciseId",
                table: "WalkthroughStep",
                column: "ExerciseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WalkthroughStep");

            migrationBuilder.DropColumn(
                name: "LearningMaterial",
                table: "Exercises");
        }
    }
}
