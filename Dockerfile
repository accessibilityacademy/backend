FROM mcr.microsoft.com/dotnet/sdk:5.0.300 AS base

RUN groupadd -r user && useradd -m -g user user
RUN mkdir /app && chown user:user /app
USER user
ENV PATH="${PATH}:/home/user/.dotnet/tools"
WORKDIR /app

# COPY --chown=user:user Directory.Build.props ./
COPY --chown=user:user AccessibilityAcademy.csproj ./
RUN dotnet restore "AccessibilityAcademy.csproj"

# https://github.com/GoogleContainerTools/kaniko/issues/1524
RUN mkdir -p Apps/ExerciseBuilder 

COPY --chown=user:user Apps/ExerciseBuilder/AccessibilityAcademy.ExerciseBuilder.csproj ./Apps/ExerciseBuilder/
RUN dotnet restore ./Apps/ExerciseBuilder/AccessibilityAcademy.ExerciseBuilder.csproj \
    && dotnet tool install --global dotnet-ef



FROM base as dev
USER root
RUN apt-get update \
    && apt-get install -y wget gnupg \
    && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
    && apt-get update \
    && apt-get install -y google-chrome-stable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst fonts-freefont-ttf libxss1 \
        # Required for dotnet watch
        procps \
        \
      --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*
COPY --chown=user:user . .

USER user
RUN cd /app/Apps/ExerciseBuilder/ \
    && dotnet pack \
    && cd /app \
    && dotnet tool install --global --add-source ./Apps/ExerciseBuilder/build AccessibilityAcademy.ExerciseBuilder

RUN dotnet eb run --input ./Exercises

USER root
ENTRYPOINT ["/app/dev-entrypoint.sh"]
CMD ["dotnet watch run --no-restore"]



FROM base AS build-prod
COPY --chown=user:user . .
RUN dotnet publish "AccessibilityAcademy.csproj" -c Release -o /app/publish \
    && cd ./Apps/ExerciseBuilder \
    && dotnet publish "AccessibilityAcademy.ExerciseBuilder.csproj" -c Release -o /app/publish/ExerciseBuilder



FROM mcr.microsoft.com/dotnet/aspnet:5.0.6 AS prod

USER root
RUN apt-get update \
    && apt-get install -y wget gnupg \
    && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
    && apt-get update \
    && apt-get install -y google-chrome-stable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst fonts-freefont-ttf libxss1 \
      \
      --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*

RUN groupadd user && useradd -m -g user user
RUN mkdir /app && chown user:user /app
USER user
WORKDIR /app


#chown is required due to a bug in kaniko builder
COPY --from=build-prod --chown=user:user /app/publish .
COPY --from=build-prod --chown=user:user /app/Exercises ./Exercises

CMD ["dotnet", "AccessibilityAcademy.dll"]
