using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccessibilityAcademy.Data;
using AccessibilityAcademy.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using Microsoft.AspNetCore.HttpOverrides;

using PuppeteerSharp;
using AccessibilityAcademy.Helpers;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.DataProtection;

namespace AccessibilityAcademy
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            _env = env;
        }

        public IConfiguration Configuration { get; }
        private readonly IWebHostEnvironment _env;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
                options.KnownProxies.Add(Dns.GetHostEntry("nginx").AddressList[0]);
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "AccessibilityAcademy", Version = "v1" });
            });

            services.AddDataProtection()
                .PersistKeysToDbContext<DatabaseContext>();

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.Cookie.Path = "/";
                    options.Events = new CookieAuthenticationEvents()
                    {
                        OnRedirectToLogin = ctx =>
                        {
                            ctx.Response.StatusCode = (int) HttpStatusCode.Unauthorized;
                            return Task.FromResult(0);
                        }
                    };
                });

            if(Configuration["DB_PASSWORD"] == null || Configuration["DB_DATABASE"] == null)
            {
                throw new Exception("Missing environment variables");
            }

            services.AddDbContext<DatabaseContext>(
                dbContextOptions => dbContextOptions.UseMySql(
                    $"server=mysql;user=root;password={Configuration["DB_PASSWORD"]};database={Configuration["DB_DATABASE"]}",
                    new MySqlServerVersion(new Version(8, 0, 22))
                )
                .EnableSensitiveDataLogging()
                .EnableDetailedErrors()
            );

            services.AddSingleton<CSCompletionChecker>();
            services.AddSingleton<EvaluatorHelpersProvider>();
            services.AddScoped<ExerciseService>();
            services.AddScoped<CourseService>();
            services.AddScoped<ChromiumService>();

            services.AddScoped<UserService>();

            // var redis = ConnectionMultiplexer.Connect(Configuration.GetConnectionString("Redis"));

            // services.AddHangfire(configuration =>
            // {
            //     configuration
            //         .UseRecommendedSerializerSettings()
            //         .UseRedisStorage(redis);
            // });

            services.AddSingleton<Browser>(x =>
            {
                if(_env.IsDevelopment())
                {
                    return Puppeteer.ConnectAsync(new ConnectOptions()
                    {
                        BrowserURL="http://" + Dns.GetHostEntry("host.docker.internal").AddressList[0] + ":9222"
                    }).Result;
                }

                var browserFetcher = new BrowserFetcher();
                browserFetcher.DownloadAsync().Wait();
                return Puppeteer.LaunchAsync(new LaunchOptions
                {
                    Headless = true,
                    // https://stackoverflow.com/a/67578811/7868639 Chrome 90 uses 100% CPU trying to initialize non existing GPU
                    Args = new string[] { "--disable-gpu", "--disable-software-rasterizer" }
                }).Result;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            if (_env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger(c => c.RouteTemplate = "api/swagger/{documentname}/swagger.json");
                app.UseSwaggerUI(c => 
                {
                    c.SwaggerEndpoint("/api/swagger/v1/swagger.json", "AccessibilityAcademy v1");
                    c.RoutePrefix = "api/swagger";
                });
            }

            app.UseForwardedHeaders();

            app.UsePathBase("/api");
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                // endpoints.MapHangfireDashboard("/queue", new DashboardOptions()
                // {
                //     Authorization = new IDashboardAuthorizationFilter[]{ }
                // });
            });

            // app.UseHangfireServer();
        }
    }
}
