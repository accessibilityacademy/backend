using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccessibilityAcademy.Models {
    public class CourseExercise {
        public int Id { get; set; }

        public int CourseId { get; set; }
        public Course Course { get; set; }

        public int ExerciseId { get; set; }
        public Exercise Exercise { get; set; }
    }
}