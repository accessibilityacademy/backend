using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccessibilityAcademy.Models {
    public class Analytics {
        public int Id { get; set; }

        [Required]
        public string Action { get; set; }

        [Required]
        public User User { get; set; }

        public string Value { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime InsertedAt { get; set; }
    }
}