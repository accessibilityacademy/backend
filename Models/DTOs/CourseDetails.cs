using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccessibilityAcademy.DTOs {
    public class CourseDetails : Course {
        public IEnumerable<int> Exercises { get; set; }
    }
}