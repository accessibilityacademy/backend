using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccessibilityAcademy.DTOs {
    public class UserAnswer {
        [Required]
        public string Body { get; set; } // Answer written in HTML
    }
}