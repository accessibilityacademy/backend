using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccessibilityAcademy.DTOs {
    public class User {
        [Required]
        public string Email { get; set; }
        [Required]
        public string AEM { get; set; }
    }
}