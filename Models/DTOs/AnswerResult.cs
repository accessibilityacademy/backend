using System.Collections.Generic;

namespace AccessibilityAcademy.DTOs {
    public class AnswerResult {
        public IList<RequirementResult> RequirementResults { get; set; } // Correctness of answer per requirement
    }
}