using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccessibilityAcademy.DTOs {
    public class ExerciseRequirement {
        public int Id { get; set; }

        public string Body { get; set; } // Markdown text that describes the requirement
    }
}