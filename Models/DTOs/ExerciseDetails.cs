using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccessibilityAcademy.DTOs {
    public class ExerciseDetails {
        public int Id { get; set; }
        public string Body { get; set; } // Markdown text of the exercise
        public string Code { get; set; }
        public string HiddenCode { get; set; }
        public string LearningMaterial { get; set; }
        public string Title { get; set; }
        public string Slug { get; set; }
        public IEnumerable<ExerciseRequirement> Requirements { get; set; }
    }
}