using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccessibilityAcademy.DTOs {
    public class RequirementResult {
        public bool Correct { get; set; }

        public int RequirementId { get; set; }
    }
}