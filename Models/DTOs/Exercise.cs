using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccessibilityAcademy.DTOs {
    public class Exercise {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Slug { get; set; }
    }
}