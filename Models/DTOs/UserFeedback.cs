using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccessibilityAcademy.DTOs {
    public class UserFeedback {
        [Required]
        public string EditorState { get; set; }
        [Required]
        public string Type { get; set; }
        public string Details { get; set; }
    }
}