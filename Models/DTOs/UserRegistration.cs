using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccessibilityAcademy.DTOs {
    public class UserRegistration {
        [Required]
        public string Email { get; set; }
        public string AEM { get; set; }
    }
}