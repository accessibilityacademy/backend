using System.ComponentModel.DataAnnotations;

namespace AccessibilityAcademy.Models {
    public class WalkthroughStep {
        public int Id { get; set; }

        public string Title { get; set; }

        [Required]
        public string Step { get; set; } // Markdown of step
    }
}