using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccessibilityAcademy.Models {
    public class ExerciseRequirement {
        public int Id { get; set; }

        [Required]
        public string Requirement { get; set; } // Markdown text that describes the requirement

        [Required]
        public string CompletionChecker { get; set; } // Javascript or C# function that checks user answer

        [Required]
        public string CompletionCheckerLanguage { get; set; } // js or c#
    }
}