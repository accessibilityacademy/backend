using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccessibilityAcademy.Models {
    public class Exercise {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Slug { get; set; } // Displayed in the URL

        [Required]
        public string Body { get; set; } // Markdown text of the exercise

        [Required]
        public string Code { get; set; } // Initial code ( HTML ) that is typed in the editor

        [Required]
        public string HiddenCode { get; set; } // Initial hidden code ( HTML ) that exists above the typed code. Not seen in editor

        [Required]
        public string LearningMaterial { get; set; } // Markdown of Learning Material section.

        public IList<WalkthroughStep> WalkthroughSteps { get; set; }
        
        public IList<ExerciseRequirement> Requirements { get; set; }

        public IList<CourseExercise> ExerciseCourses { get; set; }
        public IList<Course> Courses { get; set; }
    }
}