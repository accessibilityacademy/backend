using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccessibilityAcademy.Models {
    public class RequirementResult {
        public int Id { get; set; }

        [Required]
        public bool Correct { get; set; }

        public int RequirementId { get; set; }
        
        public ExerciseRequirement Requirement { get; set; }
        
        public ExerciseAnswer Answer { get; set; }
    }
}