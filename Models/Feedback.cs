using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccessibilityAcademy.Models {
    public class Feedback {
        public int Id { get; set; }

        public int ExerciseId { get; set; }

        [Required]
        public string EditorState { get; set; }

        [Required]
        public string Type { get; set; }

        public string Details { get; set; }

        public User User { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime InsertedAt { get; set; }
    }
}