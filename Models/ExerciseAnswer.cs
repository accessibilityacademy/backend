using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccessibilityAcademy.Models {
    public class ExerciseAnswer {
        public int Id { get; set; }

        public IList<RequirementResult> RequirementResults { get; set; } // Correctness of answer per requirement

        [Required]
        public string Answer { get; set; } // Code ( HTML ) that was given as an answer from the User

        public Exercise Exercise { get; set; }

        public User User { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime InsertedAt { get; set; }
    }
}