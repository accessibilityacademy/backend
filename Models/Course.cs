using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccessibilityAcademy.Models {
    public class Course {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        public string Description { get; set; }

        public IList<CourseExercise> CourseExercises { get; set; }
        public IList<Exercise> Exercises { get; set; }

        public IList<UserCourse> UserCourses { get; set; }
    }
}