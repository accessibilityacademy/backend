using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccessibilityAcademy.Models {
    public class User {
        public int Id { get; set; }

        [Required]
        public string Email { get; set; }

        public string AEM { get; set; }

        public IList<Analytics> Analytics { get; set; }

        public IList<ExerciseAnswer> Answers { get; set; }

        public IList<UserCourse> UserCourses { get; set; }
    }
}