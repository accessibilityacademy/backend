using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccessibilityAcademy.Models {
    public class UserCourse {
        public int Id { get; set; }
        
        public User User { get; set; }

        public Course Course { get; set; }

        public int NextExerciseId { get; set; }
    }
}