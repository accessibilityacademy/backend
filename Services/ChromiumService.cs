using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccessibilityAcademy.Data;
using AccessibilityAcademy.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using PuppeteerSharp;
using System;
using PuppeteerSharp.PageAccessibility;
using Microsoft.Extensions.Configuration;
using AccessibilityAcademy.Helpers;

namespace AccessibilityAcademy.Services 
{
    public class ChromiumService 
    {

        private Browser _browser;
        private DatabaseContext _dBContext;
        private bool _isDevelopment;
        private IConfiguration _configuration;
        private EvaluatorHelpersProvider _evaluatorHelpersProvider;

        private CSCompletionChecker _csCompletionChecker;

        public ChromiumService(
            Browser browser,
            DatabaseContext dBContext,
            IWebHostEnvironment env,
            IConfiguration configuration,
            EvaluatorHelpersProvider evaluatorHelpersProvider,
            CSCompletionChecker csCompletionChecker
        )
        {
            _browser = browser;
            _dBContext = dBContext;
            _isDevelopment = env.IsDevelopment();
            _configuration = configuration;
            _evaluatorHelpersProvider = evaluatorHelpersProvider;
            _csCompletionChecker = csCompletionChecker;
        }

        public async Task<DTOs.AnswerResult> CheckAnswer(int answerId)
        {
            var answer = await _dBContext.Answers
                .Where(x => x.Id == answerId)
                .Include(x => x.Exercise)
                .ThenInclude(e => e.Requirements)
                .Include(x => x.User)
                .ThenInclude(x => x.UserCourses)
                .ThenInclude(x => x.Course)
                .ThenInclude(x => x.Exercises)
                .FirstAsync();

            answer.RequirementResults = new List<RequirementResult>();

            var page = await _browser.NewPageAsync();

            foreach(var requirement in answer.Exercise.Requirements)
            {
                await page.ReloadAsync();
                var setContentOp = page.SetContentAsync(
                    $"<base href=\"{_configuration["FrontendBase"]}\">" + 
                    "<body style=\"background-color: #424242\">" +
                    answer.Exercise.HiddenCode + " " + answer.Answer +
                    "</body>"
                );
                bool isTimedOut = await Task.WhenAny(setContentOp, Task.Delay(2000)) != setContentOp;
                if(isTimedOut)
                {
                    return null;
                }

                bool correct = false;

                if(requirement.CompletionCheckerLanguage == "js")
                {
                    JObject result = null;
                    try
                    {
                        var accessibility = await page.Accessibility.SnapshotAsync(new AccessibilitySnapshotOptions()
                        {
                            InterestingOnly = false
                        });
                        var evaluateResultOp = page.EvaluateFunctionAsync<dynamic>(
                            $"(rootAccNode, rawAnswer) => {{try{{{_evaluatorHelpersProvider.EvaluatorHelpers + requirement.CompletionChecker}}} catch(e) {{console.error(e);}}}}",
                            accessibility, answer.Answer
                        );
                        isTimedOut = await Task.WhenAny(evaluateResultOp, Task.Delay(2000)) != evaluateResultOp;
                        if(isTimedOut)
                        {
                            return null;
                        }
                        result = await evaluateResultOp;
                        if(result != null)
                        {
                            correct = result.SelectToken("correct").Value<bool>();
                        }
                    } 
                    catch(EvaluationFailedException e)
                    {
                        Console.WriteLine(e);
                    }
                    catch(Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
                else if(requirement.CompletionCheckerLanguage == "c#")
                {
                    try
                    {
                        var evaluateResultOp = _csCompletionChecker.Checker.test(requirement.Id, page, _evaluatorHelpersProvider.EvaluatorHelpers);
                        isTimedOut = await Task.WhenAny(evaluateResultOp, Task.Delay(2000)) != evaluateResultOp;
                        if(isTimedOut)
                        {
                            return null;
                        }
                        correct = await evaluateResultOp;
                    }
                    catch(Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }

                answer.RequirementResults.Add(new RequirementResult()
                {
                    Correct = correct,
                    RequirementId = requirement.Id
                });
            }

            if(!_isDevelopment)
            {
                page.Dispose();
            }

            if(answer.RequirementResults.All(x => x.Correct))
            {
                foreach(var userCourse in answer.User.UserCourses)
                {
                    if(userCourse.NextExerciseId == answer.Exercise.Id)
                    {
                        var indexOfCurrentExercise = userCourse.Course.Exercises.IndexOf(answer.Exercise);
                        if(indexOfCurrentExercise + 1 == userCourse.Course.Exercises.Count)
                        {
                            userCourse.NextExerciseId = -1;
                        }
                        else
                        {
                            userCourse.NextExerciseId = userCourse.Course.Exercises[indexOfCurrentExercise + 1].Id;
                        }
                    }
                }
            }



            await _dBContext.SaveChangesAsync();

            return new DTOs.AnswerResult()
            {
                RequirementResults = answer.RequirementResults.Select(x => new DTOs.RequirementResult()
                {
                   RequirementId = x.RequirementId,
                   Correct = x.Correct 
                }).ToList()
            };
        }
    }
}