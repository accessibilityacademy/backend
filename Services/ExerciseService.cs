using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccessibilityAcademy.Data;
using AccessibilityAcademy.Models;
using Microsoft.EntityFrameworkCore;
using PuppeteerSharp;

namespace AccessibilityAcademy.Services 
{
    public class ExerciseService 
    {

        private DatabaseContext _dbContext;
        // private IBackgroundJobClient _backgroundJobs;
        private ChromiumService _chromiumService;
        public ExerciseService(DatabaseContext dbContext, /*IBackgroundJobClient backgroundJobs,*/ ChromiumService chromiumService) 
        {
            // _backgroundJobs = backgroundJobs;
            _dbContext = dbContext;
            _chromiumService = chromiumService;
        }

        public async Task<IList<DTOs.Exercise>> GetAllExercises()
        {
            return await _dbContext.Exercises.Select(
               exercise => new DTOs.Exercise {
                   Id = exercise.Id,
                   Title = exercise.Title,
                   Slug = exercise.Slug
               }
           ).AsNoTracking().ToListAsync();
        }

        public async Task<DTOs.ExerciseDetails> GetOneById(int id)
        {
            return await _dbContext.Exercises.Include(x => x.Requirements).Where(x => x.Id == id).Select(
               exercise => new DTOs.ExerciseDetails {
                   Id = exercise.Id,
                   Title = exercise.Title,
                   Slug = exercise.Slug,
                   Body = exercise.Body,
                   Code = exercise.Code,
                   HiddenCode = exercise.HiddenCode,
                   LearningMaterial = exercise.LearningMaterial,
                   Requirements = exercise.Requirements.Select(requirement => new DTOs.ExerciseRequirement()
                   {
                       Id = requirement.Id,
                       Body = requirement.Requirement
                   })
               }
           ).AsNoTracking().FirstOrDefaultAsync();
        }

        public async Task<IList<WalkthroughStep>> GetWalkthroughByExerciseId(int id)
        {
            return await _dbContext.Exercises
                .Include(x => x.WalkthroughSteps)
                .Where(x => x.Id == id)
                .Select(x => x.WalkthroughSteps)
                .FirstOrDefaultAsync();
        }

        public async Task<bool> Exists(int id) 
        {
            return await _dbContext.Exercises.Where(x => x.Id == id).CountAsync() != 0;
        }

        // public async Task<object> Check() {
            // var page = await browser.NewPageAsync();
            // await page.GoToAsync("http://www.google.com");
            // var accessibility = await page.Accessibility.SnapshotAsync();
            // // return accessibility;
            // Newtonsoft.Json.Linq.JObject someObject = await page.EvaluateFunctionAsync<dynamic>("(acc) => ({ acc: acc })", accessibility);
            // return someObject.ToString();
        // }

        public async Task<DTOs.AnswerResult> CheckAnswer(string email, int exerciseId, string answer)
        {
            var exercise = await _dbContext.Exercises.FirstAsync(x => x.Id == exerciseId);
            var user = await _dbContext.Users.FirstAsync(x => x.Email == email);
            var exerciseAnswer = new ExerciseAnswer()
            {
                Answer = answer,
                Exercise = exercise,
                User = user
            };
            _dbContext.Answers.Add(exerciseAnswer);
            await _dbContext.SaveChangesAsync();
            
            // IStorageConnection connection = JobStorage.Current.GetConnection();
            // var jobData = connection.GetJobData("1e4534d0-7c43-4d0d-9eaa-92bc502f5de7");
            // Console.WriteLine(jobData.State);
            //_backgroundJobs.Enqueue<ChromiumService>(s => s.CheckAnswer(exerciseAnswer.Id));
            return await _chromiumService.CheckAnswer(exerciseAnswer.Id);
            // return exerciseAnswer.Id;
        }
    }
}