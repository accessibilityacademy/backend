using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccessibilityAcademy.Data;
using AccessibilityAcademy.DTOs;
using AccessibilityAcademy.Models;
using Microsoft.EntityFrameworkCore;

namespace AccessibilityAcademy.Services
{
    public class UserService
    {
        private DatabaseContext _dbContext;

        public UserService(DatabaseContext dbContext) 
        {
            _dbContext = dbContext;
        }

        public async Task<DTOs.User> GetOneByEmail(string email)
        {
            return await _dbContext.Users.Select(x => new DTOs.User
            {
                AEM = x.AEM,
                Email = x.Email
            }).FirstOrDefaultAsync(x => x.Email == email);
        }

        public async Task<bool> CreateOne(UserRegistration user)
        {
            _dbContext.Users.Add(
               new Models.User()
               {
                   Email = user.Email,
                   AEM = user.AEM
               }
           );

            try {
                await _dbContext.SaveChangesAsync();
            } 
            catch(DbUpdateException)
            {
                return false;
            }
            return true;
        }

        public async Task AnalyticsLoadExercise(string email, int exerciseId)
        {
            var user = _dbContext.Users.FirstOrDefault(x => x.Email == email);
            _dbContext.Analytics.Add(new Analytics()
            {
                Action = "LoadExercise",
                Value = exerciseId.ToString(),
                User = user
            });

            await _dbContext.SaveChangesAsync();
        }

        public async Task AnalyticsWalkthrough(string email, int exerciseId)
        {
            var user = _dbContext.Users.FirstOrDefault(x => x.Email == email);
            _dbContext.Analytics.Add(new Analytics()
            {
                Action = "LoadWalkthrough",
                Value = exerciseId.ToString(),
                User = user
            });

            await _dbContext.SaveChangesAsync();
        }

        public async Task Feedback(string email, int exerciseId, DTOs.UserFeedback feedback)
        {
            var user = _dbContext.Users.FirstOrDefault(x => x.Email == email);
            _dbContext.Feedback.Add(new Feedback()
            {
                EditorState = feedback.EditorState,
                Type = feedback.Type,
                Details = feedback.Details,
                User = user,
                ExerciseId = exerciseId
            });

            await _dbContext.SaveChangesAsync();
        }
    }
}