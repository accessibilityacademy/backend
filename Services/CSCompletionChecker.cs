using System.Linq;
using System.Threading.Tasks;
using AccessibilityAcademy.Data;
using Newtonsoft.Json.Linq;
using PuppeteerSharp;
using System;
using Microsoft.CodeAnalysis.CSharp;
using System.Reflection;
using Microsoft.CodeAnalysis;
using System.IO;
using System.Runtime.Loader;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;

namespace AccessibilityAcademy.Services 
{

    public interface ICompletionChecker 
    {
        Task<bool> test(int id, Page page, string evaluatorHelpers);
    }

    public class CSCompletionChecker 
    {
        public ICompletionChecker Checker;

        public CSCompletionChecker(
            IServiceScopeFactory serviceScopeFactory
        )
        {

            IList<Models.ExerciseRequirement> requirements;

            using(var scope = serviceScopeFactory.CreateScope())
            {
                var dBContext = scope.ServiceProvider.GetService<DatabaseContext>();
                requirements = dBContext.ExerciseRequirement.Where(x => x.CompletionCheckerLanguage == "c#").ToList();
            }


            StringBuilder sb = new StringBuilder();
            foreach(var requirement in requirements)
            {
                sb.AppendLine($"                case {requirement.Id}:");
                sb.AppendLine("                {");
                sb.AppendLine(requirement.CompletionChecker);
                sb.AppendLine("                }");
            }


            string code = $@"using System;
using System.IO;
using System.Linq;
using AccessibilityAcademy.Services;
using PuppeteerSharp;
using PuppeteerSharp.PageAccessibility;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
namespace AccessibilityAcademy
{{
    public class SolutionChecker : ICompletionChecker
    {{
        public async Task<bool> test(int id, Page page, string evaluatorHelpers)
        {{
            switch(id)
            {{
{sb.ToString()}
            }}
            return true;
        }}
    }}
}}";
            var tree = SyntaxFactory.ParseSyntaxTree(code);
            var systemRefLocation = typeof(object).GetTypeInfo().Assembly.Location;
            var sourceRefLocation=typeof(ICompletionChecker).GetTypeInfo().Assembly.Location;
            var puppeteerRefLocation=typeof(Page).GetTypeInfo().Assembly.Location;
            var newtonsoftJsonRefLocation=typeof(JToken).GetTypeInfo().Assembly.Location;

            // https://stackoverflow.com/questions/26822811/how-do-you-add-references-to-types-compiled-in-a-memory-stream-using-roslyn
            var systemRuntimeLocation = AppContext
                .GetData("TRUSTED_PLATFORM_ASSEMBLIES")
                .ToString()
                .Split(":", StringSplitOptions.RemoveEmptyEntries)
                .Single(x => x.Contains("System.Runtime.dll", StringComparison.OrdinalIgnoreCase));

            var systemDynamicRuntimeLocation = AppContext
                .GetData("TRUSTED_PLATFORM_ASSEMBLIES")
                .ToString()
                .Split(":", StringSplitOptions.RemoveEmptyEntries)
                .Single(x => x.Contains("System.Dynamic.Runtime.dll", StringComparison.OrdinalIgnoreCase));

            var linqRefLocation = AppContext
                .GetData("TRUSTED_PLATFORM_ASSEMBLIES")
                .ToString()
                .Split(":", StringSplitOptions.RemoveEmptyEntries)
                .Single(x => x.Contains("System.Linq.dll", StringComparison.OrdinalIgnoreCase));

            var linqExpressionsRefLocation = AppContext
                .GetData("TRUSTED_PLATFORM_ASSEMBLIES")
                .ToString()
                .Split(":", StringSplitOptions.RemoveEmptyEntries)
                .Single(x => x.Contains("System.Linq.Expressions.dll", StringComparison.OrdinalIgnoreCase));

            var consoleRefLocation = AppContext
                .GetData("TRUSTED_PLATFORM_ASSEMBLIES")
                .ToString()
                .Split(":", StringSplitOptions.RemoveEmptyEntries)
                .Single(x => x.Contains("System.Console.dll", StringComparison.OrdinalIgnoreCase));

            var netStandardRefLocation = AppContext
                .GetData("TRUSTED_PLATFORM_ASSEMBLIES")
                .ToString()
                .Split(":", StringSplitOptions.RemoveEmptyEntries)
                .Single(x => x.Contains("netstandard.dll", StringComparison.OrdinalIgnoreCase));

            var assemblyPath = Path.Combine(Directory.GetCurrentDirectory(), "SolverChecker.dll");

            var compilationResult = CSharpCompilation.Create("SolverChecker.dll")
                .WithOptions(new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary))
                .AddReferences(MetadataReference.CreateFromFile(systemRefLocation))
                .AddReferences(MetadataReference.CreateFromFile(systemRuntimeLocation))
                .AddReferences(MetadataReference.CreateFromFile(systemDynamicRuntimeLocation))
                .AddReferences(MetadataReference.CreateFromFile(linqRefLocation))
                .AddReferences(MetadataReference.CreateFromFile(linqExpressionsRefLocation))
                .AddReferences(MetadataReference.CreateFromFile(consoleRefLocation))
                .AddReferences(MetadataReference.CreateFromFile(puppeteerRefLocation))
                .AddReferences(MetadataReference.CreateFromFile(newtonsoftJsonRefLocation))
                .AddReferences(MetadataReference.CreateFromFile(sourceRefLocation))
                .AddReferences(MetadataReference.CreateFromFile(netStandardRefLocation))
                .AddSyntaxTrees(tree)
                .Emit(assemblyPath);


            if(!compilationResult.Success)
            {
                foreach (Diagnostic codeIssue in compilationResult.Diagnostics)
                {
                    string issue = $"ID: {codeIssue.Id}, Message: {codeIssue.GetMessage()}, Location: {codeIssue.Location.GetLineSpan()}, Severity: {codeIssue.Severity}";
                    Console.WriteLine(issue);
                }
                throw new Exception("Failed to compile SolverChecker");
            }
            else
            {
                Console.WriteLine("Attempting to load .DLL");
                Assembly asm = AssemblyLoadContext.Default.LoadFromAssemblyPath(assemblyPath);

                Checker = (ICompletionChecker) asm.CreateInstance("AccessibilityAcademy.SolutionChecker");
            }

        }
    }
}