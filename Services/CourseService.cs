using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccessibilityAcademy.Data;
using AccessibilityAcademy.Models;
using Microsoft.EntityFrameworkCore;
using PuppeteerSharp;

namespace AccessibilityAcademy.Services 
{
    public class CourseService
    {

        private DatabaseContext _dbContext;

        public CourseService(DatabaseContext dbContext) 
        {
            _dbContext = dbContext;
        }

        public async Task StartCourse(string email, int courseId)
        {
            var course = await _dbContext.Courses.Include(x => x.Exercises.Take(1)).FirstOrDefaultAsync(x => x.Id == courseId);

            var user = await _dbContext.Users
                .Include(x => x.UserCourses.Where(x => x.Course == course))
                .FirstOrDefaultAsync(x => x.Email == email);

            
            if(user.UserCourses.Count == 0)
            {
                user.UserCourses.Add(new UserCourse()
                {
                    Course = course,
                    NextExerciseId = course.Exercises[0].Id
                });
            }

            await _dbContext.SaveChangesAsync();
        }

        public async Task<IList<DTOs.Course>> GetAllCourses(string email)
        {
            var courses = await _dbContext.Courses.AsSplitQuery()
                .Include(x => x.UserCourses.Where(x => x.User.Email == email))
                .Include(x => x.Exercises)
                .AsNoTracking()
                .ToListAsync();

            return courses.Select(course =>
            {
                int progress = 0;
                if(course.UserCourses.Count != 0)
                {
                    progress = course.Exercises.ToList().FindIndex(x => x.Id == course.UserCourses[0].NextExerciseId);
                    if(progress == -1)
                    {
                        progress = course.Exercises.Count;
                    }
                }
                return new DTOs.Course
                {
                    Id = course.Id,
                    Title = course.Title,
                    Description = course.Description,
                    Progress = progress,
                    Target = course.Exercises.Count
                };
            }).ToList();
        }

        public async Task<DTOs.CourseDetails> GetOneById(string email, int id) 
        {
            var course = await _dbContext.Courses
                .Include(x => x.UserCourses.Where(x => x.User.Email == email))
                .Include(x => x.Exercises)
                .Where(x => x.Id == id)
                .AsNoTracking()
                .FirstOrDefaultAsync();


            int progress = 0;
            if(course.UserCourses.Count != 0)
            {
                progress = course.Exercises.ToList().FindIndex(x => x.Id == course.UserCourses[0].NextExerciseId);
                if(progress == -1)
                {
                    progress = course.Exercises.Count;
                }
            }
            return new DTOs.CourseDetails
            {
                Id = course.Id,
                Title = course.Title,
                Description = course.Description,
                Progress = progress,
                Target = course.Exercises.Count,
                Exercises = course.Exercises.Select(exercise => exercise.Id)
            };
        }
    }
}