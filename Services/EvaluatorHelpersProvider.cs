namespace AccessibilityAcademy.Helpers 
{
    public class EvaluatorHelpersProvider 
    {
        public string EvaluatorHelpers { get; }

        public EvaluatorHelpersProvider()
        {
            EvaluatorHelpers = System.IO.File.ReadAllText("./Assets/evaluator-helpers.js");
        }
    }
}