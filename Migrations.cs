using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AccessibilityAcademy.Data;
using AccessibilityAcademy.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

public static class MigrationManager
{
    public static IHost MigrateDatabase(this IHost host)
    {
        using (var scope = host.Services.CreateScope())
        {
            var env = scope.ServiceProvider.GetRequiredService<IWebHostEnvironment>();

            using (var dbContext = scope.ServiceProvider.GetRequiredService<DatabaseContext>())
            {
                dbContext.Database.Migrate();


                if(env.IsProduction())
                {
                    using (var process = new Process())
                    {
                        process.StartInfo.FileName = "dotnet"; // relative path. absolute path works too.
                        process.StartInfo.Arguments = "./ExerciseBuilder/AccessibilityAcademy.ExerciseBuilder.dll run --input ./Exercises --commit";
                        process.StartInfo.CreateNoWindow = true;
                        // process.StartInfo.UseShellExecute = false;
                        process.StartInfo.RedirectStandardOutput = true;
                        process.StartInfo.RedirectStandardError = true;

                        process.OutputDataReceived += (sender, data) => Console.WriteLine(data.Data);
                        process.ErrorDataReceived += (sender, data) => Console.WriteLine(data.Data);
                        Console.WriteLine("Starting to seed database with exercises");
                        process.Start();
                        process.BeginOutputReadLine();
                        process.BeginErrorReadLine();
                        process.WaitForExit();
                    }
                }

            
                var exercises = dbContext.Exercises.ToList();
                
                _stageCourse(
                    new Course()
                    {
                        Id = 1,
                        Title = "Easy exercises",
                        //Exercises = _prepareExerciseArray(new int[] { 1, 2, 7, 9, 12, 14, 20, 22, 23, 25, 26 }, exercises)
                        CourseExercises = _prepareExerciseArray(new int[] { 1, 2, 7, 8, 10, 13, 15, 21, 23, 24, 26, 27, 34, 35, 36 }, exercises)
                    },
                    dbContext
                );

                _stageCourse(
                    new Course()
                    {
                        Id = 2,
                        Title = "Normal exercises",
                        //Exercises = _prepareExerciseArray(new int[] { 4, 6, 8, 10, 15, 16, 17, 19, 24, 27, 28, 30, 31 }, exercises)
                        CourseExercises = _prepareExerciseArray(new int[] { 4, 6, 9, 11, 16, 17, 18, 20, 25, 28, 29, 31, 32 }, exercises)
                    },
                    dbContext
                );

                _stageCourse(
                    new Course()
                    {
                        Id = 3,
                        Title = "Hard exercises",
                        //Exercises = _prepareExerciseArray(new int[] { 3, 5, 11, 13, 18, 21, 29, 32 }, exercises)
                        CourseExercises = _prepareExerciseArray(new int[] { 3, 5, 12, 14, 19, 22, 30, 33 }, exercises)
                    },
                    dbContext
                );

                // 18 exercises

                _stageCourse(
                    new Course()
                    {
                        Id = 4,
                        Title = "Recommended exercises",
                        //Exercises = _prepareExerciseArray(new int[] { 1, 2, 7, 9, 12, 14, 20, 22, 23, 25, 26 }, exercises)
                        CourseExercises = _prepareExerciseArray(new int[] { 1, 2, 6, 7, 8, 10, 13, 16, 20, 21, 23, 24, 26, 27, 34, 35, 36 }, exercises)
                    },
                    dbContext
                );

                _stageCourse(
                    new Course()
                    {
                        Id = 5,
                        Title = "Extra exercises",
                        //Exercises = _prepareExerciseArray(new int[] { 1, 2, 7, 9, 12, 14, 20, 22, 23, 25, 26 }, exercises)
                        CourseExercises = _prepareExerciseArray(new int[] { 3, 4, 5, 9, 11, 12, 14, 15, 17, 18, 19, 22, 25, 28, 29, 30, 31, 32, 33 }, exercises)
                    },
                    dbContext
                );

                dbContext.SaveChanges();
            }
        }

        return host;
    }

    private static void _stageCourse(Course newCourse, DatabaseContext dbContext) 
    {
        var existingCourse = dbContext.Courses.Include(x => x.CourseExercises).FirstOrDefault(x => x.Id == newCourse.Id);
        if(existingCourse == null)
        {
            dbContext.Courses.Add(newCourse);
        }
        else
        {
            existingCourse.Title = newCourse.Title;
            existingCourse.Description = newCourse.Description;

            var existingCourseExercises = existingCourse.CourseExercises.ToList();
            existingCourse.CourseExercises = newCourse.CourseExercises.Select((x, i) =>
            {
                if(i < existingCourseExercises.Count)
                {
                    existingCourseExercises[i].ExerciseId = x.ExerciseId;
                    return existingCourseExercises[i];
                }
                return x;
            }).ToList();

            /*
            int i = 0;
            for (i = 0; i < newCourse.Exercises.Count; i++)
            {
                if(i < existingCourse.CourseExercises.Count)
                {
                    if(newCourse.Exercises[i].Id != existingCourse.CourseExercises[i].ExerciseId)
                    {
                        existingCourse.CourseExercises[i].ExerciseId = newCourse.Exercises[i].Id;
                    }
                    continue;
                }
                else
                {
                    existingCourse.CourseExercises.Append(new CourseExercise()
                    {
                        CourseId = existingCourse.Id,
                        ExerciseId = newCourse.Exercises[i].Id
                    });
                }
            }

            while (i < existingCourse.CourseExercises.Count) {
                existingCourse.CourseExercises.Remove(existingCourse.CourseExercises[i]);
            }
            */

            dbContext.Courses.Update(existingCourse);
        }
    }

    /*private static IList<Exercise> _prepareExerciseArray(IEnumerable<int> exercisesIds, IList<Exercise> existingExercises)
    {
        var exercises = new List<Exercise>(exercisesIds.Count());

        foreach (var exerciseId in exercisesIds)
        {
            var existingExercise = existingExercises.FirstOrDefault(x => x.Id == exerciseId);
            if(existingExercise == null)
            {
                Console.Error.WriteLine($"Missing exercise {exerciseId} while creating courses");
                continue;
            }
            exercises.Add(existingExercise);
        }

        return exercises;
    }*/
    private static IList<CourseExercise> _prepareExerciseArray(IEnumerable<int> exercisesIds, IList<Exercise> existingExercises)
    {
        var exercises = new List<CourseExercise>(exercisesIds.Count());

        foreach (var exerciseId in exercisesIds)
        {
            var existingExercise = existingExercises.FirstOrDefault(x => x.Id == exerciseId);
            if(existingExercise == null)
            {
                Console.Error.WriteLine($"Missing exercise {exerciseId} while creating courses");
                continue;
            }
            exercises.Add(new CourseExercise()
            {
                ExerciseId = exerciseId
            });
        }

        return exercises;
    }
}