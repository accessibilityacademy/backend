---
id: 36
title: Exercise 4.3 - Status Messages
slug: status-messages
---


# Exercise-Area

The following website saves a draft when a user stops typing. Such information should be made available to assistive technologies by marking any area that changes content with an appropriate role. The most common roles for these use cases are: status, alert, log

# Requirements-Area

-   The information about the automatic draft saving is heard by screen reader users. Use the status role on the information container!
    ```js
    return { correct: document.getElementById('information-container').getAttribute('role').toLowerCase() === 'status' };
    ```

# Hidden-Code-Area

```html
<style>
    label {
        vertical-align: top;
    }
</style>
```

# Code-Area

```html
<p id="information-container"></p>

<label for="message">Message: </label>
<textarea id="message" rows="6"></textarea>
<script>
    let timer;
    const textarea = document.querySelector("textarea");
    textarea.addEventListener("input", (e) => {
        if(timer) {
            clearTimeout(timer);
        }
        timer = setTimeout(() => debouncedOnChange(e), 1000);
    });

    let informationContainer = document.getElementById("information-container");
    function debouncedOnChange() {
        informationContainer.innerText = "Draft saved.";
    }
</script>
```

# Walkthrough-Area

- 
    Simply add the role of status on the paragraph that displays the message:
    ```html
    <p id="information-container" role="status"></p>
    ```



# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/status-messages
- Related Techniques: 
  - https://www.w3.org/WAI/WCAG21/Techniques/aria/ARIA22
  - https://www.w3.org/WAI/WCAG21/Techniques/failures/F103
- Using the status role: https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_status_role