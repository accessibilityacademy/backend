---
id: 35
title: Exercise 4.2 - Name, Role, Value
slug: name-role-value
---


# Exercise-Area

The following website contains an input that does not have an accessibility name. A screen reader would not be able to provide useful information on what the input is for.

Note: [Exercise 1.3](/exercise/3/info-and-relationships-2) also violates this criterion because there was script usage to make generic html elements function like user interface controls without providing a role. For a better understanding of this criterion, make sure to also complete that exercise!

# Requirements-Area

-   The input must have the proper accessibility name "***search***". Do not get confused with the name attribute. The only techniques that set the accessible name are the following:
    - Having a `<label>` reference the input with the [for](https://www.w3schools.com/tags/att_label_for.asp) attribute
    - Having an aria-label or aria-labelledby on the input
    - Having a placeholder
    - Having a `<label>` as a parent of the input

    ```js
    return { correct: acc.find(x => x.role === 'textbox').name.toLowerCase().includes('search') }
    ```

# Hidden-Code-Area

```html
```

# Code-Area

```html
<input name="search">
<button>Search</button>
```

# Walkthrough-Area

- # Add a name
    One of the ways to assign the name "***Search***" on the input is to reference the text of the button:
    ```html
    <input aria-labelledby="search-btn" name="search">
    <button id="search-btn">Search</button>
    ```


# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/name-role-value
- Related Techniques: 
  - https://www.w3.org/WAI/WCAG21/Techniques/failures/F68
  - https://www.w3.org/WAI/WCAG21/Techniques/aria/ARIA16