---
id: 34
title: Exercise 4.1 - Parsing
slug: parsing
---


# Exercise-Area

The following website contains markup errors. Even though some errors can be corrected by most browsers, other assistive web technologies may not interpret the page as intended.

Hint: You can use a [markdown validator](https://validator.w3.org/#validate_by_input+with_options) to find such errors.

# Requirements-Area

-   A div is not allowed inside a paragraph. The creator of the website probably intended to use a span. 
    Change the HTML so as not to violate this rule.

    ```js
    const paragraphs = document.querySelectorAll('p')
    return { correct: 
        paragraphs.length === 1 &&
        paragraphs[0].innerText === 'Instructions: Fill your username below and click go to begin!'
    }
    ```

-   Fix the lack of whitespace between attributes on the input element
    ```js
    const matches = rawAnswer.match(/"[^"]*"(.|\n)|'[^']*'(.|\n)/g);
    if(matches) {
        for(const match of matches) {
            if(!match.endsWith(">") && !match.endsWith(" ") && !match.endsWith("\n")) {
                return { correct: false };
            }
        }
    }

    const input = document.querySelector('input');

    return { correct: 
        input.getAttribute('name') === 'search' &&
        input.getAttribute('placeholder') === 'username' &&
        input.getAttribute('autocomplete') === 'username'
    };
    ```

# Hidden-Code-Area

```html
<style>
    .text-red {
        color: var(--contrast-color-red);
    }
</style>
```

# Code-Area

```html
<p>
    Instructions: Fill your username below and click
    <div class="text-red" style="display: inline;">
        go 
    </div>
    to begin!
</p>

<input name="search"placeholder="username"autocomplete="username">
<button class="text-red">Go</button>
```

# Walkthrough-Area

- # Fix the paragraph
    To fix the paragraph, change the div into a span:
    ```html
    <p>
        Instructions: Fill your username below and click 
        <span class="text-red">go</span> 
        to begin!
    </p>
    ```
- # Add whitespaces in the input attributes
    ```html
    <input name="search" placeholder="username" autocomplete="username">
    ```


# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/parsing
- Related Techniques: 
    - https://www.w3.org/WAI/WCAG21/Techniques/failures/F70
    - https://www.w3.org/WAI/WCAG21/Techniques/html/H88
    - https://www.w3.org/WAI/WCAG21/Techniques/general/G134