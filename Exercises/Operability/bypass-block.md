---
id: 20
title: Exercise 2.5 - Bypass Blocks
slug: bypass-blocks
---


# Exercise-Area

The following website is very hard to use with assistive technologies because there is no easy way to bypass the header and jump to the content.  

Additionally, accessing the contact form through the heading with a screen reader will make blind users miss some information because it's above the heading.

It is important for headings to be at the beginning of each section. If there is additional information before the heading, the use of (ARIA landmarks)[https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques#landmark_roles]
should be preferred. 

# Requirements-Area

- 
    
    Make it easy to skip the header of the website. Either provide a **skip navigation** link or an even simpler way is to use semantic markup for the document. You can change the `<div>` into a `<main>`.
    ```js
    const hasMainLandmark = !!acc.find(x => x.role === "main");

    const skipLink = document.querySelector("a[href='#content']");


    let validSkipLink = false;
    if(skipLink) {
        skipLink.focus();
        const isFirstLink = Array.from(document.querySelectorAll("a")).indexOf(skipLink) === 0;
        const isVisibleAndFocusable = skipLink.getBoundingClientRect().height > 15 && document.activeElement === skipLink;
        validSkipLink = isFirstLink && isVisibleAndFocusable;
    }
    return { correct: hasMainLandmark || validSkipLink };
    ```

- 
    Mark the contact form section appropriately. Provide either a hidden level 1 heading or a region landmark for the contact form.  
    Do ***not*** change the appearance of the form!  
    Make sure that the text `Have questions? Fill the contact form below` is included in the region!
    ```js
    const caption = acc.find(x => x.name === 'Have questions? Fill the contact form below');
    const contactForm = acc.find(x => (x.role === "heading" && x.level === 1 || x.role === "region") && x.name.toLowerCase().includes("contact"));

    const headings = acc.findAll(x => x.role === "heading" && x.name.toLowerCase().includes("contact"));

    const captionRect = document.querySelector('p').getBoundingClientRect();
    
    if(document.getElementById('contact-heading').innerText !== 'Contact form') {
        return { correct: false };
    }

    const headingRect = document.getElementById('contact-heading').getBoundingClientRect();

    const documentFormText = document.getElementById('contact-form').innerText;

    return { correct: 
        contactForm.isAbove(caption) &&
        headings.length <= 1 &&
        captionRect.bottom < headingRect.top &&
        documentFormText.toLowerCase().split('contact').length - 1 === 2
    };
    ```

# Hidden-Code-Area

```html
<style>
    .nav-links {
        list-style-type: none;
        display: flex;
        padding: 0;
        margin: 0;
        flex-wrap: wrap;
    }

    .nav-links li {
        border: 1px solid white;
        padding: 16px 4px;
        border-radius: 4px;
        white-space: nowrap;
    }

    .nav-links > * {
        margin-right: 8px;
    }

    body {
        margin: 0;
    }

    header {
        padding: 8px 16px;
        background-color: black;
    }

    a {
        color: #7CB1FF;
    }

    @media (min-height: 250px) {
        header {
            position: sticky;
            top: 0;
        }
    }

    label {
        display: block;
    }

    #content {
        margin: 8px;
    }

    .main-content {
        display: flex;
        align-items: center;
        justify-content: center;
        height: 450px;
        border: 1px solid black;
        margin-bottom: 16px;
    }
</style>
```

# Code-Area

```html
<style>
    #contact-heading {
        margin-top: 0;
        margin-bottom: 16px;
        font-size: 1.5rem;
        font-weight: bold;
    }
</style>
<header>
    <nav>
        <ul class="nav-links">
            <a href="#"><li>Homepage</li></a>
            <a href="#"><li>Webpage 1</li></a>
            <a href="#"><li>Webpage 2</li></a>
            <a href="#"><li>Webpage 3</li></a>
            <a href="#"><li>Webpage 4</li></a>
            <a href="#"><li>Webpage 5</li></a>
        </ul>
    </nav>
</header>

<div id="content">
    <div class="main-content">
        Main website content.
    </div>
    <div id="contact-form">
        <p>Have questions? Fill the contact form below</p>
        <h1 id="contact-heading">
            Contact form
        </h1>
        <form>
            <label for="email">Email</label>
            <input id="email" name="email">

            <label for="subject">Subject</label>
            <input id="subject" name="subject">
        </form>
    </div>
</div>
```

# Walkthrough-Area
- 
    # Make skipping to main content easy
    First required change is to fix the markup to be more semantic by changing the div into a main element:
    ```html
    <div id="content"> <!-- before -->
        ...
    </div>

    <main id="content"> <!-- after -->
        ...
    </main>
    ```
    # Optional improvement:
    Even though the above technique is sufficient for the WCAG criterion, accessibility can be improved further by providing a skip navigation link:
    ```html
    <header>
      <nav>
          <ul class="nav-links">
              <a href="#content" class="hidden-unfocused"><li>Skip navigation</li></a>
              ...
    ```

    To make the link disappear when not focused by a keyboard user add the following CSS:
    ```css
    .hidden-unfocused:not(:focus) {
        opacity: 0;
        width: 1px;
        height: 1px;
    }

    .hidden-unfocused:focus {
        opacity: 1
    }
    ```
- 
    # Mark the contact region
    The best solution is to mark the region with ARIA attributes:
    ```html
    <div id="contact-form" role="region" aria-labelledby="contact-heading"> <!-- Notice the region -->
        <p>Have questions? Fill the contact form below</p>
        <h1 id="contact-heading">
            Contact form
        </h1>
        <form>
            ...
        </form>
    </div>
    ```

    Another suboptimal but perfectly fine solution is to include a hidden accessible heading in the beginning of the form:
    ```html
    <div id="contact-form">
        <h1 aria-labelledby="contact-heading"></h1> <!-- Not seen by normal screens -->
        <p>Have questions? Fill the contact form below</p>
        <h1 aria-hidden="true" id="contact-heading"> <!-- Not seen by screen readers -->
            Contact form
        </h1>
        <form>
            ...
        </form>
    </div>
    ```


# Learning-Material-Area
- WCAG: 
    - https://www.w3.org/WAI/WCAG21/Understanding/bypass-blocks
    - https://www.w3.org/WAI/WCAG21/Understanding/identify-purpose
    - https://www.w3.org/WAI/WCAG21/Understanding/section-headings
- Related Techniques: 
    - https://www.w3.org/WAI/WCAG21/Techniques/aria/ARIA11
    - https://www.w3.org/WAI/WCAG21/Techniques/general/G1
    - https://www.w3.org/WAI/WCAG21/Techniques/html/H69
    - https://www.w3.org/WAI/WCAG21/Techniques/general/G141
- WAI tutorial on Regions: https://www.w3.org/WAI/tutorials/page-structure/regions
- WAI tutorial on Labeling Regions: https://www.w3.org/WAI/tutorials/page-structure/labels
