---
id: 18
title: Exercise 2.3 - Character Key Shortcuts
slug: character-key-shortcuts
---


# Exercise-Area

A developer created a keyboard shortcut for opening a search popup. By pressing the key **s** when there are no elements focused, the popup is enabled. By doing so they violated [Success Criterion 2.1.4](https://www.w3.org/TR/WCAG21/#character-key-shortcuts) because a user who uses assistive technologies may require this key to operate the website. 

This violation can occur when a shortcut is using only letters and no [modifier keys](https://en.wikipedia.org/wiki/Modifier_key). **Shift** alone is also not acceptable. Generally the acceptable solutions are:

- to allow the user to change the shortcut to a different one
- to use a shortcut that requires modifier keys e.g. **Alt + S**

**Hint**: When testing for this violation make sure that no input is focused!

# Requirements-Area

- 
    Change the shortcut key combination to **Alt + S**.

    ```c#
    var modal = await page.QuerySelectorAsync(".modal");
                        
    await page.Keyboard.PressAsync("s");
    if(await modal.IsIntersectingViewportAsync()) {
        return false;
    }

    await page.Keyboard.DownAsync("Alt");
    await page.Keyboard.PressAsync("s");
    await page.Keyboard.UpAsync("Alt");
    return await modal.IsIntersectingViewportAsync();
    ```

# Hidden-Code-Area

```html
<style>
    .backdrop {
        background-color: rgba(0, 0, 0, 0.5);
        position: fixed;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        display: none;
        justify-content: center;
        align-items: center;
    }

    .modal {
        color: black;
        padding: 16px;
        background-color: white;
    }
</style>
<script>
    window.addEventListener("load", () => {
        main();
    }); 
    function main() {
        const backdrop = document.querySelector(".backdrop");
        const modal = document.querySelector(".modal");
        const form = document.querySelector("form");

        backdrop.addEventListener("click", () => {
            hideModal();
        });
        modal.addEventListener("click", (e) => {
            e.stopPropagation();
        });
        document.addEventListener("keydown", (e) => {
            if(e.code === "Escape") {
                hideModal();
            }
        });

        function hideModal() {
            backdrop.style.display = "none";
            modal.setAttribute("aria-hidden", true);
            form.setAttribute("aria-hidden", false);
        }
    }

    if(document.readyState === "complete") {
        main();
    }
</script>
```

# Code-Area

```html
<form>
    <h2>Contact form</h2>
    <div class="form-control">
        <label for="fullName">FullName: </label>
        <input name="fullName" id="fullName" autocomplete="off">
    </div>
    <div class="form-control">
        <label for="message">Message: </label>
        <textarea name="message" id="message"></textarea>
    </div>

    <button type="submit">submit</button>
</form>

<div class="backdrop" role="presentation">
    <div class="modal" aria-hidden="true">
        <label for="search">Search: </label>
        <input name="search" id="search">
    </div>
</div>
<script>
    const backdrop = document.querySelector(".backdrop");
    const modal = document.querySelector(".modal");
    const form = document.querySelector("form");
    document.addEventListener("keydown", (e) => {
        // Don't handle shortcuts if an element is focused
        if (e.target !== document.body) return; 
        if(e.code === "KeyS") { // On press S

            // Unhide the modal from assistive technology
            modal.setAttribute("aria-hidden", false);
            // Hide the rest of the website from assistive technology
            form.setAttribute("aria-hidden", true);
            backdrop.style.display = "flex";

        }
    });
</script>
```

# Walkthrough-Area
- 
    Changing the javascript event to only display the modal when alt was pressed alongside the key S is fairly easy.
    ```js
    if(e.code === "KeyS") { // previous handler check
    if(e.code === "KeyS" && e.altKey) { // new handler check
    ```

# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/character-key-shortcuts
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/failures/F99
