---
id: 23
title: Exercise 2.8 - Link Purpose in Context
slug: link-purpose-in-context
---


# Exercise-Area

A website with articles has a preview function where only a small part of an article is displayed on the homepage. The user can click on a link to read the rest of the article. These links are labelled as **read more**, making it hard for screen reader users to know which topic they would read more about.

The best solution is to provide custom labels of such links with `aria-label` and `aria-labelledby`.
This solution provides level AAA conformance.  
Web Content Accessibility Guidelines relax the requirements for links: if the purpose of the link can be found on the same paragraph that the link is in, a level AA conformance can be achieved.

# Requirements-Area

- 
    The link purpose can be determined at least in context.
    ```js
    const articleText = acc.find(x => x.name?.includes("In publishing"));
    const readMoreLink = acc.find(x => x.role === "link");

    const linkInSameParagraph = readMoreLink.parent.children.indexOf(articleText) !== -1;
    const linkIsLabelled = readMoreLink.name.toLowerCase().includes("lorem ipsum");

    return { correct: linkInSameParagraph || linkIsLabelled };
    ```

- 
    Do not change the link text.
    ```js
    return { correct: document.querySelector("a").innerText === "Read more" };
    ```

# Hidden-Code-Area
```html
<style>
    body.dark a {
        color: #7CB1FF;
    }
    a:-webkit-any-link {
        color: -webkit-link;
    }
    a:-webkit-any-link:active {
        color: -webkit-activelink;
    }
</style>
```

# Code-Area

```html
<div>
    <h1>
        Lorem ipsum
    </h1>
    <p>
        In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.
    </p>
    <a href="#">Read more</a>
</div>
```

# Walkthrough-Area
- 
    In order not to change the visual appearance and to be compliant with the strictest criterion, add a label to the link:

    ```html
    <a href="#" aria-label="read more about lorem ipsum">Read more</a>
    ```


# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/link-purpose-in-context
- More strict WCAG criterion: https://www.w3.org/WAI/WCAG21/Understanding/link-purpose-link-only
- Related Techniques: 
    - https://www.w3.org/WAI/WCAG21/Techniques/aria/ARIA8
    - https://www.w3.org/WAI/WCAG21/Techniques/html/H78