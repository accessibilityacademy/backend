---
id: 19
title: Exercise 2.4 - Timing adjustable and Pause, Stop, Hide
slug: timing-adjustable-and-pause-stop-hide
---


# Exercise-Area

A news site shows scrolling headlines to grab the attention of the user. Such scrolling headlines are hard to read for some users. Moreover, the fact that there is a limited time to read a headline before going to the next one is a serious issue. 

# Requirements-Area

- 
    When hovering the headline, the animation stops to read comfortably.

    ```c#
    var animatedHeadline = await page.QuerySelectorAsync(".headlines-animated");
    
    var previousX = (await animatedHeadline.BoundingBoxAsync()).X;
    await Task.Delay(20);
    var currentX = (await animatedHeadline.BoundingBoxAsync()).X;

    if(currentX >= previousX) {
        return false; // Animation is not working
    }

    await animatedHeadline.HoverAsync();
    previousX = (await animatedHeadline.BoundingBoxAsync()).X;
    await Task.Delay(20);
    currentX = (await animatedHeadline.BoundingBoxAsync()).X;

    return previousX == currentX;
    ```

- 
    Add functionality to start displaying the current heading from the beginning. Please use the attribute `id="reset"` on the element that when clicked, resets the current heading scrolling state.
    ```c#
    var animatedHeadline = await page.QuerySelectorAsync(".headlines-animated");
    var reset = await page.QuerySelectorAsync("#reset");

    await Task.Delay(100);

    var previousTextHeadline = (await page.EvaluateExpressionAsync(
        "document.querySelector('.headlines-animated').textContent"
    )).Value<string>();
    var previousX = (await animatedHeadline.BoundingBoxAsync()).X;
    await reset.ClickAsync();
    animatedHeadline = await page.QuerySelectorAsync(".headlines-animated"); // Headline is recreated on click
    var currentX = (await animatedHeadline.BoundingBoxAsync()).X;
    var currentTextHeadline = (await page.EvaluateExpressionAsync(
        "document.querySelector('.headlines-animated').textContent"
    )).Value<string>();

    return currentX > previousX && previousTextHeadline == currentTextHeadline;
    ```

# Hidden-Code-Area

```html
```

# Code-Area

```html
<style>
.headlines-container {
    max-width: 400px;
    background-color: black;
    color: white;
    overflow: hidden;
}

.headlines-animated {
    display: inline-block;
    padding-left: 100%;
    animation: scrolling 4s linear forwards;
    white-space: nowrap;
}

@keyframes scrolling {
    to {
        transform: translateX(-100%);
    }
}
</style>
<div class="headlines-container">
    <p class="headlines-animated">
        Placeholder. This text is controlled from javascript
    </p>
</div>
<script>

    const headlines = [
        "First headline is displayed right now", 
        "Second headline is displayed right now", 
        "Third headline is displayed right now", 
    ];

    let currentIndex = -1;

    let previousAnimatedHeadlineNode = document.querySelector(".headlines-animated");

    nextHeadline();

    // Add your code here

    function nextHeadline() {
        currentIndex++;
        if(currentIndex === headlines.length) {
            currentIndex = 0;
        }
        const headline = previousAnimatedHeadlineNode.cloneNode(true);
        headline.innerText = headlines[currentIndex];
        previousAnimatedHeadlineNode.parentNode.replaceChild(headline, previousAnimatedHeadlineNode);
        headline.addEventListener("animationend", () => {
            nextHeadline();
        }, { once: true });
        previousAnimatedHeadlineNode = headline;
    }
</script>
```

# Walkthrough-Area
- 
    # Pause scroll on hover
    Pausing of the CSS animation on hover of the container can be done without any use of javascript with the following css:
    ```css
    .headlines-container:hover > .headlines-animated {
        animation-play-state: paused;
    }
    ```

- 
    # Reset scroll position on button click
    First of all, add the button for the user to click: 
    ```html
    <div class="headlines-container">
        ...
    </div>
    <button type="button" id="reset">Reset headline</button>
    ```

    and then set it up to reset the headline in the javascript code:
    ```js
    let resetButton = document.getElementById("reset");
    resetButton.addEventListener("click", () => {
        currentIndex--;
        nextHeadline();
    });
    ```


# Learning-Material-Area
- WCAG: 
    - https://www.w3.org/WAI/WCAG21/Understanding/timing-adjustable
    - https://www.w3.org/WAI/WCAG21/Understanding/pause-stop-hide
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/general/G4
