---
id: 26
title: Exercise 2.11 - Label in Name
slug: label-in-name
---


# Exercise-Area

A developer created a search functionality for their website. Because they thought that the name of the button shown on the screen is not clear enough to users of screen readers, they provided a different accessible name.  
What slipped their mind was that whilst some people can see the visible name, they use assistive technologies to select inputs e.g. speech-input. Because the accessible name does not include the visible text, such users will have trouble clicking the button.

# Requirements-Area

-   The accessible name of the submit button must at the very least include the whole text of the visible name.

    ```js
    const name = document.querySelector("input[type='submit']").value.toLowerCase();
    const accessibleName = acc.find(x => x.role === 'button').name.toLowerCase();

    return { correct: accessibleName.includes(name) };
    ```

# Hidden-Code-Area

```html
<style>

</style>
<script>
    window.addEventListener("load", () => {
        main();
    });

    function main() {
        document.querySelector("form").addEventListener("submit", (e) => {
            e.preventDefault();
        });
    }

    if(document.readyState === "complete") {
        main();
    }
</script>
```

# Code-Area

```html
<form>
    <label for="search">Search: </label>
    <input name="search" id="search">
    <input 
        type="submit"
        aria-label="Search in the website"
        value="Go"
    >
</form>
```

# Walkthrough-Area

- 
    Include the visible text in the aria-label attribute:
    ```html
    <input 
        type="submit"
        aria-label="Go and search in the website"
        value="Go"
    >
    ```


# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/label-in-name
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/general/G208