---
id: 25
title: Exercise 2.10 - Focus Visible
slug: focus-visible
---


# Exercise-Area

The developer who created the webpage for answering quizzes... made another mistake. After adding the hidden radio input to solve the previous accessibility issues, they failed to handle the focus styling so as to make focus visible to keyboard users. 

# Requirements-Area

-   When an answer is focused, a visual cue is displayed. Make sure to use a CSS [outline](https://developer.mozilla.org/en-US/docs/Web/CSS/outline).  
    Hint: Because you want to style the `<label>` when the `<input>` itself is focused, you must use the [:focus-within](https://developer.mozilla.org/en-US/docs/Web/CSS/:focus-within) pseudo-class.

    ```js
    const label = document.querySelector('label');
    const labelStyles = getComputedStyle(label);

    if(labelStyles.outlineWidth !== '0px' && labelStyles.outlineStyle !== 'none') {
        return { correct: false }; // Outline must not be visible when not in focus!
    }

    label.focus();

    return { correct: labelStyles.outlineWidth !== '0px' &&  labelStyles.outlineStyle !== 'none' };
    ```

# Hidden-Code-Area

```html
<style>
    p {
        font-size: 1.25rem;
        margin: 0;
    }

    .list {
        padding: 0;
        margin: 0;
    }

    .list-item {
        padding: 8px 16px;
        cursor: pointer !important;
        display: block;
    }

    .list-item:hover {
        background-color: rgba(var(--contrast-color-rgb), 0.1);
    }

    .list-item.active {
        background-color: rgba(var(--contrast-color-rgb), 0.2);
    }

    .visuallyHidden {
        position: absolute;
        clip: rect(0, 0, 0, 0);
    }

    /* https://stackoverflow.com/a/44977676/7868639 */
    .hide-focus .list-item {
        outline: 0;
    }
</style>

<script>
    window.addEventListener("load", () => {
        main();
    });


    function main() {

        // https://stackoverflow.com/a/44977676/7868639
        function addHideFocus(e) {
            document.documentElement.classList.add("hide-focus");
        };
        function removeHideFocus(e) {
            document.documentElement.classList.remove("hide-focus");
        };
        window.addEventListener("mousedown", addHideFocus, true);
        window.addEventListener("touchstart", addHideFocus, true);
        window.addEventListener("keydown", removeHideFocus, true);

    
        var listItems = document.querySelectorAll(".list-item");
        listItems.forEach(answer => {
            answer.addEventListener("click", onClick);
        });

        function onClick(e) {
            e.currentTarget.classList.add("active");
            listItems.forEach(answer => {
                if(e.currentTarget !== answer) {
                    answer.classList.remove("active");
                }
            });
        }

        document.querySelector("form").addEventListener("submit", (e) => {
            e.preventDefault();
        });
    }
    if(document.readyState === "complete") {
        main();
    }
</script>
```

# Code-Area

```html
<p>Question text. Please select an answer below: </p>
<hr>
<form class="list">
    <label class="list-item">
        Answer A 
        <input class="visuallyHidden" type="radio" name="answer" value="A">
    </label>
    <label class="list-item">
        Answer B
        <input class="visuallyHidden" type="radio" name="answer" value="B">
    </label>
    <br>
    <button type="submit">Submit</button>
</form>

```

# Walkthrough-Area

- 
    # Add an outline on focus

    Use the pseudo-class `:focus-within` to apply the outline. Any color is accepted as long as it makes good contrast with the background. Usage of the browser default focus outline is also accepted: 
    ```css
    .list-item:focus-within {
        outline: 5px auto Highlight;
        outline: 5px auto -webkit-focus-ring-color;
    }
    ```


# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/focus-visible
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/css/C15