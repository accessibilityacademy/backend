---
id: 17
title: Exercise 2.2 - No Keyboard Trap
slug: no-keyboard-trap
---


# Exercise-Area

A developer created a custom textarea which supports the use of the tab key to insert a tab character.  
Keyboard users expect to use the tab key to change the focus to the next element of a webpage. Screen reader users are also dependent on this functionality to navigate around.  

The textarea that the developer created, traps the focus inside the textarea. Despite the fact that they added a "kill" switch with the use of the **Esc** key, website users do not expect this key to untrap the keyboard.  
An explicit instruction is required to advise the user!

# Requirements-Area

- 
    When trapped inside the editor, there are instructions on screen which infom the user that the "***Esc***" key untraps the keyboard.

    ```c#
    var editor = await page.QuerySelectorAsync("#editor");
    await editor.ClickAsync();
    var hasEscapeInstructions = (await page.EvaluateExpressionAsync(@"
        Array.from(
            document.querySelectorAll('*:not(script)')
        ).some(x => {
            for(const node of x.childNodes) {
                if (node.nodeType == Node.TEXT_NODE) {
                    if(node.textContent.toLowerCase().includes('esc')) {
                        return true;
                    }
                }
            }
            return false;
        })
    ")).Value<bool>();

    return hasEscapeInstructions;
    ```

- 
    When a screen reader user focuses on the editor, they hear the instruction regarding the usage of the Esc key. This can be achieved by referencing the instructions with `aria-describedby` or by including the instructions in the `<label>`.

    ```c#
    var editor = await page.QuerySelectorAsync("#editor");
    await editor.ClickAsync();

    var accessibility = await page.Accessibility.SnapshotAsync(new AccessibilitySnapshotOptions()
    {
        InterestingOnly = false
    });
    var hasScreenReaderText = await page.EvaluateFunctionAsync<bool>(
        $@"(rootAccNode) => {{try{{
            {evaluatorHelpers}
            const textbox = acc.find((x) => x.role === 'textbox');

            return textbox.name.toLowerCase().includes('esc') || textbox.description.toLowerCase().includes('esc');
        }} catch(e) {{console.error(e);}}}}",
        accessibility
    );

    return hasScreenReaderText;
    ```

# Hidden-Code-Area

```html
<style>
    #editor {
        margin-bottom: 8px;
    }
</style>
<!-- Credits to gcoulby from SO, https://stackoverflow.com/questions/6637341/use-tab-to-indent-in-textarea#comment116748813_45396754 -->
<script>
    window.addEventListener("load", () => {
        main();
    });
    function main() {
        const toggledEvent = new Event("toggled.textareaeditor");
    
        const state = { enabled: true, blurring: false }; 
        const textarea = document.getElementById("editor");
    
        textarea.addEventListener("keydown", (e) => {
            switch(e.key) {
                case "Escape":
                    e.preventDefault();
                    state.enabled = !state.enabled;
                    textarea.dispatchEvent(toggledEvent);
                    return false;
                case "Enter":
                    if (textarea.selectionStart == textarea.selectionEnd) {
                        // find start of the current line
                        let sel = textarea.selectionStart;
                        let text = textarea.value;
                        while (sel > 0 && text[sel-1] != '\n') {
                            sel--;
                        }
                        
                        let lineStart = sel;
                        while (text[sel] == ' ' || text[sel]=='\t')
                        sel++;
                        
                        if (sel > lineStart) {
                            e.preventDefault();
                            // Insert carriage return and indented text
                            document.execCommand("insertText", false, "\n" + text.substr(lineStart, sel-lineStart));
    
                            // Scroll caret visible
                            textarea.blur();
                            textarea.focus();
                            return false;
                        }
                    }
                    break;
                case "Tab":
                    if(!state.enabled) {
                        state.blurring = true;
                        break;
                    };
                    e.preventDefault();
                    // selection?
                    if (textarea.selectionStart == textarea.selectionEnd) {
                        // These single character operations are undoable
                        if (!e.shiftKey) {
                            document.execCommand("insertText", false, "\t");
                        } else {
                            let text = textarea.value;
                            if (textarea.selectionStart > 0 && text[textarea.selectionStart-1]=="\t") {
                                document.execCommand("delete");
                            }
                        }
                    } else {
                        // Block indent/unindent trashes undo stack.
                        // Select whole lines
                        let selStart = textarea.selectionStart;
                        let selEnd = textarea.selectionEnd;
                        let text = textarea.value;
                        while (selStart > 0 && text[selStart-1] != "\n")
                            selStart--;
                        while (selEnd > 0 && text[selEnd-1]!="\n" && selEnd < text.length)
                            selEnd++;
    
                        // Get selected text
                        let lines = text.substr(selStart, selEnd - selStart).split("\n");
    
                        // Insert tabs
                        for (let i = 0; i < lines.length; i++) {
                            // Don't indent last line if cursor at start of line
                            if (i==lines.length-1 && lines[i].length==0)
                                continue;
    
                            // Tab or Shift+Tab?
                            if (e.shiftKey) {
                                if (lines[i].startsWith("\t"))
                                    lines[i] = lines[i].substr(1);
                                else if (lines[i].startsWith("    "))
                                    lines[i] = lines[i].substr(4);
                            } else {
                                lines[i] = "\t" + lines[i];
                            }
                        }
                        let output = lines.join("\n");
    
                        // Update the text area
                        textarea.value = text.substr(0, selStart) + output + text.substr(selEnd);
                        textarea.selectionStart = selStart;
                        textarea.selectionEnd = selStart + output.length; 
                    }
                    return false;
            }
            state.enabled = true;
            return true;
    
        });
    
        textarea.addEventListener("focus", () => {
            textarea.dispatchEvent(toggledEvent);
        });
    
        textarea.addEventListener("blur", () => {
            if(state.enabled && !state.blurring) {
                textarea.dispatchEvent(toggledEvent);
            }
            state.blurring = false;
            state.enabled = true;
        });
    }
    if(document.readyState === "complete") {
        main();
    }
</script>
```

# Code-Area

```html
<label for="editor">Editor: </label>
<br>
<textarea id="editor" rows="8" cols="40"></textarea>
<br>
<button>Submit</button>
<script>
    let isTrapped = false;
    const editor = document.getElementById("editor");
    editor.addEventListener("toggled.textareaeditor", () => {
        isTrapped = !isTrapped;
        // Add your code below

    });
</script>
```

# Walkthrough-Area
- 
    The easiest way to inform both a user who uses a screen or a screen reader is to simply add the instruction on the textarea label:

    ```html
    <label for="editor">Editor (Press ESC and then Tab to move forward): </label>
    ```

    The solution can be improved by showing the message only when the textarea has actually trapped the focus. For that to work, you can add the following javascript:

    ```js
    editor.addEventListener("toggled.textareaeditor", () => {
        isTrapped = !isTrapped;

        document.querySelector("label").innerText = isTrapped ?
            "Editor (Press ESC and then Tab to move forward): " :
            "Editor: ";
    });
    ```


# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/no-keyboard-trap
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/general/G21
