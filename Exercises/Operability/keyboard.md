---
id: 16
title: Exercise 2.1 - Keyboard Interface and Pointer Cancellation
slug: keyboard-interface-and-pointer-cancellation
---


# Exercise-Area

A developer created a custom link to change the appearance of a page programmatically. They made use of the event `onMouseDown` to trigger the action. By doing so they violated [Success Criterion 2.1.1 Keyboard](https://www.w3.org/TR/WCAG21/#keyboard) because the link cannot be activated from the keyboard, and [Success Criterion 2.5.2 Pointer Cancellation](https://www.w3.org/TR/WCAG21/#pointer-cancellation) because the action is triggered before releasing the mouse button leaving the user without an option to cancel the navigation if they clicked by mistake.

Try to modify the JavaScript event used.

**Note**: For a website to be accessible, **all user actions must be accessible from the keyboard**! By default, any element that is not supposed to do an action when clicked is not focusable with the keyboard. That means that a website that sets click listeners on elements like `<div>`, is not accessible. Setting a [tabindex](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex) on such elements can fix this issue

Note2: A proper navigation implementation also updates the history API of the browser to support the back/forward buttons and provides a proper href. Do not use this routing example in real websites.

# Requirements-Area

- 
    The navigation must not occur if after the user pressed down on the link, they move the mouse away (i.e., like the start of a drag and drop action)”.  
    Such action is typical for a user who changed their mind.

    ```c#
    await page.EvaluateExpressionAsync("document.querySelector('a').href = 'about:blank#'"); // TODO

    var link = await page.QuerySelectorAsync("a");
    var linkPosition = await link.BoundingBoxAsync();

    // move mouse to the center of the link
    await page.Mouse.MoveAsync(linkPosition.X + linkPosition.Width / 2, linkPosition.Y + linkPosition.Height / 2);
    await page.Mouse.DownAsync();

    var contentText = (await page.EvaluateExpressionAsync(
        "document.getElementById('content').textContent"
    )).Value<string>();
    if(contentText != "Page A") return false;

    await page.Mouse.UpAsync();

    contentText = (await page.EvaluateFunctionAsync(
        @"() => {
            return document.getElementById('content').textContent;
        }"
    )).Value<string>();

    if(contentText != "Page B") return false;

    return true;
    ```

- 
    The link must be usable from a keyboard.

    ```c#
    await page.EvaluateExpressionAsync("document.querySelector('a').href = 'about:blank#'"); // TODO

    var contentText = (await page.EvaluateFunctionAsync(
        @"() => {
            return document.getElementById('content').textContent;
        }"
    )).Value<string>();

    var link = await page.QuerySelectorAsync("a");
    await link.PressAsync("Enter");


    var contentTextAfterPress = (await page.EvaluateFunctionAsync(
        @"() => {
            return document.getElementById('content').textContent;
        }"
    )).Value<string>();

    return contentText != contentTextAfterPress;
    ```

# Hidden-Code-Area

```html
<style>
    body.dark a {
        color: #7CB1FF;
    }
    a:-webkit-any-link {
        color: -webkit-link;
    }
    a:-webkit-any-link:active {
        color: -webkit-activelink;
    }
</style>
```

# Code-Area

```html
<a href="#" data-target="pageB">Go to Page B</a>
<div id="content">Page A</div>
<script>
    const link = document.querySelector("a");
    const content = document.getElementById('content');
    link.addEventListener('mousedown', () => {
        switch(link.dataset.target) {
            case "pageA":
                content.innerHTML = "Page A";

                link.innerText = "Go to Page B";
                link.dataset.target = "pageB";
                break;
            case "pageB":
                content.innerHTML = "Page B";

                link.innerText = "Go to Page A";
                link.dataset.target = "pageA";
        }
    });
</script>
```

# Walkthrough-Area
- 
    Changing the event used from `mousedown` to `click` fixes both issues since browsers allow "clicking" of elements with a keyboard by **focusing** the element and **pressing enter**.

    ```js
    link.addEventListener('mousedown', () => {...}); // Wrong event
    link.addEventListener('click', () => {...}); // Correct event
    ```

# Learning-Material-Area
- WCAG: 
    - https://www.w3.org/WAI/WCAG21/Understanding/keyboard
    - https://www.w3.org/WAI/WCAG21/Understanding/pointer-cancellation
- Related Techniques: 
    - https://www.w3.org/WAI/WCAG21/Techniques/failures/F42
    - https://www.w3.org/WAI/WCAG21/Techniques/failures/F54
