---
id: 21
title: Exercise 2.6 - Page Titled
slug: page-titled
---


# Exercise-Area

A news site doesn't display the article title in the title of the browser tab. Instead, it only displays the name of the news site. Such omissions are bad for both Accessibility and Search Engine Optimization. A user cannot easily determine in which part of the website they are on.  
Fix the issue by changing the title.

# Requirements-Area

- 
    
    Use the main article heading as the title.  
    You may keep the name of the website in the title by seperating with `-`. e.g. `Article Title - NewsSite`
    ```js
    return { correct: document.title.includes("XYZ is better than ABC") };
    ```

# Hidden-Code-Area
```html

```

# Code-Area

```html
<head>
    <title>NewsSite</title>
</head>
<body>
    <main>
        <article>
            <h1>
                XYZ is better than ABC
            </h1>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin elementum erat metus, eu pulvinar erat mattis ut. Vivamus turpis ligula, eleifend eu elementum ac, faucibus at leo. Curabitur accumsan, tellus sit amet accumsan laoreet, augue augue porta sapien, vel dapibus leo justo sed enim. Etiam eleifend neque id leo scelerisque, eu tincidunt neque feugiat. Maecenas rutrum tellus a urna euismod, vestibulum pharetra risus tincidunt. Sed vestibulum varius vestibulum. Nulla dignissim posuere cursus. Nunc in aliquam libero. In nunc orci, condimentum vel auctor ac, suscipit vitae libero. In tempus convallis felis, a aliquet orci varius at. Nam sagittis mauris lacus, eu lobortis ex sodales sed. Nam sit amet orci non lorem cursus molestie et condimentum sapien. Fusce suscipit nisl mi, ut elementum leo lobortis sed. Sed semper odio erat, vitae elementum felis tristique ac. Morbi hendrerit in libero eu rutrum. Vivamus consectetur nisi at neque venenatis, eu pulvinar orci ultricies.
            </p>
            <p>
                Fusce lorem augue, fringilla in tempus eget, condimentum nec quam. Proin scelerisque suscipit odio vitae dictum. Nullam ut convallis tortor, a bibendum purus. Etiam blandit dapibus pharetra. Etiam dapibus mi sed orci finibus, eget lobortis ipsum tempor. Vivamus a dolor gravida, commodo est nec, dapibus elit. Nullam eget enim vel justo interdum pellentesque. Etiam urna ipsum, feugiat sit amet sapien quis, mollis elementum ipsum. Mauris a magna libero. Aenean elementum diam in odio viverra, ut venenatis urna viverra. Nulla vestibulum tellus vel est aliquet ultrices. Proin fringilla dui rhoncus arcu egestas, vel iaculis lorem accumsan. Morbi sit amet gravida mi. Aliquam nec purus hendrerit, commodo nunc vel, eleifend urna.
            </p>
        </article>
    </main>
</body>
```

# Walkthrough-Area
- 
    Change the title:
    ```html
    <title>XYZ is better than ABC - NewsSite</title>
    ```
    
    


# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/page-titled
- Related Techniques: 
    - https://www.w3.org/WAI/WCAG21/Techniques/general/G88
    - https://www.w3.org/WAI/WCAG21/Techniques/html/H25
