---
id: 24
title: Exercise 2.9 - Headings and Labels
slug: headings-and-labels
---


# Exercise-Area

The previous website's heading of the article does not convey enough information for users to conceive what they are about to read.

# Requirements-Area

- 
    Change the heading to better describe the content. You can either use the heading: "Lorem ipsum explanation" or "What is lorem ipsum?"
    ```js
    const headingText = document.querySelector("h1").innerText.toLowerCase();

    return { 
        correct: 
            headingText.includes("lorem ipsum") && 
            (headingText.includes("explanation") || headingText.includes("what is"))
    };
    ```

# Hidden-Code-Area
```html
<style>
    body.dark a {
        color: #7CB1FF;
    }
    a:-webkit-any-link {
        color: -webkit-link;
    }
    a:-webkit-any-link:active {
        color: -webkit-activelink;
    }
</style>
```

# Code-Area

```html
<div>
    <h1>
        Lorem ipsum
    </h1>
    <p>
        In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.
    </p>
    <a href="#" aria-label="read more about lorem ipsum">Read more</a>
</div>
```

# Walkthrough-Area
- 
    Make the heading describe the content by adding the word "explanation":

    ```html
    <h1>
        Lorem ipsum explanation
    </h1>
    ```


# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/headings-and-labels
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/general/G130
- WAI tutorial on headings: https://www.w3.org/WAI/tutorials/page-structure/headings/