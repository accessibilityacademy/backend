---
id: 22
title: Exercise 2.7 - Focus Order
slug: focus-order
---


# Exercise-Area

A developer created a dialog to confirm a destructive user action. They did the right thing to hide the dialog from screen readers when it's not open by using aria-hidden. They also correctly used the [`role="alertdialog"`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_alertdialog_role) to grab the attention of screen readers on the dialog when it is activated.  

Even though they thought their account actions page is accessible, they made two serious mistakes.  
First of all, the focus order of the buttons is wrong. According to the DOM hierarchy, after clicking on the delete account button, the element that gets focused is the reset account button, instead of the reset inside the dialog. This irritates and slows down keyboard users, because it forces them to navigate the whole page before reaching the dialog action buttons.   
A second mistake is that for an alert dialog to properly work and for screen readers to announce it, an action must be in focus inside the dialog.  

# Requirements-Area

- 
    When opening the **delete account** confirmation dialog, you can use the keyboard for navigation.  
    When returning from the dialog, the focus continues from where you left off.
    ```c#
    await page.ClickAsync("#delete-btn");
    await page.Keyboard.PressAsync("Tab");
    var isFocusedInside = await page.EvaluateExpressionAsync<bool>(
        "Array.from(document.querySelectorAll('.dialog-actions button')).some(btn => document.activeElement === btn)"
    );
    await page.Keyboard.PressAsync("Escape");
    await page.Keyboard.PressAsync("Tab");
    var isFocusedOutside = await page.EvaluateExpressionAsync<bool>(
        "document.activeElement === document.getElementById('reset-btn')"
    );

    return isFocusedInside && isFocusedOutside;
    ```

- 
    When opening the dialog, the first action button is focused!
    ```js
    document.getElementById("delete-btn").click();
    return { correct: document.activeElement === document.querySelector(".dialog-actions button") };
    ```

# Hidden-Code-Area
```html
<style>
    .backdrop {
        background-color: rgba(0, 0, 0, 0.5);
        position: fixed;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        display: none;
        justify-content: center;
        align-items: center;
    }

    .dialog {
        color: black;
        padding: 16px;
        background-color: white;
    }

    .dialog-actions {
        margin-top: 8px;
        margin-left: auto;
        width: fit-content;
    }

    h1, button {
        margin-bottom: 8px;
    }
</style>
<script>
    window.addEventListener("load", () => {
        main();
    }); 
    function main() {
        const backdrop = document.querySelector(".backdrop");
        const dialog = document.querySelector(".dialog");
        const dialogButtons = document.querySelectorAll(".dialog-actions button");

        backdrop.addEventListener("click", () => {
            hideDialog();
        });
        dialog.addEventListener("click", (e) => {
            e.stopPropagation();
        });
        document.addEventListener("keydown", (e) => {
            if(e.code === "Escape") {
                hideDialog();
            }
        });

        Array.from(dialogButtons).forEach(button => {
            button.addEventListener("click", (e) => {
                hideDialog();
            });
        });

        function hideDialog() {
            backdrop.style.display = "none";
            dialog.setAttribute("aria-hidden", true);
            dialog.removeAttribute("aria-modal");
            dialog.removeAttribute("role");
        }
    }

    if(document.readyState === "complete") {
        main();
    }
</script>
```

# Code-Area

```html
<main>
    <h1>Account actions</h1>
    <button id="delete-btn">Delete account</button>
    <br>
    <button id="reset-btn">Reset Account</button>
    <br>
    <button>Change Password</button>
</main>

<div class="backdrop" role="presentation">
    <div 
        class="dialog"
        aria-hidden="true"
        aria-label="Delete confirmation"
        aria-describedby="dialog-description"
    >
        <span id="dialog-description">Are you sure you want to delete your account?</span>
        <div class="dialog-actions">
            <button>Cancel</button>
            <button>Delete account</button>
        </div>
    </div>
</div>
<script>
    const backdrop = document.querySelector(".backdrop");
    const dialog = document.querySelector(".dialog");
    const deleteBtn = document.getElementById("delete-btn");

    const cancelActionButton = document.querySelector(".dialog-actions button");

    deleteBtn.addEventListener("click", (e) => {
        unhideDialog();
    });

    function unhideDialog() {
        backdrop.style.display = "flex";
        dialog.setAttribute("aria-hidden", false);
        dialog.setAttribute("aria-modal", true);
        dialog.setAttribute("role", "alertdialog");
        
        // Write your code below
    }

</script>
```

# Walkthrough-Area
- 
    # Handle focus order
    The simplest way to handle the focus order is to put the dialog next to the button in the hierarchy:
    ```html
    <button id="delete-btn">Delete account</button>
    <div class="backdrop" role="presentation"> <!-- Notice modal position -->
        ...
    </div>
    <br>
    <button id="reset-btn">Reset Account</button>
    <br>
    <button>Change Password</button>
    ```

    Another solution would be to use the tabindex property on the action buttons and on the delete account button.
- 
    # Focus the first action button
    To focus the first action button add this line of code in the javascript unhide function:
    ```js
    function unhideDialog() {
        ...
        
        cancelActionButton.focus();
    }
    ```


# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/focus-order
- Related Techniques: 
    - https://www.w3.org/WAI/WCAG21/Techniques/failures/F85
    - https://www.w3.org/WAI/WCAG21/Techniques/client-side-script/SCR26
- Read more on authoring Modal Dialogs: https://www.w3.org/TR/wai-aria-practices/examples/dialog-modal/dialog

