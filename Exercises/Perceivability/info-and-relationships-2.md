---
id: 3
title: Exercise 1.3 - Info and Relationships part 2
slug: info-and-relationships-2
---


# Exercise-Area

A developer created a webpage for answering quizzes. As an input, they decided to use a clickable list.
By doing so, they failed to address the accessibility issues of not having a standard `<input>` tag.

Try to correct the code by adding a hidden `<input type="radio">` or by using **ARIA attributes**.

Note: Solving this exercise with ARIA requires editing the JavaScript provided. Other considerations, such as proper input handling, will be taught in a later exercise!

# Requirements-Area
-   Make sure that a screen reader can understand that there are two **grouped** radio inputs that are labelled as *Answer A* and *Answer B*.

    ```js
    let radio1;
    let radio2;

    const radioInputs = document.querySelectorAll('input[type="radio"]');
    let properlyGrouped = true;
    if(radioInputs.length > 0) {
        radio1 = acc.find(e => e.role === "radio" && e.name === "Answer A");
        radio2 = acc.find(e => e.role === "radio" && e.name === "Answer B");
        properlyGrouped = !!radioInputs[0].getAttribute("name") && radioInputs[0].getAttribute("name") === radioInputs[1].getAttribute("name");
    } else {
        const group = acc.find(e => e.role === "radiogroup");
        radio1 = group.find(e => e.role === "radio" && e.name === "Answer A");
        radio2 = group.find(e => e.role === "radio" && e.name === "Answer B");
    }

    return { correct: !!radio1 && !!radio2 && properlyGrouped };
    ```


-   Make sure to either remove the list or change its meaning through ARIA. Do not use a list of radio buttons as that creates unnecessary confusion.

    ```js
    const listBullet = acc.find(e => e.role === "list" || e.role === "listitem");
    return { correct: !listBullet };
    ```

-   Only one radio input must be selected at a time. Check that the underlying radio input gets selected properly.
  
    If using ARIA you must make sure that only one element receives the attribute `aria-selected=true`. In this case, JavaScript modifications are required.

    ```js
    const listElements = document.querySelectorAll(".list-item");
    const radioInputs = document.querySelectorAll('[role="radio"], input[type="radio"]');
    listElements[0].click();
    const radio1Selected = radioInputs[0]?.checked || radioInputs[0]?.attributes["aria-selected"].value.toLowerCase() === "true";

    listElements[1].click();

    const radio2Selected = radioInputs[1]?.checked || radioInputs[1]?.attributes["aria-selected"].value.toLowerCase() === "true";
    const radio1Unselected = !radioInputs[0]?.checked || radioInputs[0]?.attributes["aria-selected"].value.toLowerCase() === "false";
    return { correct: !!radio1Selected && !!radio2Selected && !!radio1Unselected };
    ```

-   Do not change the appearance of the list.
    
    If you are using an underlying `<input type="radio">`, hide it from the screen by using the following css:
    ```css
    .visuallyHidden {
        position: absolute;
        clip: rect(0, 0, 0, 0);
    }
    ```

    ```js
    const radioInputs = document.querySelectorAll('input[type="radio"]');
    let isHidden = true;
    radioInputs.forEach(radio => {
        const styles = getComputedStyle(radio);
        if(styles.position !== "absolute" || styles.clip !== "rect(0px, 0px, 0px, 0px)") {
            isHidden = false;
        }
    });

    const listItems = document.querySelectorAll(".list-item");
    listItems[0].click();

    const listDims = [listItems[0].getBoundingClientRect(), listItems[1].getBoundingClientRect()];

    const container = document.querySelector("body");

    return { 
        correct: isHidden &&
        listItems[0].classList.contains("active") &&
        listDims[1].top - listDims[0].top === listDims[0].height &&
        container.getBoundingClientRect().width === listDims[0].width
    };
    ```

# Hidden-Code-Area

```html
<style>
    p {
        font-size: 1.25rem;
        margin: 0;
    }

    .list {
        padding: 0;
        margin: 0;
    }

    .list-item {
        padding: 8px 16px;
        cursor: pointer !important;
    }

    .list-item:hover, .active {
        background-color: rgba(var(--contrast-color-rgb), 0.1);
    }
</style>
```

# Code-Area

```html
<p>Question text. Please select an answer below: </p>
<hr>
<ul class="list">
    <li class="list-item">Answer A</li>
    <li class="list-item">Answer B</li>
</ul>

<script>
    var listItems = document.querySelectorAll(".list-item");
    listItems.forEach(answer => {
        answer.addEventListener("click", onClick);
    });

    function onClick(e) {
        e.currentTarget.classList.add("active");
        listItems.forEach(answer => {
            if(e.currentTarget !== answer) {
                answer.classList.remove("active");
            }
        });
    }
</script>
```
# Walkthrough-Area
-   # Add radio inputs

    You can either add a physical radio input or use the **role** attribute like the example below:

    ```html
    <form>
        <label class="list-item">
            Answer A
            <input type="radio" name="answer">
        </label>
        <label class="list-item">
            Answer B
            <input type="radio" name="answer">
        </label>
    </form>
    ```

    or

    ```html
    <ul role="radiogroup" class="list">
      <label class="list-item" role="radio">
          Answer A
      </label>
      <label class="list-item" role="radio">
          Answer B
      </label>
    </ul>
    ```

    Notice that if you are using an underlying input, the inputs must have the same name attribute. If using ARIA, the inputs must be grouped with a parent radiogroup element.

-   # Handle selection

    If you choose to use an underlying input then this step is handled by the browser. By default only one radio input can be selected at a time as long as they have the same name attribute.

    If you choose the aria solution then you must handle the `aria-selected` value manually with JavaScript.
    An example onClick function is given below:

    ```js
    function onClick(e) {
        e.currentTarget.classList.add("active");
        e.currentTarget.setAttribute('aria-selected', "true");
        listItems.forEach(answer => {
            if(e.currentTarget !== answer) {
                answer.setAttribute('aria-selected', "false");
                answer.classList.remove("active");
            }
        });
    }
    ```
-   # Fix styling
    
    To hide the radio input from the screen the provided CSS can be used by adding the visuallyHidden class in the HTML code.
    Don't forget to declare the class styles inside the styles editor!
    ```html
    <input class="visuallyHidden" type="radio" name="answer">
    ```

    To make the different answers be displayed vertically, you can use the following CSS:
    ```css
    label {
        display: block;
    }
    ```

# Learning-Material-Area
- WCAG: 
  - https://www.w3.org/WAI/WCAG21/Understanding/info-and-relationships
  - https://www.w3.org/WAI/WCAG21/Understanding/name-role-value
- Related Techniques: 
  - https://www.w3.org/WAI/WCAG21/Techniques/aria/ARIA17
  - https://www.w3.org/WAI/WCAG21/Techniques/html/H44
  - https://www.w3.org/WAI/WCAG21/Techniques/failures/F59
- Custom controls: https://www.w3.org/WAI/tutorials/forms/custom-controls/
- Labeling controls: https://www.w3.org/WAI/tutorials/forms/labels/
- Hiding elements: https://www.w3.org/WAI/tutorials/forms/labels/#note-on-hiding-elements
