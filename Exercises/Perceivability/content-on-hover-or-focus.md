---
id: 15
title: Exercise 1.15 - Content on Hover or Focus
slug: content-on-hover-or-focus
---


# Exercise-Area

A developer did not like the default tooltip that is offered by browsers and instead used bootstrap's tooltip component. Bootstrap's tooltip component is not by default accessible and they failed to take corrective actions to mitigate the issues.

Because it takes a lot of time and thought to solve the issue, the required code is given below. Basically the trigger of the tooltip is switched from the default that is `hover focus` into `manual` and the events are handled manually.

Try to hover the tooltip before and after you paste the code below into the javascript editor to see the difference it makes!

```js
var tooltipTriggerList = Array.from(
    document.querySelectorAll('[data-bs-toggle="tooltip"]')
);
var tooltipList = tooltipTriggerList.map((tooltipTriggerEl) => {
    return new bootstrap.Tooltip(tooltipTriggerEl, { trigger: 'manual' });
})

tooltipTriggerList.forEach(tooltipTriggerEl => {
    var tooltip = bootstrap.Tooltip.getInstance(tooltipTriggerEl);
    tooltipTriggerEl.addEventListener('mouseenter', () => {
        tooltip.show();
        document.querySelector('.tooltip').addEventListener('mouseleave', () => {
            if(
                !tooltipTriggerEl.parentElement.querySelector(
                    `${tooltipTriggerEl.tagName}:hover`
                )
            ) {
                tooltip.hide();
            }
        });
    });

    tooltipTriggerEl.addEventListener('mouseleave', () => {
        if(!document.querySelector('.tooltip:hover')) {
            tooltip.hide();
        }
    });

    tooltipTriggerEl.addEventListener('focus', () => {
        tooltip.show();
    });

    tooltipTriggerEl.addEventListener('blur', () => {
        tooltip.hide();
    });
});

window.addEventListener('keydown', (event) => {
    if(event.code === 'Escape') {
        tooltipTriggerList.forEach(tooltipTriggerEl => {
            bootstrap.Tooltip.getInstance(tooltipTriggerEl).hide();
        });
    }
});
```

# Requirements-Area

- 
    The text of the **bootstrap tooltip** must be hoverable. You should be able to select and copy the text of the tooltip.
    Note: The native tooltip doesn't have to satisfy this requirement!

    ```c#
    var button = await page.QuerySelectorAsync("#bs-tooltip");
    await button.HoverAsync();
    var tooltip = await page.QuerySelectorAsync(".tooltip");
    await tooltip.HoverAsync();
    await Task.Delay(100);
    return await tooltip.IsIntersectingViewportAsync();
    ```

- 
    The tooltip is dismissable from the keyboard with the escape button.

    ```c#
    var button = await page.QuerySelectorAsync("#bs-tooltip");
    await button.HoverAsync();
    var tooltip = await page.QuerySelectorAsync(".tooltip");
    await page.Keyboard.PressAsync("Escape");
    await Task.Delay(200);
    return !(await tooltip.IsIntersectingViewportAsync());
    ```

# Hidden-Code-Area

```html
<style>

</style>
```

# Code-Area

```html
<style>
.button {
    padding: 8px;
}

body { 
    margin: 8px !important;
}
</style>
<!-- Import bootstrap -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>


Native tooltip: 
<button  title="refresh">
    <i class="fas fa-undo button"></i>
</button>

<div class="mt-2">
    Bootstrap's tooltip: 
    <button id="bs-tooltip" title="refresh" data-keyboard="true" data-bs-toggle="tooltip" data-bs-placement="right">
        <i class="fas fa-undo button"></i>
    </button>
</div>
<script>
    var tooltipTriggerList = Array.from(
        document.querySelectorAll('[data-bs-toggle="tooltip"]')
    );
    var tooltipList = tooltipTriggerList.map((tooltipTriggerEl) => {
        return new bootstrap.Tooltip(tooltipTriggerEl);
    })
</script>
```

# Walkthrough-Area
- 
    The solution is given on the exercise explanation. You can just copy and paste the following JavaScript:
    ```js
    var tooltipTriggerList = Array.from(
        document.querySelectorAll('[data-bs-toggle="tooltip"]')
    );
    var tooltipList = tooltipTriggerList.map((tooltipTriggerEl) => {
        return new bootstrap.Tooltip(tooltipTriggerEl, { trigger: 'manual' });
    })

    tooltipTriggerList.forEach(tooltipTriggerEl => {
        var tooltip = bootstrap.Tooltip.getInstance(tooltipTriggerEl);
        tooltipTriggerEl.addEventListener('mouseenter', () => {
            tooltip.show();
            document.querySelector('.tooltip').addEventListener('mouseleave', () => {
                if(
                    !tooltipTriggerEl.parentElement.querySelector(
                        `${tooltipTriggerEl.tagName}:hover`
                    )
                ) {
                    tooltip.hide();
                }
            });
        });

        tooltipTriggerEl.addEventListener('mouseleave', () => {
            if(!document.querySelector('.tooltip:hover')) {
                tooltip.hide();
            }
        });

        tooltipTriggerEl.addEventListener('focus', () => {
            tooltip.show();
        });

        tooltipTriggerEl.addEventListener('blur', () => {
            tooltip.hide();
        });
    });

    window.addEventListener('keydown', (event) => {
        if(event.code === 'Escape') {
            tooltipTriggerList.forEach(tooltipTriggerEl => {
                bootstrap.Tooltip.getInstance(tooltipTriggerEl).hide();
            });
        }
    });
    ```

# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/content-on-hover-or-focus
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/client-side-script/SCR39
- Bootstrap tooltip options: https://getbootstrap.com/docs/5.0/components/tooltips/#options
