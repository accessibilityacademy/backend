---
id: 6
title: Exercise 1.6 - Sensory characteristics
slug: sensory-characteristics
---


# Exercise-Area

This website distinguishes product availability solely by shape and color. Even though there are instructions that explain the different icons, such sensory characteristics cannot be perceived by some users. A text alternative is missing for those people.

# Requirements-Area

- 
    Availability icons must have an alternative text that properly describes them as ***Available within 2 business days*** or 
    ***Available within 10 business days***. Instead of having to type the same alt text that already exists on the page in the form of instructions, try using [aria-labelledby](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_aria-labelledby_attribute) to reference the text.

    ```js
    const availabilities = acc.findAll(x =>
        x.role === "img" &&
        (x.name.includes("Available within 2 business days") || x.name.includes("Available within 10 business days"))
    );
    return { correct: availabilities[0].name.includes("2") && availabilities[1].name.includes("10") };
    ```

- 
    Instructions must be [hidden](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_aria-hidden_attribute) from screen readers to avoid confusion. Do not hide the instructions from the screen!

    ```js
    const instructionElement = acc.find(x =>
        x.name === "Instructions" ||
        x.role !== "img" && (x.name.includes("Available within 2 business days") || x.name.includes("Available within 10 business days"))
    );

    const instructions = document.querySelectorAll('.instructions');
    if(instructions.length !== 2) {
        return { correct: false };
    }
    const boundingBox = instructions[0].getBoundingClientRect();
    const isVisible = boundingBox.width > 10 && boundingBox.height > 10 && getComputedStyle(instructions[0]).visibility !== 'hidden';

    return { correct: !instructionElement && isVisible };
    ```

# Hidden-Code-Area

```html
<style>
    .product {
        border: 1px solid var(--contrast-color);
        padding: 8px;
        display: flex;
    }

    .placeholder-image {
        width: 100px;
        height: 100px;
        border: 1px solid var(--contrast-color);
        font-size: 24px;
        align-items: center;
        justify-content: center;
        flex-shrink: 0;
        display: inline-flex;
        order: -1;
    }

    .content {
        margin: 0 8px;
    }

    h2 {
        margin: 0;
    }

    .actions {
        margin-left: auto;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
    }

    i {
        align-self: flex-end;
    }

    .icon-green {
        color: green;
    }

    .icon-red {
        color: red;
    }

    .product:not(:last-child) {
        margin-bottom: 8px;
    }

    .instructions-heading {
        margin: 0;
    }

    .instructions:last-child {
        margin-bottom: 8px;
    }
</style>
```

# Code-Area

```html
<div>
    <h2 class="instructions-heading">Instructions</h2>
    <div class="instructions">
        <i class="fas fa-check-circle icon-green"></i>
        <span>Available within 2 business days<span>
    </div>
    <div class="instructions">
        <i class="fas fa-exclamation-circle icon-red"></i>
        <span>Available within 10 business days</span>
    </div>
</div>

<div class="product">
    <div class="content">
        <h2>Product 1</h2>
        <p>Product description. Product description. Product description. Product description.</p>
    </div>
    <div class="placeholder-image" role="img" aria-label="placeholder image">100x100</div>
    <div class="actions">
        <!-- Availability Icon -->
        <i 
            class="fas fa-check-circle fa-2x icon-green" 
            role="img"
        ></i>
        <button>Add to cart</button>
    </div>
</div>
<div class="product">
    <div class="content">
        <h2>Product 2</h2>
        <p>Product description. Product description. Product description. Product description.</p>
    </div>
    <div class="placeholder-image" role="img" aria-label="placeholder image">100x100</div>
    <div class="actions">
        <!-- Availability Icon -->
        <i 
            class="fas fa-exclamation-circle fa-2x icon-red" 
            role="img"
        ></i>
        <button>Add to cart</button>
    </div>
</div>
```

# Walkthrough-Area
- # Label the availability icons
    To label the availability icons, the easiest option is to add an id on the availability text and to reference that with the attribute `aria-labelledby`:
    ```html
    <div class="instructions">
        <i class="fas fa-check-circle icon-green"></i>

        <!-- Add the ID on the span element. It's on the line 5 of the HTML editor -->
        <span id="availability-2">Available within 2 business days<span>

    </div>
    <div class="instructions">
        <i class="fas fa-exclamation-circle icon-red"></i>

        <!-- Add the ID on the span element. It's on the line 9 of the HTML editor -->
        <span id="availability-10">Available within 10 business days</span>

    </div>
    ```
    
    ```html
    <div class="actions">
        <!-- Availability Icon. It's on the line 20 of the HTML editor -->
        <!-- Notice the aria-labelledby that is referencing the span of the instructions -->
        <i
            class="fas fa-check-circle fa-2x icon-green"
            role="img"
            aria-labelledby="availability-2"
        ></i>
        <button>Add to cart</button>
    </div>

    ...

    <div class="actions">
        <!-- Availability Icon. It's on the line 35 of the HTML editor -->
        <!-- Notice the aria-labelledby that is referencing the span of the instructions -->
        <i
            class="fas fa-exclamation-circle fa-2x icon-red"
            role="img" 
            aria-labelledby="availability-10"
        ></i>
        <button>Add to cart</button>
    </div>
    ```


- # Hide Instructions
    Instructions for an icon that a person who is using a screen reader can't see, are useless.
    To remove them, the `aria-hidden` attribute can be used on the div that contains the instructions like the example below:
    ```html
    <div aria-hidden="true">
        <h2 class="instructions-heading">Instructions</h2>
        ...
    </div>
    ```

# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/sensory-characteristics
- Related Techniques:
    - https://www.w3.org/WAI/WCAG21/Techniques/general/G96
    - https://www.w3.org/WAI/WCAG21/Techniques/failures/F14 