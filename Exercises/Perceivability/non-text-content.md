---
id: 1
title: Exercise 1.1 - Non-text Content
slug: non-text-content
---


# Exercise-Area

The following website has an accessibility issue regarding the styling of a list with non-text content. 

A screen reader will **not** be able to understand that the image is used only for styling and **will create unnecessary confusion** to a user.

Note: When there isn't any alternative text on an image, a user will likely hear the screen reader recite the full path and file name of the image!

# Requirements-Area
- 
    Change the code so that the images are **ignored** by screen readers.

    ```js
    const images = document.querySelectorAll("img");

    if(images.length) {
        // Solution 1 is to have alt=""

        const correctImages = [];

        images.forEach(img => {
            if(img.attributes.alt?.value === "") {
                correctImages.push(img);
            }
        });
        
        //Solution 2 is to use aria to hide the images (aria-hidden or role="none/presentation")
        document.querySelectorAll("img[role]").forEach(image => {
            if(correctImages.indexOf(image) > -1) return;

            const role = image.attributes.role.value.toLowerCase();
            if(role === "none" || role === "presentation") {
                correctImages.push(image);
            }
        });

        document.querySelectorAll('[aria-hidden="true"] img, img[aria-hidden="true"]').forEach(image => {
            if(correctImages.indexOf(image) > -1) return;

            correctImages.push(image);
        });

        return { correct: correctImages.length === images.length }
    }
    //Solution 3 is to completely remove the images
    return { correct: true }
    ```
- 
    Make sure not to change the way that the list looks like. The decorated bullets must **not** be removed!

    ```js
    const images = document.querySelectorAll("img");
    if(images.length === 2) {
        return { correct: true };
    }
    const ul = document.querySelector("ul");
    if(getComputedStyle(ul).listStyleImage.indexOf('decorative') !== -1) { // Secret solution
        return { correct: true };
    }
    return { correct: false };
    ```


# Hidden-Code-Area

```html
<style>
    img {
        width: 1rem;
        height: 1rem;
        vertical-align: text-bottom;
    }

    ul {
        padding: 16px 32px;
        background-color: var(--bg-dark);
        color: var(--text-dark-color);
    }
</style>
```

# Code-Area

```html
<ul style="list-style: none">
    <li>
        <img src="/decorative-bullet.svg">
        Item 1
    </li>
    <li>
        <img src="/decorative-bullet.svg">
        Item 2
    </li>
</ul>
```


# Walkthrough-Area

- 
    # Hide images
    The easiest way to hide the images from screen readers is by using an empty alt attribute on **both** images of the exercise like the example below:

    ```html
    <li>
        <img src="/decorative-bullet.svg" alt="">
        Item 1
    </li>
    <li>
        <img src="/decorative-bullet.svg" alt="">
        Item 2
    </li>
    ```

# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/non-text-content
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/html/H67
- WAI tutorial on decorative images: https://www.w3.org/WAI/tutorials/images/decorative/
