---
id: 10
title: Exercise 1.10 - Contrast (Minimum)
slug: minimum-contrast
---


# Exercise-Area

A website's dark-themed cards have a problem. Links are hard to read because of insufficient contrast.

Fix the contrast issue by changing the link's color.
A very useful tool for finding compatible colors is [contrast checker](https://webaim.org/resources/contrastchecker/?fcolor=0000FF&bcolor=222222).
Example color to try: 
<span style="color: #60A0FF; font-size: 1.25rem; font-weight: bold; background-color: #303030; padding: 4px;">\#60A0FF</span>

**Note**: Developer tools can help you detect such problems on your websites easily. Familiarize yourself with your browser's dev tools.  
Every browser has a slightly different way to help you find low-contrast text:
- [Chromium based browsers](https://developers.google.com/codelabs/devtools-cvd#3)
- [Firefox](https://developer.mozilla.org/en-US/docs/Tools/Accessibility_inspector#check_for_accessibility_issues)

# Requirements-Area

-   Change the link color to provide a contrast of at least 4.5:1

    ```js
    const link = document.querySelector("a");
    const container = document.querySelector(".card");

    const linkColor = getComputedStyle(link).color.replace(/[^\d,]/g, '').split(',').map(x => Number(x));
    const containerBackgroundColor = getComputedStyle(container).backgroundColor.replace(/[^\d,]/g, '').split(',').map(x => Number(x));

    return { correct: contrast(linkColor, containerBackgroundColor) >= 4.5 };
    ```


# Hidden-Code-Area

```html
<style>
.card {
    padding: 16px;
    background-color: #222;
    border-radius: 4px;
    box-shadow: 0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%);
    color: white;
}

a {
    color: blue;
}
</style>
```

# Code-Area

```html
<div class="card">
    For more information see
    <a href="#">
        example link
    </a>.
    Information provided there does not apply for XYZ.
</div>
```

# Walkthrough-Area
-   Change link color 
    
    ```html
    <a href="#" style="color: #60A0FF">
        example link
    </a>
    ```


# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/contrast-minimum
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/general/G18