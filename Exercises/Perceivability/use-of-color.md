---
id: 8
title: Exercise 1.8 - Use of Color
slug: use-of-color
---


# Exercise-Area

A website of articles has some links inside of paragraphs. The design they used does not have an underline on the links when they are not hovered making them hard or impossible to distinguish for users with color difficulty.

There are multiple ways to make links distinguishable from text. Some options include:
- Using a color with a contrast of at least 3:1 compared to the rest of the text.
- Using an underlying that is displayed all the time, not only when the link is hovered.
- Using bold characters.
- Using italic characters.

A very useful tool for finding compatible colors is [contrast checker](https://webaim.org/resources/contrastchecker/?fcolor=FFFFFF&bcolor=7CB1FF).

# Requirements-Area

-   Make sure that a user who can't see colors can distinguish the link from the rest of the text.

    ```js
    const link = document.querySelector('a');
    const linkStyles = getComputedStyle(link);

    const linkColor = linkStyles.color.replace(/[^\d,]/g, '').split(',').map(x => Number(x));

    const linkColorContrast = contrast(linkColor, [255, 255, 255]);


    return { correct:
        Number(linkStyles.fontWeight) !== 400 ||
        linkStyles.fontStyle !== 'normal' ||
        linkStyles.textDecorationLine !== 'none' ||
        linkColorContrast >= 3
    };
    ```

# Hidden-Code-Area

```html
<style>
    a {
        color: #7CB1FF;
    }

    body.dark, body {
        background-color: #2E2E2E;
        color: white;
    }
</style>
```

# Code-Area

```html
<style>
a {
    text-decoration: none;
}

a:hover {
    text-decoration: underline;
}
</style>
<p>
      In publishing and graphic design, Lorem ipsum is a
      <a href="https://en.wikipedia.org/wiki/Filler_text">placeholder text</a>
      commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.
</p>
```

# Walkthrough-Area

- 
    One solution for when an underline doesn't look good is to change the color to have enough contrast from the rest of the text:
    ```css
    a {
        color: #4D94FF;
    }
    ```

# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/use-of-color
- Related Technique: 
    - https://www.w3.org/WAI/WCAG21/Techniques/failures/F73
