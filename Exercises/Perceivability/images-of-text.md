---
id: 11
title: Exercise 1.11 - Images of Text
slug: images-of-text
---


# Exercise-Area

The developer who previously used an image as a heading violated the [WCAG Criterion 1.4.6](https://www.w3.org/TR/WCAG21/#images-of-text) since the decoration they used on the text can be achieved with CSS.

The font they used to create the image is ***Brush Script MT***.  
The font size they used is ***32***.  
The color they used is ***\#9696FF***. 


# Requirements-Area
- 
    Remove the image to replace it with a similar CSS implementation.

    ```js
    const img = document.querySelector("img");
    const heading = document.querySelector("h1");
    return { correct: !img && !!heading };
    ```

- 
    The heading content must not change.

    ```js
    const heading = acc.find(x => x.role === "heading" && x.name === "Chapter 1");
    return { correct: !!heading };
    ```


- 
    The heading's font family must remain the same.

    ```js
    const headingFontFamily = getComputedStyle(document.querySelector("h1")).fontFamily;
    return { correct: headingFontFamily.toLowerCase().indexOf("brush script mt") === 1 };
    ```

- 
    The heading's font size and color must remain the same.

    ```js
    const headingStyles = getComputedStyle(document.querySelector("h1"));
    const fontSize = Number(headingStyles.fontSize.match(/[0-9]+/g)[0])
    const color = headingStyles.color.replace(/[^\d,]/g, '').split(',').map(x => Number(x));
    return { correct: fontSize === 32 && color[0] === 150 && color[1] === 150 && color[2] === 255 };
    ```
# Hidden-Code-Area

```html
<style>
    @font-face {
        font-family: 'Brush Script MT';
        font-style: normal;
        font-weight: 400;
        src: url("//db.onlinewebfonts.com/t/33bc06ea126d3ff79886277122f1f510.woff2") format("woff2"),
            url("//db.onlinewebfonts.com/t/33bc06ea126d3ff79886277122f1f510.woff") format("woff"),
            url("//db.onlinewebfonts.com/t/33bc06ea126d3ff79886277122f1f510.ttf") format("truetype")
    }
</style>
```

# Code-Area

```html
<h1>
    <img src="/chapter1.png" alt="Chapter 1">
</h1>
<p>This is some placeholder text for chapter 1</p>
```

# Walkthrough-Area
- 
    To properly replicate the image heading with CSS, the following styles can be used:
    ```css
    h1 {
        font-size: 28px;
        color: #9696FF;
        font-family: Brush Script MT;
    }
    ```

    Don't forget to remove the image from the HTML and replace it with the text:
    ```html
    <h1>
        Chapter 1
    </h1>
    <p>This is some placeholder text for chapter 1</p>
    ```

# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/images-of-text
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/css/C22 
- WAI tutorial on images of text: https://www.w3.org/WAI/tutorials/images/textual/
