---
id: 4
title: Exercise 1.4 - Meaningful Sequence
slug: meaningful-sequence
---


# Exercise-Area

The following website has an accessibility issue regarding the order in which a screen reader or a bot will read the text. 

A screen reader will read the text from top to bottom as it appears in the markup. Any positioning made with css or tables **will be ignored**.

The reading order from this example would be:
-   Accessibility Academy top!
-   Accessibility Academy gets you to the

# Requirements-Area
- 
    Change the code so that the sequence of text in the advertisement is meaningful and makes sense

    ```js
    const ad = document.querySelector("table");

    const text = ad?.textContent.replaceAll("\n", "").replaceAll(/\s+/g, " ").trim();
    if(text === "Accessibility Academy gets you to the top!") {
        return { correct: true };
    }
    return { correct: false };
    ```

- 
    Make sure not to change the way the ad looks like!

    ```js
    const tableCells = document.querySelectorAll("table td");
    if(tableCells.length !== 3) {
        return { correct: false };
    }
    const getsYouToThe = tableCells[tableCells.length - 2];
    getsYouToThe.innerHTML = '<span id="test1">' + getsYouToThe.innerHTML + '</span>';
    const top = tableCells[tableCells.length - 1];
    top.innerHTML = '<span id="test2">' + top.innerHTML + '</span>';
    return { 
        correct: document.getElementById("test1").getBoundingClientRect().bottom ===
            document.getElementById("test2").getBoundingClientRect().bottom
    };
    ```


# Hidden-Code-Area

```html
<style>
    img {
        width: 100px;
        height: 100px;
    }

    td {
        width: 100px;
    }
</style>
```

# Code-Area

```html
<table>
    <tr>
        <td><img src="/logo2.svg" alt="Accessibility Academy"></td>
        <td rowspan="2" valign="bottom">top!</td>
    </tr>
    <tr>
        <td>Accessibility Academy gets you to the</td>
    </tr>
</table>
```

# Walkthrough-Area
- 
    Change the order of the elements so that they make sense when they are read from top to bottom:
    ```html
    <table>
        <tr>
            <td><img src="/logo2.svg" alt="Accessibility Academy"></td>
        </tr>
        <tr>
            <td>Accessibility Academy gets you to the</td>
            <td valign="bottom">top!</td>
        </tr>
    </table>
    ```
# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/meaningful-sequence
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/failures/F49