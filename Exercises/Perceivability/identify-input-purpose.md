---
id: 7
title: Exercise 1.7 - Identify Input Purpose
slug: identify-input-purpose
---


# Exercise-Area

A website's registration form does not have a working autocomplete. Having no autocomplete functionality can slow down users with a disability and can even annoy users with no disabilities (e.g., password managers cannot reliably determine what each input represents).

**Note**: Most browsers attempt to understand what autocomplete attribute is correct for each input based on the label text. This technique does not work reliably in all languages. For example, an internationalized website will for sure have trouble. Weird label names will also make the autocomplete functionality stop working. Having the autocomplete attribute on all inputs should be considered **mandatory**!

**Hint**: When making changes to the HTML code, try to reload the preview page to see what autocomplete functionality your browser offers with each attribute!

# Requirements-Area

-   Add the proper autocomplete attribute on the username input.

    ```js
    return { correct: document.querySelectorAll("input")[0].getAttribute("autocomplete") === "username" };
    ```

-   Add the proper autocomplete attribute on the email input.

    ```js
    return { correct: document.querySelectorAll("input")[1].getAttribute("autocomplete") === "email" };
    ```

-   Add the proper autocomplete attribute on the full name input.

    ```js
    return { correct: document.querySelectorAll("input")[2].getAttribute("autocomplete") === "name" };
    ```

-   Add the proper autocomplete attribute on the password inputs.

    ```js
    return {
        correct: document.querySelectorAll("input")[3].getAttribute("autocomplete") === "new-password" &&
            document.querySelectorAll("input")[4].getAttribute("autocomplete") === "new-password"
    };
    ```

# Hidden-Code-Area

```html
<style>

form {
    display: grid;
    grid-template-columns: 0fr 1fr;
    column-gap: 8px;
    row-gap: 8px;
}

label {
    white-space: nowrap;
}

input {
    max-width: 140px;
}

</style>
```

# Code-Area

```html
<h1>Register for a new account</h1>
<form>
    <label for="1">Username:</label>
    <input id="1" type="text">

    <label for="2">Email:</label>
    <input id="2" type="text">

    <label for="3">Full name:</label>
    <input id="3" type="text">

    <label for="4">Password:</label>
    <input id="4" type="password">

    <label for="5">Confirm password:</label>
    <input id="5" type="password">
</form>
```

# Walkthrough-Area
-   The correct autocomplete attributes for this form are: **username**, **email**, **name**, **new-password**, **new-password**  
    The complete code is shown below:
    ```html
    <form>
        <label for="1">Username:</label>
        <input id="1" type="text" autocomplete="username">

        <label for="2">Email:</label>
        <input id="2" type="text" autocomplete="email">

        <label for="3">Full name:</label>
        <input id="3" type="text" autocomplete="name">

        <label for="4">Password:</label>
        <input id="4" type="password" autocomplete="new-password">

        <label for="5">Confirm password:</label>
        <input id="5" type="password" autocomplete="new-password">
    </form>
    ```


# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/identify-input-purpose
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/html/H98