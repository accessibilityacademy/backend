---
id: 9
title: Exercise 1.9 - Use of Color part 2
slug: use-of-color-2
---


# Exercise-Area

A developer created a webpage for answering quizzes. To give the correct answer they decided to use the color green for correct and red for false.
For the blind users they correctly used a non-visible text alternative in accordance to the [WCAG guideline 1.1.1](https://www.w3.org/TR/WCAG21/#text-alternatives) for non-text content. The implementation of that can be seen in the JavaScript editor.

They thought they did all the actions necessary to make the webpage accessible but they did not. They failed to account for the 1% of the population that cannot distinguish the two colors.

# Requirements-Area

-   Make sure that a user with **protanopia** or **deuteranopia** can use the quiz application effectively.
    There are muliple ways to achieve that. You must at least change the function `onSubmit` found in the javascript editor.
    Assume that the first answer is correct and the second answer is wrong. Use your imagination on how to convey the information.  
    You could do one of the following:
    - Add some text below that either contains the word ***correct*** or ***wrong***.
    - Add the font awesome icon `fa-check-circle` and `fa-times-circle`. Don't forget the class `far`, the `aria-label` attribute and `role="img"`.
    - Simply remove the visuallyHidden class from the span that was added for the screen readers. Find it in the javascript code.  
      This option is not the best design-wise.

    ```js
    function validate(correct) {
        let result = Array.from(
            document.querySelectorAll("*:not(.visuallyHidden) > *:not(.visuallyHidden):not(script)")
        ).filter(x => {
            for(const node of x.childNodes) {
                if (node.nodeType == Node.TEXT_NODE) {
                    if(node.textContent.toLowerCase().includes(correct ? "correct" : "wrong")) {
                        return true;
                    }
                }
            }
            return false;
        });

        const imgExists = document.querySelector(correct ? 'i.fa-check-circle' : 'i.fa-times-circle');
        const visibleTextExists = result.length > 0;

        return !!imgExists || visibleTextExists;
    }

    document.querySelectorAll("input")[0].click();
    document.querySelector("button").click();
    const isPassingOnCorrectAnswer = validate(true);

    document.querySelectorAll("input")[1].click();
    document.querySelector("button").click();
    const isPassingOnWrongAnswer = validate(false);

    return { correct: isPassingOnCorrectAnswer && isPassingOnWrongAnswer };
    ```

# Hidden-Code-Area

```html
<style>
    p {
        font-size: 1.25rem;
        margin: 0;
    }

    .list {
        padding: 0;
        margin: 0;
    }

    .list-item {
        padding: 8px 16px;
        cursor: pointer !important;
        display: block;
    }

    .list-item:hover {
        background-color: rgba(var(--contrast-color-rgb), 0.1);
    }

    .list-item:focus-within {
        outline: 5px auto Highlight;
        outline: 5px auto -webkit-focus-ring-color;
    }

    .list-item.active {
        background-color: rgba(var(--contrast-color-rgb), 0.2);
    }

    .visuallyHidden {
        position: absolute;
        clip: rect(0, 0, 0, 0);
    }

    /* https://stackoverflow.com/a/44977676/7868639 */
    .hide-focus .list-item {
        outline: 0;
    }
</style>

<script>
    window.addEventListener("load", () => {
        main();
    });


    function main() {

        // https://stackoverflow.com/a/44977676/7868639
        function addHideFocus(e) {
            document.documentElement.classList.add("hide-focus");
        };
        function removeHideFocus(e) {
            document.documentElement.classList.remove("hide-focus");
        };
        window.addEventListener("mousedown", addHideFocus, true);
        window.addEventListener("touchstart", addHideFocus, true);
        window.addEventListener("keydown", removeHideFocus, true);

    
        var listItems = document.querySelectorAll(".list-item");
        listItems.forEach(answer => {
            answer.addEventListener("click", onClick);
        });

        function onClick(e) {
            e.currentTarget.classList.add("active");
            listItems.forEach(answer => {
                if(e.currentTarget !== answer) {
                    answer.classList.remove("active");
                }
            });
        }
    }
    if(document.readyState === "complete") {
        main();
    }
</script>
```

# Code-Area

```html
<p>Question text. Please select an answer below: </p>
<hr>
<form class="list">
    <label class="list-item">
        Answer A 
        <input class="visuallyHidden" type="radio" name="answer" value="A">
    </label>
    <label class="list-item">
        Answer B
        <input class="visuallyHidden" type="radio" name="answer" value="B">
    </label>
    <br>
    <button type="submit">Submit</button>
</form>

<script>
    document.querySelector("form").addEventListener("submit", onSubmit);
    var inputs = document.querySelectorAll("input");
    var labels = document.querySelectorAll("label");
    function onSubmit(e) {
        inputs.forEach(input => {
            if(input.checked) {
                input.parentElement.style.backgroundColor = input === inputs[0] ?
                    "rgba(0, 255, 0, 0.4)":
                    "rgba(255, 0, 0, 0.4)";
                document.getElementById("cue")?.remove();

                // Try to change the code below
                var hiddenCue = document.createElement("span");
                hiddenCue.id = "cue";
                hiddenCue.classList.add("visuallyHidden");
                hiddenCue.innerText = input === inputs[0] ? "Correct." : "Wrong.";


                input.parentElement.prepend(hiddenCue);
                labels.forEach(label => {
                    if(label !== input.parentElement) {
                        label.style.backgroundColor = null;
                    }
                })
            }
        });
        e.preventDefault();
    }
</script>

```

# Walkthrough-Area

- 
    # Add visual cues

    One easy way to convey the information about the correctness of an answer is to add an icon instead of just using color and a hidden text.

    Instead of having:
    ```html
    <span id="cue" class="visuallyHidden">Correct. </span>
    Answer A
    ```
    the elements below could be used:
    ```html
    <i id="cue" role="img" class="far fa-check-circle" aria-label="correct">
    Answer A
    ```

    This change must be made in the javascript code since the visuallyHidden element is rendered onSubmit dynamically. The following code is one solution:
    ```js
    ...
    var cue = document.createElement("i");
    cue.id = "cue";
    cue.className=`far ${ input === inputs[0] ? 'fa-check-circle' : 'fa-times-circle'}`;
    cue.setAttribute("role", "img");
    cue.setAttribute("aria-label", input === inputs[0] ? "correct" : "wrong");
    ...
    ```

# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/use-of-color
- Related Technique: 
    - https://www.w3.org/WAI/WCAG21/Techniques/general/G14
    - https://www.w3.org/WAI/WCAG21/Techniques/failures/F13
