---
id: 12
title: Exercise 1.12 - Reflow
slug: reflow
---


# Exercise-Area

A developer created a template for a website that is very hard to use in mobile devices.  
Some of the header links are not visible, the card text needs horizontal scrolling to read the text and the contact form is hard to use on landscape because the keyboard and the header hide the input from the screen.

# Requirements-Area
- 
    With a small viewport, the header must be accessible. Normally you would want to create a sidebar or a dropdown menu for mobile devices but for this exercise it is enough to simply allow the overflowing links to **wrap**.

    ```c#
    await page.SetViewportAsync(new ViewPortOptions()
    {
        Width=320,
        Height=800
    });
    var links = await page.QuerySelectorAllAsync("header li");
    var firstLink = await links[0].BoundingBoxAsync();
    var lastLink = await links[links.Count() - 1].BoundingBoxAsync();

    return firstLink.Width + firstLink.X < 320 && lastLink.Width + lastLink.X < 320;
    ```

- 
    With a small width i.e. from a mobile device, the content of the cards must be readable.
    Make sure that the card width can become as small as required so that no scrolling is required on a 320px wide screen.  
    Do not change the appearance on large screens! 

    ```c#
    await page.SetViewportAsync(new ViewPortOptions()
    {
        Width=1400,
        Height=800
    });

    var firstCard = await page.QuerySelectorAsync(".card");
    var img = await page.QuerySelectorAsync(".card .placeholder-image");
    var content = await page.QuerySelectorAsync(".card p");

    var isOkDesktop =  (await firstCard.BoundingBoxAsync()).Width > 400 &&
        (await img.BoundingBoxAsync()).Y == (await content.BoundingBoxAsync()).Y;

    await page.SetViewportAsync(new ViewPortOptions()
    {
        Width=320,
        Height=800
    });

    var firstCardBoundingBox = await firstCard.BoundingBoxAsync();
    var imgBoundingBox = await img.BoundingBoxAsync();
    var contentBoundingBox = await content.BoundingBoxAsync();

    var isOkMobile = 
        firstCardBoundingBox.X + firstCardBoundingBox.Width < 320 &&
        contentBoundingBox.Width > 150 &&
        imgBoundingBox.Y < contentBoundingBox.Y;

    return isOkDesktop && isOkMobile;
    ```

- 
    With a small height i.e. when using the website from a phone in landscape and an open keyboard, the input must be visible. That means that the navigation bar must [un-fix](https://www.w3.org/WAI/WCAG21/Techniques/css/C34) itself from the top.

    ```c#
    await page.SetViewportAsync(new ViewPortOptions()
    {
        Width=600,
        Height=100
    });
    var positionStyle = (await page.EvaluateExpressionAsync("getComputedStyle(document.querySelector('header')).position;")).Value<string>();
    return positionStyle != "sticky";
    ```

# Hidden-Code-Area

```html
<style>
.nav-links {
    list-style-type: none;
    display: flex;
    padding: 0;
    margin: 0;
}

.nav-links > li {
    border: 1px solid white;
    padding: 16px 4px;
    border-radius: 4px;
    white-space: nowrap;
    margin-right: 8px;
}

label {
    vertical-align: top;
}

body {
    margin: 0;
}

header {
    padding: 8px 16px;
    background-color: black;
    color: white;
}

.form-control {
    margin-bottom: 8px;
}

main {
    margin: 8px 16px;
}

.card {
    border: 1px solid #777;
    background-color: var(--light-paper);
    padding: 16px;
    margin-right: 8px;
}

.card > h3 {
    margin-top: 0;
}

.card-content > p {
    margin: 0;
}

.cards-container {
    display: flex;
    overflow-y: auto;
    padding-bottom: 8px;
}

.placeholder-image {
    width: 150px;
    height: 150px;
    border: 1px solid var(--contrast-color);
    font-size: 24px;
    align-items: center;
    justify-content: center;
    flex-shrink: 0;
    display: inline-flex;
    margin-right: 8px;
}

</style>
```

# Code-Area

```html
<style>

header {
    position: sticky;
    top: 0;
}

.nav-links {
    display: flex;
}

.card {
    min-width: 500px;
}

.card-content {
    display: flex;
}

</style>
<header>
    <nav>
        <ul class="nav-links">
            <li>Homepage</li>
            <li>Webpage 1</li>
            <li>Webpage 2</li>
            <li>Webpage 3</li>
            <li>Webpage 4</li>
            <li>Webpage 5</li>
        </ul>
    </nav>
</header>

<main>

    <h2>Placeholder text</h2>
    <div class="cards-container">
        <div class="card">
            <h3>Content Title 1</h3>
            <div class="card-content">
                <div class="placeholder-image" role="img" aria-label="placeholder image">150x150</div>
                <p>
                    This is some content text that the user must be able to read without having to scroll left and right all the time. It's annoying for all users whether they are handicapped or not. Just don't do it. Make sure that the card width can become as small as required so that no scrolling is required on a 320px wide screen.
                </p>
            </div>
        </div>
        <div class="card">
            <h3>Content Title 2</h3>
            <div class="card-content">
                <div class="placeholder-image" role="img" aria-label="placeholder image">150x150</div>
                <p>
                    This is some content text that the user must be able to read without having to scroll left and right all the time. It's annoying for all users whether they are handicapped or not. Just don't do it. Make sure that the card width can become as small as required so that no scrolling is required on a 320px wide screen.
                </p>
            </div>
        </div>
        <div class="card">
            <h3>Content Title 3</h3>
            <div class="card-content">
                <div class="placeholder-image" role="img" aria-label="placeholder image">150x150</div>
                <p>
                    This is some content text that the user must be able to read without having to scroll left and right all the time. It's annoying for all users whether they are handicapped or not. Just don't do it. Make sure that the card width can become as small as required so that no scrolling is required on a 320px wide screen.
                </p>
            </div>
        </div>
    </div>


    <form>
        <h2>Contact form</h2>
        <div class="form-control">
            <label for="fullName">FullName: </label>
            <input name="fullName" id="fullName">
        </div>
        <div class="form-control">
            <label for="message">Message: </label>
            <textarea name="message" id="message"></textarea>
        </div>

        <button type="submit">submit</button>
    </form>

</main>
```

# Walkthrough-Area
- 
    # Allow header links to wrap

    To make the header accessible to mobile devices, allow the links to wrap in a new line with the following css:

    ```css
    .nav-links {
        display: flex;
        flex-wrap: wrap;
    }
    ```
- 
    # Fit at least one card to screen

    To make the cards responsive the following css can be used:
    ```css
    .card {
        max-width: 500px; /* Allow the card to be less than 500 pixels wide */
    }

    .card-content {
        display: flex;
        flex-wrap: wrap; /* Allow the content to wrap below the image */
    }

    .card-content p {
        flex-grow: 1; /* On bigger screens allow the paragraph to grow to above 150 pixels wide */
        flex-basis: 150px; /* Allow the paragraph to be a minimum of 150 pixels wide before wrapping */
    }
    ```
- 
    # Un-fix header on small heights

    To un-fix the header on small heights the following css can be used:
    ```css
    @media (min-height: 250px) {
        header {
            position: sticky;
            top: 0;
        }
    }
    ```

# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/reflow
- Related Techniques:
    - https://www.w3.org/WAI/WCAG21/Techniques/css/C31
    - https://www.w3.org/WAI/WCAG21/Techniques/css/C34
