---
id: 5
title: Exercise 1.5 - Meaningful Sequence part 2
slug: meaningful-sequence-2
---


# Exercise-Area

An e-shop has a card styling for displaying products. Even though the interface is straightforward for people with eyesight, there is a problem with screen readers. Because the heading is not the first element and there is an image before the heading, it's easy for that image to be missed.

# Requirements-Area

- 
    The heading must be the first element that is read by a screen reader followed by the product description and then the image.

    Hint: You can use the [flex layout to change the order of the elements](https://css-tricks.com/snippets/css/a-guide-to-flexbox/#order) displayed on screen!
    ```js
    const heading = acc.find(x =>
        x.role === "heading" &&
        x.name === "Product 1"
    );

    const image = acc.find(x => 
        x.role === "img" &&
        x.name === "placeholder image"
    );
    const description = acc.find(x =>
        x.role === "paragraph"
    );

    const cardChildren = heading.parent.children;

    return { correct: cardChildren.indexOf(heading) < cardChildren.indexOf(description) && cardChildren.indexOf(description) < cardChildren.indexOf(image) };
    ```

- 
    Make sure not to change the way the product information is displayed!

    ```js
    const image = document.querySelector('.placeholder-image').getBoundingClientRect();
    const heading = document.querySelector('h2').getBoundingClientRect();
    const content = document.querySelector('.content').getBoundingClientRect();

    return { correct: heading.left > image.right && heading.left === content.left && heading.top === content.top };
    ```

# Hidden-Code-Area

```html
<style>
    .product {
        border: 1px solid var(--contrast-color);
        padding: 8px;
        display: flex;
    }

    .placeholder-image {
        width: 100px;
        height: 100px;
        border: 1px solid var(--contrast-color);
        font-size: 24px;
        align-items: center;
        justify-content: center;
        flex-shrink: 0;
        display: inline-flex;
    }

    .content {
        margin: 0 8px;
    }

    h2 {
        margin: 0;
    }

    .actions {
        margin-left: auto;
        display: flex;
        flex-direction: column;
        justify-content: flex-end;
    }

    img {
        align-self: flex-end;
        color: green;
    }

    .product:not(:last-child) {
        margin-bottom: 8px;
    }

    .instructions-heading {
        margin: 0;
    }

    .instructions:last-child {
        margin-bottom: 8px;
    }
</style>
```

# Code-Area

```html
<style>
.placeholder-image {
    display: inline-flex;
}
</style>
<div class="product">
    <div class="placeholder-image" role="img" aria-label="placeholder image">100x100</div>
    <div class="content">
        <h2>Product 1</h2>
        <p>Product description. Product description. Product description. Product description.</p>
    </div>
    <div class="actions">
        <button>Add to cart</button>
    </div>
</div>
```

# Walkthrough-Area

-   # Change the sequence of the elements
    First step is to change the HTML so that the content is before the image:
    ```html
    <div class="product">
      <div class="content">
          <h2>Product 1</h2>
          <p>Product description. Product description. Product description. Product description.</p>
      </div>
      <div class="placeholder-image" role="img" aria-label="placeholder image">100x100</div>
      <div class="actions">
          <button>Add to cart</button>
      </div>
    </div>
    ```
-   # Restore the original look
    To restore the original look of the card the following css can be used:
    ```css
    .placeholder-image {
        display: inline-flex;
        order: -1; /* Put image before all other flex elements */
    }
    ```
# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/meaningful-sequence
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/css/C6
- WAI developing tips: https://www.w3.org/WAI/tips/developing/#reflect-the-reading-order-in-the-code-order
- Flex layout: https://css-tricks.com/snippets/css/a-guide-to-flexbox
