---
id: 14
title: Exercise 1.14 - Text Spacing
slug: text-spacing
---


# Exercise-Area

A developer created a caption that is positioned on the bottom center of an image. What they failed to account for is that some people prefer to use custom letter and word spacing to improve their reading experience.

Try to set the following css to see the failure:
```css
.caption {
    letter-spacing: 0.12rem;
    word-spacing: 0.16rem;
}
```


# Requirements-Area

- 
    The text must not be clipped or overlapped. The text must stay positioned inside and at the bottom of the image. Using `text-overflow: ellipsis` should be ***avoided*** for labels!

    ```js
    const caption = document.querySelector(".caption");
    caption.style.letterSpacing = "0.12rem";
    caption.style.wordSpacing = "0.16rem";

    const captionDims = caption.getBoundingClientRect();

    caption.innerHTML = '<span id="caption-wrapper">' + caption.innerHTML + '</span>';

    const captionText = document.getElementById("caption-wrapper");
    const captionTextDims = captionText.getBoundingClientRect();

    const captionTextOverflowedHeight = captionText.scrollHeight;
    const captionTextOverflowedWidth = captionText.scrollWidth;

    //Reset styles
    caption.style.removeProperty("letter-spacing");
    caption.style.removeProperty("word-spacing");

    return { correct: 
        captionDims.height === captionTextDims.height &&
        captionDims.width === captionTextDims.width &&
        captionTextOverflowedHeight === captionDims.height &&
        captionTextOverflowedWidth <= captionDims.width
    };
    ```

- 
    The look must not change on the default letter/word spacing. Make sure that the height starts from **24 pixels** and that you have ***removed*** the letter-spacing and word-spacing css that you used to see the issue.

    ```js
    const container = document.querySelector('.image-container').getBoundingClientRect();
    const caption = document.querySelector(".caption").getBoundingClientRect();

    const isCaptionOnBottomOfContainer = caption.top + caption.height + 1 === container.bottom;
    const isCaptionCorrectHeight = caption.height === 24;

    return { correct: isCaptionOnBottomOfContainer && isCaptionCorrectHeight };
    ```

# Hidden-Code-Area

```html
<style>
    .image-container {
        width: 200px;
        height: 200px;
        border: 1px solid var(--contrast-color);
        position: relative;
    }

    .placeholder-image {
        width: 100%;
        height: 100%;
        font-size: 24px;
        align-items: center;
        justify-content: center;
        flex-shrink: 0;
        display: inline-flex;
    }
    h2 {
        margin: 0;
    }
    .caption {
        color: white;
    }

</style>
```

# Code-Area

```html
<style>
.caption {
    display: inline-flex;
    justify-content: center;
    position: absolute;
    left: 0;
    bottom: 0;

    background: linear-gradient(rgba(0, 0, 0, 0), #000);

    width: 100%;
    height: 24px;
}
</style>
<div class="image-container">
    <div class="placeholder-image" role="img" aria-label="placeholder image">200x200</div>
    <span class="caption">Placeholder image caption</span>
</div>
<h2>Placeholder heading</h2>
```

# Walkthrough-Area
- 
    The accepted method is to simply allow the caption to expand to multiple lines:
    ```css
    .caption {
        height: 24px; /* Remove this line */
        min-height: 24px; /* Add this line */
    }
    ```
    You can always avoid such issues by never locking the size of an element to a specific value!

# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/text-spacing
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/failures/F104
