---
id: 2
title: Exercise 1.2 - Info and Relationships
slug: info-and-relationships
---


# Exercise-Area

A developer did not like the default `<h1></h1>` look and decided to use their own styling shown below:
  
```css
.heading1 {
    font-size: 175%;
    font-weight: bold;
    margin: 0;
}
```

They also used an image as a heading incorrectly.

However, these choices make the website difficult to navigate with assistive technologies.


# Requirements-Area
- 
    Make sure that the first heading is marked properly. Do not change the appearance. Use a level 1 heading.

    ```js
    const headingAcc = acc.find(e => e.role === "heading" && e.name === "Introduction" && e.level === 1);
    const headingDom = document.querySelector(".heading1");
    const fontSize = Number(getComputedStyle(headingDom).fontSize.match(/[0-9]+/g)[0])
    return { correct: !!headingAcc && !!headingDom && fontSize === 28 };
    ```


- 
    Make sure that the image is marked as a heading. Do not change the appearance. Use a level 1 heading.

    ```js
    const imgHeading = acc.find(e => e.role === "heading" && e.name === "Chapter One" && e.level === 1);
    const imgAnywhere = acc.find(e => e.role === "img" && e.name === "Chapter One");
    const imgChildOfHeading = imgHeading.find(e => e.role === "img" && e.name === "Chapter One");
    const imgDom = document.querySelector("img");
    return { correct: !!imgHeading && imgAnywhere === imgChildOfHeading && !!imgDom }
    ```


# Hidden-Code-Area

```html
<style>
</style>
```

# Code-Area

```html
<style>
.heading1 {
    font-size: 175%;
    font-weight: bold;
    margin: 0;
}
</style>
<p class="heading1">Introduction</p>
<p>This is an introduction to the website</p>

<img src="/chapter1.png" alt="Chapter One">
<p>This is some placeholder text for chapter 1</p>
```

# Walkthrough-Area
- 
    One way to properly mark the headings is to change the `<p>` element with the `<h1>` element. Stylings would still apply and there would be no visual difference.
    
    For the image there are two alternative solutions.
    - One possible solution would be to wrap it with an h1 like the example below:
      ```html
      <h1>
          <img src="/chapter1.png" alt="Chapter One">
      </h1>
      ```

    - For an existing website it may not be easy to change the markup of the document since such changes could break the CSS rules. For such cases, the `role` and `aria-level` attributes can be used:
      ```html
      <img role="heading" aria-level="1" src="/chapter1.png" alt="Chapter One">
      ```

# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/info-and-relationships
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/html/H42
- WAI tutorial on headings: https://www.w3.org/WAI/tutorials/page-structure/headings/
