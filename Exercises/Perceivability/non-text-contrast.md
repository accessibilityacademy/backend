---
id: 13
title: Exercise 1.13 - Non-text Contrast
slug: non-text-contrast
---


# Exercise-Area

This e-shop has an issue when viewed on a dark theme that can affect people with vision deficiencies. It provides an icon in addition to color information for the availability of the products so that they are accessible for people with colorblindness. The problem is that the icons do not have enough contrast. Once again, the [contrast checker tool](https://webaim.org/resources/contrastchecker/?fcolor=008000&bcolor=424242) is very helpful.

<!-- 80FF80?? TODO  -->

You can try the following colors: 

- <span style="color: #00A700; font-size: 1.25rem; font-weight: bold; background-color: #303030; padding: 4px;">\#00A700</span>
- <span style="color: #FF5050; font-size: 1.25rem; font-weight: bold; background-color: #303030; padding: 4px;">\#FF5050</span>


# Requirements-Area

- 
    Both of the **available within 2 days** icons must have a contrast of at least 3:1

    ```js
    const availableIcons = document.querySelectorAll("i.fa-check-circle");
    const container = document.querySelector("body"); // TODO

    const icon1 = getComputedStyle(availableIcons[0]).color.replace(/[^\d,]/g, '').split(',').map(x => Number(x));
    const icon2 = getComputedStyle(availableIcons[1]).color.replace(/[^\d,]/g, '').split(',').map(x => Number(x));
    const containerBackgroundColor = getComputedStyle(container).backgroundColor.replace(/[^\d,]/g, '').split(',').map(x => Number(x));

    return { correct: contrast(icon1, containerBackgroundColor) >= 3 && contrast(icon2, containerBackgroundColor) };
    ```

- 
    Both of the **available within 10 days** icons must have a contrast of at least 3:1

    ```js
    const availableIcons = document.querySelectorAll("i.fa-exclamation-circle");
    const container = document.querySelector("body"); // TODO

    const icon1 = getComputedStyle(availableIcons[0]).color.replace(/[^\d,]/g, '').split(',').map(x => Number(x));
    const icon2 = getComputedStyle(availableIcons[1]).color.replace(/[^\d,]/g, '').split(',').map(x => Number(x));
    const containerBackgroundColor = getComputedStyle(container).backgroundColor.replace(/[^\d,]/g, '').split(',').map(x => Number(x));

    return { correct: contrast(icon1, containerBackgroundColor) >= 3 && contrast(icon2, containerBackgroundColor) };
    ```

# Hidden-Code-Area

```html
<style>
    body {
        background-color: #424242;
        color: white;
    }

    .product {
        border: 1px solid white;
        padding: 8px;
        display: flex;
    }

    .placeholder-image {
        width: 100px;
        height: 100px;
        border: 1px solid white;
        font-size: 24px;
        align-items: center;
        justify-content: center;
        flex-shrink: 0;
        display: inline-flex;
        order: -1;
    }

    .content {
        margin: 0 8px;
    }

    h2 {
        margin: 0;
    }

    .actions {
        margin-left: auto;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
    }

    i {
        align-self: flex-end;
    }

    .product:not(:last-child) {
        margin-bottom: 8px;
    }

    .instructions-heading {
        margin: 0;
    }

    .instructions:last-child {
        margin-bottom: 8px;
    }
</style>
```

# Code-Area

```html
<style>
.icon-green {
    color: green;
}

.icon-red {
    color: red;
}
</style>
<div aria-hidden="true">
    <h2 class="instructions-heading">Instructions</h2>
    <div class="instructions">
        <i class="fas fa-check-circle icon-green"></i>
        <span id="availability-2">Available within 2 business days<span>
    </div>
    <div class="instructions">
        <i class="fas fa-exclamation-circle icon-red"></i>
        <span id="availability-10">Available within 10 business days</span>
    </div>
</div>

<div class="product">
    <div class="content">
        <h2>Product 1</h2>
        <p>Product description. Product description. Product description. Product description.</p>
    </div>
    <div class="placeholder-image" role="img" aria-label="placeholder image">100x100</div>
    <div class="actions">
        <i
        
            class="fas fa-check-circle fa-2x icon-green"
            role="img"
            aria-labelledby="availability-2"
        ></i>
        <button>Add to cart</button>
    </div>
</div>
<div class="product">
    <div class="content">
        <h2>Product 2</h2>
        <p>Product description. Product description. Product description. Product description.</p>
    </div>
    <div class="placeholder-image" role="img" aria-label="placeholder image">100x100</div>
    <div class="actions">
        <i 
            class="fas fa-exclamation-circle fa-2x icon-red"
            role="img" 
            aria-labelledby="availability-10"
        ></i>
        <button>Add to cart</button>
    </div>
</div>
```

# Walkthrough-Area

-   Change colors of icons 
    
    ```css
    .icon-green {
        color: #00A700;
    }

    .icon-red {
        color: #FF5050;
    }
    ```


# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/non-text-contrast
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/general/G207