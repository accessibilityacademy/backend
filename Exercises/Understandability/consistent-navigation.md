---
id: 31
title: Exercise 3.5 - Consistent Navigation
slug: consistent-navigation
---


# Exercise-Area

The following website's navigation menu links are not consistent across all pages, making navigation confusing for all users.

Note: A proper navigation implementation also updates the history API of the browser to support the back/forward buttons and provides a proper href. Do not use this routing example in real websites.

# Requirements-Area

-   The links have consistent order across all webpages.

    ```js
    function validateLinkOrder(links) {
        return (
            links[0].getAttribute('link') === '/' &&
            links[1].getAttribute('link') === '/page1' &&
            links[2].getAttribute('link') === '/page2'
        );
    }

    const homepageLinks = document.querySelectorAll("a[link]");
    if(!validateLinkOrder(homepageLinks)) {
        return { correct: false };
    }

    homepageLinks[1].href = 'about:blank#'; // TODO
    homepageLinks[1].click();

    const firstPageLinks = document.querySelectorAll("a[link]");
    if(!validateLinkOrder(firstPageLinks)) {
        return { correct: false };
    }

    firstPageLinks[2].href = 'about:blank#'; // TODO
    firstPageLinks[2].click();

    const secondPageLinks = document.querySelectorAll("a[link]");
    if(!validateLinkOrder(secondPageLinks)) {
        return { correct: false };
    }
    
    return { correct: true };
    ```


# Hidden-Code-Area

```html
<style>
    .nav-links {
        list-style-type: none;
        display: flex;
        padding: 0;
        margin: 0;
        flex-wrap: wrap;
    }

    .nav-links li {
        border: 1px solid white;
        padding: 16px 4px;
        border-radius: 4px;
        white-space: nowrap;
    }

    .nav-links > * {
        margin-right: 8px;
    }

    body {
        margin: 0;
    }

    header {
        padding: 8px 16px;
        background-color: black;
    }

    a {
        color: #7CB1FF;
    }

    @media (min-height: 250px) {
        header {
            position: sticky;
            top: 0;
        }
    }

    label {
        display: block;
    }

    #content {
        margin: 8px;
    }

    .main-content {
        display: flex;
        align-items: center;
        justify-content: center;
        height: 450px;
        border: 1px solid black;
    }

    .active {
        background-color: rgba(255, 255, 255, 0.3);
    }
</style>
<script>
    window.addEventListener("load", () => {
        main();
    });

    function main() {
        const container = document.getElementById('/').parentElement;
        const pages = {
            '/': document.getElementById('/'),
            '/page1': document.getElementById('/page1'),
            '/page2': document.getElementById('/page2')
        };
        Array.from(document.querySelectorAll('a[link]')).forEach(link => {
            link.addEventListener('click', () => {
                const targetPage = link.getAttribute('link');
                loadPage(targetPage);
            });
        });

        function loadPage(page) {
            for(const availPage of Object.keys(pages)) {
                if(availPage !== page) {
                    pages[availPage].remove();
                }
            }
            container.appendChild(pages[page]);
        }

        loadPage('/');
    }

    if(document.readyState === "complete") {
        main();
    }
</script>
```

# Code-Area

```html
<!-- Homepage -->
<div id="/">
    <header>
        <nav>
            <ul class="nav-links">
                <a href="#" class="active" link="/"><li>Homepage</li></a>
                <a href="#" link="/page1"><li>Webpage 1</li></a>
                <a href="#" link="/page2"><li>Webpage 2</li></a>
            </ul>
        </nav>
    </header>

    <main id="content">
        <div class="main-content">
            Main website content.
        </div>
    </main>
</div>

<!-- Webpage 1 -->
<div id="/page1">
    <header>
        <nav>
            <ul class="nav-links">
                <a href="#" link="/"><li>Homepage</li></a>
                <a href="#" class="active" link="/page1"><li>Webpage 1</li></a>
                <a href="#" link="/page2"><li>Webpage 2</li></a>
            </ul>
        </nav>
    </header>

    <main id="content">
        <div class="main-content">
            Webpage 1 content.
        </div>
    </main>
</div>

<!-- Webpage 2 -->
<div id="/page2">
    <header>
        <nav>
            <ul class="nav-links">
                <a href="#" link="/"><li>Homepage</li></a>
                <a href="#" class="active" link="/page2"><li>Webpage 2</li></a>
                <a href="#" link="/page1"><li>Webpage 1</li></a>
            </ul>
        </nav>
    </header>

    <main id="content">
        <div class="main-content">
            Webpage 2 content.
        </div>
    </main>
</div>


```

# Walkthrough-Area

- # Fix the order of the links
    The order of the links on the Webpage 2 is inconsistent with the rest of the website. You can fix it like this:
    ```html
    <!-- Webpage 2 -->
    <div id="/page2">
        <header>
            <nav>
                <ul class="nav-links">
                    <a href="#" link="/"><li>Homepage</li></a>
                    <a href="#" link="/page1"><li>Webpage 1</li></a>
                    <a href="#" class="active" link="/page2"><li>Webpage 2</li></a>
                </ul>
            </nav>
        </header>
    ...
    </div>
    ```



# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/consistent-navigation
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/failures/F66