---
id: 32
title: Exercise 3.6 - Consistent Identification
slug: consistent-identification
---


# Exercise-Area

The following website's remove buttons are not consistent across all pages, making the functionality they offer confusing for all users.
Both the delete and the remove buttons offer the same functionality for different entities on the website.

# Requirements-Area

-   Change the "delete" button's text and icon to be the same across the website.

    ```js
    const firstButton = document.querySelector('button').innerText;
    const firstIcon = document.querySelector('button > i').className.trim();


    const link = document.querySelectorAll("a[link]")[1];
    link.href = 'about:blank#'; // TODO
    link.click();

    const secondButton = document.querySelector('button').innerText;
    const secondIcon = document.querySelector('button > i').className.trim();
    

    return { correct: firstButton === secondButton && firstIcon === secondIcon };
    ```


# Hidden-Code-Area

```html
<style>
    .nav-links {
        list-style-type: none;
        display: flex;
        padding: 0;
        margin: 0;
        flex-wrap: wrap;
    }

    .nav-links li {
        border: 1px solid white;
        padding: 16px 4px;
        border-radius: 4px;
        white-space: nowrap;
    }

    .nav-links > * {
        margin-right: 8px;
    }

    body {
        margin: 0;
    }

    header {
        padding: 8px 16px;
        background-color: black;
    }

    a {
        color: #7CB1FF;
    }

    @media (min-height: 250px) {
        header {
            position: sticky;
            top: 0;
        }
    }

    label {
        display: block;
    }

    #content {
        margin: 8px;
    }

    .main-content {
        display: flex;
        align-items: center;
        justify-content: center;
        height: 450px;
        border: 1px solid black;
    }

    .active {
        background-color: rgba(255, 255, 255, 0.3);
    }

    .card {
        border: 1px solid var(--contrast-color);
        padding: 8px;
        display: flex;
    }

    .placeholder-image {
        width: 100px;
        height: 100px;
        border: 1px solid var(--contrast-color);
        font-size: 24px;
        align-items: center;
        justify-content: center;
        flex-shrink: 0;
        display: inline-flex;
    }

    .content {
        margin: 0 8px;
    }

    h2 {
        margin: 0;
    }

    .actions {
        margin-left: auto;
        display: flex;
        flex-direction: column;
        justify-content: flex-end;
    }
</style>
<script>
    window.addEventListener("load", () => {
        main();
    });

    function main() {
        const container = document.getElementById('/products').parentElement;
        const pages = {
            '/products': document.getElementById('/products'),
            '/customers': document.getElementById('/customers')
        };
        Array.from(document.querySelectorAll('a[link]')).forEach(link => {
            link.addEventListener('click', () => {
                const targetPage = link.getAttribute('link');
                loadPage(targetPage);
            });
        });

        function loadPage(page) {
            for(const availPage of Object.keys(pages)) {
                if(availPage !== page) {
                    pages[availPage].remove();
                }
            }
            container.appendChild(pages[page]);
        }

        loadPage('/products');
    }

    if(document.readyState === "complete") {
        main();
    }
</script>
```

# Code-Area

```html
<!-- products -->
<div id="/products">
    <header>
        <nav>
            <ul class="nav-links">
                <a href="#" class="active" link="/products"><li>Products</li></a>
                <a href="#" link="/customers"><li>Customers</li></a>
            </ul>
        </nav>
    </header>

    <main id="content">
        <div class="card">
            <div class="placeholder-image" role="img" aria-label="placeholder image">100x100</div>
            <div class="content">
                <h2>Product 1</h2>
                <p>Product description</p>
            </div>
            <div class="actions">
                <button>Remove <i class="fas fa-trash-alt"></i></button>
            </div>
        </div>
    </main>
</div>

<!-- customers -->
<div id="/customers">
    <header>
        <nav>
            <ul class="nav-links">
                <a href="#" link="/products"><li>Products</li></a>
                <a href="#" class="active" link="/customers"><li>Customers</li></a>
            </ul>
        </nav>
    </header>

    <main id="content">
        <div class="card">
            <div class="placeholder-image" role="img" aria-label="placeholder image">100x100</div>
            <div class="content">
                <h2>Customer 1</h2>
                <p>Customer description</p>
            </div>
            <div class="actions">
                <button>Delete <i class="fas fa-user-minus"></i></button>
            </div>
        </div>
    </main>
</div>


```

# Walkthrough-Area

-
    Use the same button on both pages:
    ```html
    <!-- products -->
    <button>Remove <i class="fas fa-trash-alt"></i></button>
    ...
    <!-- customers -->
    ...
    <button>Remove <i class="fas fa-trash-alt"></i></button>
    ```



# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/consistent-identification
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/failures/F31