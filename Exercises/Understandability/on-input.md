---
id: 30
title: Exercise 3.4 - On Input
slug: on-input
---


# Exercise-Area

A website's multi-page register form moves to the next step as soon as a value from the dropdown is selected. Blind users and users with hand tremors can easily make a mistake when selecting an item on the dropdown menu. Nonetheless, they are directed to the next step with no way to correct their mistake.

# Requirements-Area

-   When the value of the input changes, the form is not submitted.

    ```c#
    var genderSelect = await page.QuerySelectorAsync("select");
    await genderSelect.SelectAsync("Male");
    var isOnFirstStep = await page.EvaluateExpressionAsync<bool>(
        "document.querySelector('h1').innerText.toLowerCase().includes('step 1')"
    );
    
    return isOnFirstStep;
    ```

-   There is a button to submit the gender.
    ```c#
    var submitButton = await page.QuerySelectorAsync("button, input[type='submit']");
    if(submitButton == null) return false;
    var genderSelect = await page.QuerySelectorAsync("select");
    await genderSelect.SelectAsync("Male");
    await submitButton.ClickAsync();
    var isOnSecondStep = await page.EvaluateExpressionAsync<bool>(
        "document.querySelector('h1').innerText.toLowerCase().includes('step 2')"
    );
    
    return isOnSecondStep;
    ```



# Hidden-Code-Area

```html
<style>
.form {
    display: grid;
    grid-template-columns: 0fr 1fr;
    column-gap: 8px;
    row-gap: 8px;
}

label {
    white-space: nowrap;
}

input {
    max-width: 140px;
}
</style>

<script>
    window.addEventListener("load", () => {
        main();
    }); 

    function main() {
        const form = document.getElementById("form");
        form.addEventListener("submit", (e) => {
            const gender = document.querySelector('select').value;
            form.innerHTML = `
                <h1>Register Step 2</h1>
                <p>Selected gender: ${gender}</p>
                <div class="form">
                    <label for="4">Password:</label>
                    <input id="4" type="password" autocomplete="new-password">

                    <label for="5">Confirm password:</label>
                    <input id="5" type="password" autocomplete="new-password">
                </div>
            `;
            e.preventDefault();
        });
    }


    if(document.readyState === "complete") {
        main();
    }
</script>
```

# Code-Area

```html
<form id="form">
    <h1>Register Step 1</h1>
    <label for="gender">Gender:</label>
    <select required id="gender" name="gender">
        <option disabled selected value> -- select an option -- </option>
        <option>Male</option>
        <option>Female</option>
        <option>Other</option>
    </select>
</form>
<script>
    // Remove all the code below
    const gender = document.querySelector('select');
    gender.addEventListener('change', () => {
        const submitEvent = document.createEvent("Event");
        submitEvent.initEvent("submit", true, true);
        document.getElementById('form').dispatchEvent(submitEvent);
    });
</script>
```

# Walkthrough-Area
-   # Remove all the JavaScript code
    Remove the javascript that submits the form when the select input changes. The JavaScript editor should be empty.

-   # Add a submit button
    Add a simple submit button after the select input:
    ```html
    <form id="form">
        ...
        <button type="submit">Submit</button>
    </form>
    ```


# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/on-input
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/failures/F36
- More on multi-page forms: https://www.w3.org/WAI/tutorials/forms/multi-page/