---
id: 28
title: Exercise 3.2 - Language of Parts
slug: language-of-parts
---


# Exercise-Area

The previous website has an article about the word amen. The start of the article includes the word in different languages but the language is not programmatically determinable, hindering the performance of screen readers.

List of accepted languages: [ISO 639-2 codes](https://en.wikipedia.org/wiki/List_of_ISO_639-2_codes)

# Requirements-Area

- 
    Specify the language of the Hebrew word.
    ```js
    const foreignWords = document.querySelectorAll('*[lang]:not(html)');
    for(const word of foreignWords) {
        if(word.innerText.includes('אָמֵן') && word.innerText.length <= 8) {
            if(word.lang === 'he' || word.lang === 'heb') {
                return { correct: true };
            }
            return { correct: false };
        }
    }
    return { correct: false };
    ```

- 
    Specify the language of the Greek word. Make sure to use the ancient type!
    ```js
    const foreignWords = document.querySelectorAll('*[lang]:not(html)');
    for(const word of foreignWords) {
        if(word.innerText.includes('ἀμήν') && word.innerText.length <= 6) {
            if(word.lang === 'grc') {
                return { correct: true };
            }
            return { correct: false };
        }
    }
    return { correct: false };
    ```

- 
    Specify the language of the Arabic word.
    ```js
    const foreignWords = document.querySelectorAll('*[lang]:not(html)');
    for(const word of foreignWords) {
        if(word.innerText.includes('آمین‎') && word.innerText.length <= 7) {
            if(word.lang === 'ara' || word.lang === 'ar') {
                return { correct: true };
            }
            return { correct: false };
        }
    }
    return { correct: false };
    ```

- 
    Specify the language of the Classical Syriac word.
    ```js
    const foreignWords = document.querySelectorAll('*[lang]:not(html)');
    for(const word of foreignWords) {
        if(word.innerText.includes('ܐܡܝܢ‎') && word.innerText.length <= 7) {
            if(word.lang === 'syc') {
                return { correct: true };
            }
            return { correct: false };
        }
    }
    return { correct: false };
    ```

# Hidden-Code-Area
```html
<style>
    body.dark a {
        color: #7CB1FF;
    }
    a:-webkit-any-link {
        color: -webkit-link;
    }
    a:-webkit-any-link:active {
        color: -webkit-activelink;
    }
</style>
```

# Code-Area

```html
<html lang="en">
    <head>
        <title>Amen</title>
    </head>
    <body>
        <main>
            <h1>
                Amen
            </h1>
            <p>
                Amen (Hebrew: אָמֵן‎; Ancient Greek: ἀμήν; Arabic: آمین‎; Classical Syriac: ܐܡܝܢ‎) is an Abrahamic declaration of affirmation first found in the Hebrew Bible, and subsequently in the New Testament. It is used in Jewish, Christian and Islamic worship, as a concluding word, or as a response to a prayer.
            </p>
            <a href="#" aria-label="read more about amen">Read more</a>
        </main>
    </body>
</html>
```

# Walkthrough-Area
- 
    Add the correct language for every non-english word:

    ```html
    <html lang="en">
        ...
        <p>
            Amen (Hebrew: <span lang="he">אָמֵן</span>‎;
            Ancient Greek: <span lang="grc">ἀμήν</span>;
            Arabic: <span lang="ar">آمین‎</span>;
            Classical Syriac: <span lang="syc">ܐܡܝܢ‎</span>) 
            is an Abrahamic declaration of affirmation first found in the Hebrew Bible,
            and subsequently in the New Testament. It is used in Jewish,
            Christian and Islamic worship, as a concluding word,
            or as a response to a prayer.
        </p>
        ...
    </html>
    ```


# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/language-of-parts
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/html/H58