---
id: 29
title: Exercise 3.3 - On Focus
slug: on-focus
---


# Exercise-Area

A website's register form offers additional information about the password field. When focusing the help button, the website changes the focus into the ***I understood*** button, confusing screen reader users and making tabbing through the password field into the new password field nearly impossible.

# Requirements-Area

-   When the help button gets focus, no change of context happens.  
    You can either remove the autofocus attribute or completely remove the button.

    ```c#
    var element = await page.QuerySelectorAsync("button");
    await page.Mouse.ClickAsync(0, 0); // Required due to a chrome bug
    await page.EvaluateExpressionAsync<bool>("document.querySelector('button.btn-secondary').focus()");
    var isFocused = await page.EvaluateExpressionAsync<bool>(
        "document.activeElement === document.querySelector('button.btn-secondary')"
    );

    return isFocused;
    ```

# Hidden-Code-Area

```html
<style>

form {
    display: grid;
    grid-template-columns: 0fr 1fr;
    column-gap: 8px;
    row-gap: 8px;
}

label {
    white-space: nowrap;
}

input {
    max-width: 140px;
}

body {
    margin: 8px !important;
}

i {
    font-size: 1.25rem;
}

body.dark .popover {
    background-color: black;
}

body.dark .popover-header {
    background-color: black;
}

body.dark .popover-body {
    color: white;
}

</style>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>
<script>
    window.addEventListener("load", () => {
        main();
    }); 

    function main() {
        var popoverTriggerList = Array.from(
            document.querySelectorAll('[data-bs-toggle="popover"]')
        );
        var popoverList = popoverTriggerList.map((popoverTriggerEl) => {
            return new bootstrap.Popover(popoverTriggerEl, {
                trigger: 'manual',
                sanitize: false,
                html: true,
            });
        });
        popoverTriggerList.forEach((popoverTriggerEl) => {
            var popover = bootstrap.Popover.getInstance(popoverTriggerEl);
            popoverTriggerEl.addEventListener('mouseenter', () => {
                popover.show();
                document.querySelector('.popover').addEventListener('mouseleave', () => {
                    if(
                        !popoverTriggerEl.parentElement.querySelector(
                            `${popoverTriggerEl.tagName}:hover`
                        )
                    ) {
                        popover.hide();
                    }
                });
            });
    
            popoverTriggerEl.addEventListener('mouseleave', () => {
                if(!document.querySelector('.popover:hover')) {
                    popover.hide();
                }
            });
    
            popoverTriggerEl.addEventListener('focus', () => {
                popover.show();
            });
    
            popoverTriggerEl.addEventListener('inserted.bs.popover', () => {
                var understoodBtn = document.getElementById("understood");
                if(understoodBtn) {
                    understoodBtn.addEventListener('click', () => {
                        popover.hide();
                    });
                    if(understoodBtn.autofocus) {
                        understoodBtn.focus();
                    }
                } else {
                    popoverTriggerEl.addEventListener('blur', () => {
                        popover.hide();
                    });
                }
            });
        });
    
        
        window.addEventListener('keydown', (event) => {
            if(event.code === 'Escape') {
                popoverTriggerList.forEach(popoverTriggerEl => {
                    bootstrap.Popover.getInstance(popoverTriggerEl).hide();
                });
            }
        });
    }


    if(document.readyState === "complete") {
        main();
    }
</script>
```

# Code-Area

```html
<h1>Register for a new account</h1>
<form>
    <label for="1">Username:</label>
    <input id="1" type="text" autocomplete="username">

    <label for="2">Email:</label>
    <input id="2" type="text" autocomplete="email">

    <label for="3">Full name:</label>
    <input id="3" type="text" autocomplete="name">

    <label for="4">Password:</label>
    <span>
        <input id="4" type="password" autocomplete="new-password">
        <button
                type="button"
                class="btn btn-secondary"
                data-keyboard="true" 
                data-bs-toggle="popover"
                data-bs-placement="right"
                data-bs-title="Password Requirements"
                data-bs-content="Password must have at least 8 characters and one uppercase letter. <br> <button autofocus id='understood'>I Understood</button>"
            >
            <i class="far fa-question-circle"></i>
        </button>
    </span>
    <label for="5">Confirm password:</label>
    <input id="5" type="password" autocomplete="new-password">
</form>
```

# Walkthrough-Area
-   
    Remove the `autofocus` attribute from the button of the content that gets displayed on hover:
    ```html
    <button
        type="button"
        class="btn btn-secondary"
        data-keyboard="true" 
        data-bs-toggle="popover"
        data-bs-placement="right"
        data-bs-title="Password Requirements"
        data-bs-content="Password must have at least 8 characters and one uppercase letter. <br> <button id='understood'>I Understood</button>"
    >
        <i class="far fa-question-circle"></i>
    </button>
    ```


# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/on-focus
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/general/G107