---
id: 27
title: Exercise 3.1 - Language of Page
slug: language-of-page
---


# Exercise-Area

The website with the articles does not specify the language of the document, hindering the performance of screen readers.

List of accepted languages: [ISO 639-2 codes](https://en.wikipedia.org/wiki/List_of_ISO_639-2_codes)
# Requirements-Area

- 
    Specify the language of the page.
    ```js
    const languageOfPage = document.querySelector("html").lang;
    return { correct: languageOfPage === "en" || languageOfPage === "eng" };
    ```

# Hidden-Code-Area
```html
<style>
    body.dark a {
        color: #7CB1FF;
    }
    a:-webkit-any-link {
        color: -webkit-link;
    }
    a:-webkit-any-link:active {
        color: -webkit-activelink;
    }
</style>
```

# Code-Area

```html
<html>
    <head>
        <title>Lorem ipsum explanation</title>
    </head>
    <body>
        <main>
            <h1>
                Lorem ipsum explanation
            </h1>
            <p>
                In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.
            </p>
            <a href="#" aria-label="read more about lorem ipsum">Read more</a>
        </main>
    </body>
</html>
```

# Walkthrough-Area
- 
    Add the english language as the default language of the website:

    ```html
    <html lang="en">
        ...
    </html>
    ```


# Learning-Material-Area
- WCAG: https://www.w3.org/WAI/WCAG21/Understanding/language-of-page
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/html/H57