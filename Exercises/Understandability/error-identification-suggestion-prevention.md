---
id: 33
title: Exercise 3.7 - Error Identification, Suggestion and Prevention
slug: error-identification-suggestion-and-prevention
---


# Exercise-Area

This exercise is not yet implemented. Submit to pass

# Requirements-Area

-   Submit to pass

    ```js    
    return { correct: true };
    ```


# Hidden-Code-Area

```html
```

# Code-Area

```html
```

# Walkthrough-Area

- Submit to pass



# Learning-Material-Area
- WCAG:
    - https://www.w3.org/WAI/WCAG21/Understanding/error-identification
    - https://www.w3.org/WAI/WCAG21/Understanding/labels-or-instructions
    - https://www.w3.org/WAI/WCAG21/Understanding/error-suggestion
    - https://www.w3.org/WAI/WCAG21/Understanding/error-prevention-legal-financial-data
- Related Technique: https://www.w3.org/WAI/WCAG21/Techniques/failures/F66