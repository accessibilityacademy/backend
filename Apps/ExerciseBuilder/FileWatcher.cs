using System;
using System.IO;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AccessibilityAcademy.Apps.ExerciseBuilder
{

    class FileWatcher
    {

        private FileSystemWatcher Watcher;

        public FileWatcher(string folderPath)
        {
                // Create a new FileSystemWatcher and set its properties
                Watcher = new FileSystemWatcher(folderPath, "*.md");


                Watcher.IncludeSubdirectories = true;

                // Watch for changes in LastWrite time
                Watcher.NotifyFilter = NotifyFilters.LastWrite;


                // Add event handler
                // IObservable<string> fileChangedObservable = Observable
                //     .FromEventPattern<FileSystemEventArgs>(
                //         Watcher,
                //         "Changed"
                //     )
                //     .Select(x => x.EventArgs.FullPath)
                //     .Throttle(TimeSpan.FromMilliseconds(500));

                // https://stackoverflow.com/a/53627167/7868639
                IObservable<string> fileChangedObservable = Observable
                    .FromEventPattern<FileSystemEventArgs>(
                        Watcher,
                        "Changed"
                    )
                    .Delay(TimeSpan.FromMilliseconds(100)) // Prevent race conditions since the events fire before file has completed saving to disk
                    .Select(x => 
                    {
                        return new {
                            Value = x.EventArgs.FullPath,
                            Time = DateTime.Now,
                            Keep = true
                        };
                    })
                    .Scan((acc, cur) => 
                    {
                        var diff = cur.Time - acc.Time;
                        var isSame = acc.Value == cur.Value;

                        return diff.TotalMilliseconds > 100 || !isSame ?
                            new {
                                Value = cur.Value,
                                Time = cur.Time,
                                Keep = true
                            } :
                            new {
                                Value = acc.Value,
                                Time = acc.Time,
                                Keep = false
                            };
                    })
                    .Where(x => x.Keep)
                    .Select(x => x.Value);

                fileChangedObservable.Subscribe(filePath => 
                {
                    var handler = FileChanged;
                    if(handler != null) 
                    {
                        handler(this, filePath);
                    }
                });
                
        }

        // Blocks thread and starts watching
        public void Run() 
        {
                // Begin watching
                Watcher.EnableRaisingEvents = true;
                Thread.Sleep(Timeout.Infinite);
        }

        public event EventHandler<string> FileChanged;
    }
}
