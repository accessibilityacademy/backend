using YamlDotNet.Serialization;

namespace AccessibilityAcademy.Apps.ExerciseBuilder
{
    class ExerciseFrontMatter
    {
        public ExerciseFrontMatter()
        {

        }

        [YamlMember(Alias = "id")]
        public int Id { get; set; }
        [YamlMember(Alias = "title")]
        public string Title { get; set; }
        [YamlMember(Alias = "slug")]
        public string Slug { get; set; }

    }
}
