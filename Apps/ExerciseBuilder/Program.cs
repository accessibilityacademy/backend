using System;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
//using Microsoft.Toolkit.Parsers.Markdown;
//using Microsoft.Toolkit.Parsers.Markdown.Blocks;
//using Microsoft.Toolkit.Parsers.Markdown.Inlines;
using System.Collections.Generic;

using Markdig;
using Markdig.Syntax;
using Markdig.Extensions.Yaml;
using YamlDotNet.Core;
using YamlDotNet.Serialization;
using Markdig.Syntax.Inlines;
using AccessibilityAcademy.Models;
using AccessibilityAcademy.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;

namespace AccessibilityAcademy.Apps.ExerciseBuilder
{
    class Program
    {
        static async Task Main(string[] args)
        {
            if(args.Count() == 0)
            {
                Console.WriteLine(@"Accessibility Academy Exercise Builder Helper Tool

Usage dotnet eb [command] [options]

Commands:
    watch
    run

Options:
    --commit      Stores all parsed markdown documents to the database.
    --input       The directory that includes the exercises.
                ");
                System.Environment.Exit(0);
            }

            var commit = false;
            var inputFolderPath = ".";

            for(int i = 1; i < args.Count(); i++)
            {
                if(args[i] == "--commit") 
                {
                    commit = true;
                } 
                else if(args[i] == "--input") 
                {
                    if(i + 1 == args.Count() || args[i + 1][0] == '-')
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"Required argument missing for option: {args[i]}");
                        Console.ForegroundColor = ConsoleColor.Gray;
                        System.Environment.Exit(0);
                    }
                    inputFolderPath = args[i + 1];
                    i++;
                }
                else 
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"Unrecognized option '{args[i]}'");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    System.Environment.Exit(0);
                }
            }

            if(args[0] == "watch")
            {
                WatchFolder(inputFolderPath, commit);
            }
            else if(args[0] == "run")
            {
                await ReadAllFiles(inputFolderPath, commit);

            }
            else {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Unrecognized command '{args[0]}'");
                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }

        static void WatchFolder(string folderPath, bool commit)
        {
            var config = new ConfigurationBuilder().AddEnvironmentVariables().Build();
            var watcher = new FileWatcher(folderPath);
            watcher.FileChanged += async (object s, string filePath) => {
                var exercise = await ParseFile(filePath);
                if(exercise == null)
                {
                    return;
                }
                if(commit)
                {
                    using(var dbContext = new DatabaseContext(config))
                    {
                        await StageExercise(dbContext, exercise);
                        await dbContext.SaveChangesAsync();
                        Console.WriteLine(filePath + " exercise successfully stored in the database!");
                    }
                }
            };
            watcher.Run();
        }

        static async Task ReadAllFiles(string folderPath, bool commit)
        {
            Func<string, IEnumerable<string>> ProcessDirectory = null;
            ProcessDirectory = delegate (string targetDirectory)
            {
                // Process the list of files found in the directory.
                IEnumerable<string> fileEntries = Directory.GetFiles(targetDirectory, "*.md");
                

                // Recurse into subdirectories of this directory.
                string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
                foreach(string subdirectory in subdirectoryEntries)
                {
                    fileEntries = fileEntries.Concat(ProcessDirectory(subdirectory));
                }
                return fileEntries;
            };

            string[] files = ProcessDirectory(folderPath).ToArray();

            var config = new ConfigurationBuilder().AddEnvironmentVariables().Build();
            using(var dbContext = new DatabaseContext(config))
            {
                foreach(string file in files) 
                {
                    var exercise = await ParseFile(file);
                    if(exercise == null)
                    {
                        continue;
                    }
                    if(commit)
                    {
                        await StageExercise(dbContext, exercise);
                    }
                }
                if(commit)
                {
                    await dbContext.SaveChangesAsync();
                }
            }
        }


        static async Task<Exercise> ParseFile(string filePath) 
        {   
            string[] rawMarkdown; 
            try {
                rawMarkdown = await File.ReadAllLinesAsync(filePath);

            } 
            catch(Exception)
            {
                Console.WriteLine($"Unable to read file {filePath}. I/O Exception thrown");
                return null;
            }

            var pipeline = new MarkdownPipelineBuilder().UseYamlFrontMatter().Build();
            MarkdownDocument document = Markdown.Parse(rawMarkdown.Aggregate("", (acc, x) => acc + x + '\n'), pipeline);

            var exercise = new Exercise();

            if(document.Count == 0)
            {
                return null; // Do not try to do anything with empty files
            }

            var errored = false;
            
            if(!(document[0] is YamlFrontMatterBlock))
            {
                PrintError(filePath, document[0], "Missing required yaml header");
                errored = true;
            }
            else
            {
                var yamlBlock = (YamlFrontMatterBlock) document[0];

                var yaml = yamlBlock.Lines.Lines
                    .Select(x => x.ToString() + '\n')
                    .Where(x => !string.IsNullOrWhiteSpace(x))
                    .Aggregate((acc, x) => acc + x);

                var yamlDeserializer = new DeserializerBuilder()
                    .IgnoreUnmatchedProperties()
                    .Build();

                ExerciseFrontMatter attributes;
                try 
                {
                    attributes = yamlDeserializer.Deserialize<ExerciseFrontMatter>(yaml);
                    if(attributes.Id < 0) 
                    {
                        PrintError(filePath, null, "Invalid header. id must be positive");
                        errored = true;
                    }
                    exercise.Id = attributes.Id;
                    exercise.Title = attributes.Title;
                    exercise.Slug = attributes.Slug;
                }
                catch (YamlException)
                {
                    PrintError(filePath, null, "Invalid header. Do you miss an integer id, title or a slug?");
                    errored = true;
                }
            }

            
            var exerciseArea = document.FirstOrDefault(x =>
                x is HeadingBlock && 
                ((HeadingBlock) x).Level == 1 &&
                ((LiteralInline)((HeadingBlock) x).Inline.FirstChild).Content.Match("Exercise-Area")
            );

            if (exerciseArea == null)
            {
                PrintError(
                    filePath,
                    null,
                    "Exercise-Area is required. Is it a heading 1 like this: '# Exercise-Area'?"
                );
                errored = true;
            }

            var requirementsArea = document.FirstOrDefault(x =>
                x is HeadingBlock && 
                ((HeadingBlock) x).Level == 1 &&
                ((LiteralInline)((HeadingBlock) x).Inline.FirstChild).Content.Match("Requirements-Area")
            );

            if (requirementsArea == null)
            {
                PrintError(filePath, null, "Requirements-Area is required. Is it a heading 1 like this: '# Requirements-Area'?");
                errored = true;
            }
            else if(exerciseArea != null) 
            {
                var rawMarkdownExerciseArea = rawMarkdown
                    .Skip(exerciseArea.Line + 1)
                    .Take(requirementsArea.Line - 1 - exerciseArea.Line); // @TODO find better way to find next element line


                exercise.Body = Markdown.Normalize(rawMarkdownExerciseArea.Aggregate("", (acc, x) => acc + x + '\n'));

                Block requirementsListBlock = null;
                if(requirementsArea != document.Last()) 
                {
                    requirementsListBlock = document[document.IndexOf(requirementsArea) + 1];
                }

                if(requirementsListBlock == null || !(requirementsListBlock is ListBlock))
                {
                    PrintError(
                        filePath,
                        requirementsListBlock != null ? requirementsListBlock : requirementsArea,
                        "Requirements-Area content must be a single list"
                    );
                    errored = true;
                }
                else
                {

                    List<ExerciseRequirement> requirements = new List<ExerciseRequirement>();

                    foreach(ListItemBlock requirementBlock in (ListBlock) requirementsListBlock)
                    {
                        if(
                            !(requirementBlock.LastChild is FencedCodeBlock) ||
                            ((FencedCodeBlock) requirementBlock.LastChild).Info != "js" &&
                            ((FencedCodeBlock) requirementBlock.LastChild).Info != "c#"
                        )
                        {
                            PrintError(
                                filePath,
                                requirementBlock.LastChild,
                                "A requirement in the requirements list must end with a js or c# code block"
                            );
                            errored = true;
                        }
                        else
                        {
                            if(requirementBlock.Count() == 1)
                            {
                                PrintError(filePath, requirementBlock, "Requirements list cannot be without content");
                                errored = true;
                                continue;
                            }

                            var rawMarkdownRequirementArea = rawMarkdown
                                .Skip(requirementBlock.First().Line)              
                                .Take((requirementBlock.LastChild.Line - 1) - (requirementBlock.First().Line - 1))
                                .Select((x, i) => 
                                {
                                    if(i == 0) 
                                    {
                                        return x.Substring(requirementBlock.First().Column);
                                    }
                                    if(
                                        x.Count() >= 4 && !String.IsNullOrWhiteSpace(x.Substring(0, 4)) ||
                                        x.Count() > 0 && x.Count() < 4 && !String.IsNullOrWhiteSpace(x)
                                    )
                                    {
                                        var j = 0;
                                        for(j = 0; j < requirementBlock.Count; j++)
                                        {
                                            if(requirementBlock[j].Line >= requirementBlock.First().Line + i)
                                            {
                                                break;
                                            }
                                        }
                                        j--;
                                        PrintError(filePath, requirementBlock[j], "Requirements list content must be indented with 4 spaces or a tab");
                                        errored = true;
                                    }
                                    else if(x.Count() >= 4)
                                    {
                                        return x.Substring(4);
                                    }
                                    return x;
                                })
                                .ToArray();

                            requirements.Add(new ExerciseRequirement()
                            {
                                CompletionChecker = ((FencedCodeBlock) requirementBlock.LastChild).Lines.ToString(),
                                CompletionCheckerLanguage = ((FencedCodeBlock) requirementBlock.LastChild).Info,
                                Requirement = Markdown.Normalize(rawMarkdownRequirementArea.Aggregate("", (acc, x) => acc + x + '\n'))
                            });
                        }
                    }
                    exercise.Requirements = requirements;
                }
            }

            var hiddenCodeArea = document.FirstOrDefault(x =>
                x is HeadingBlock && 
                ((HeadingBlock) x).Level == 1 &&
                ((LiteralInline)((HeadingBlock) x).Inline.FirstChild).Content.Match("Hidden-Code-Area")
            );
            
            if (hiddenCodeArea == null)
            {
                PrintError(
                    filePath,
                    null,
                    "Hidden-Code-Area is required. Is it a heading 1 like this: '# Hidden-Code-Area'?"
                );
                errored = true;
            }
            else 
            {    
                Block hiddenCodeBlock = null;
                if(hiddenCodeArea != document.Last()) 
                {
                    hiddenCodeBlock = document[document.IndexOf(hiddenCodeArea) + 1];
                }

                if(hiddenCodeBlock == null || !(hiddenCodeBlock is FencedCodeBlock) || ((FencedCodeBlock)hiddenCodeBlock).Info != "html")
                {
                    PrintError(
                        filePath,
                        hiddenCodeBlock != null ? hiddenCodeBlock : hiddenCodeArea,
                        "Hidden-Code-Area content must be a single html code block"
                    );
                    errored = true;
                }
                else
                {
                    exercise.HiddenCode = ((FencedCodeBlock)hiddenCodeBlock).Lines.ToString();
                }
            }


            var initialCodeArea = document.FirstOrDefault(x =>
                x is HeadingBlock && 
                ((HeadingBlock) x).Level == 1 &&
                ((LiteralInline)((HeadingBlock) x).Inline.FirstChild).Content.Match("Code-Area")
            );
            
            if (initialCodeArea == null)
            {
                PrintError(
                    filePath,
                    null,
                    "Code-Area is required. Is it a heading 1 like this: '# Code-Area'?"
                );
                return null; // Can't continue to find any more errors
            }

            Block initialCodeBlock = null;
            if(initialCodeArea != document.Last()) 
            {
                initialCodeBlock = document[document.IndexOf(initialCodeArea) + 1];
            }

            if(initialCodeBlock == null || !(initialCodeBlock is FencedCodeBlock) || ((FencedCodeBlock)initialCodeBlock).Info != "html")
            {
                PrintError(
                    filePath,
                    initialCodeBlock != null ? initialCodeBlock : initialCodeArea,
                    "Code-Area content must be a single html code block"
                );
                errored = true;
            }
            else
            {
                exercise.Code = ((FencedCodeBlock)initialCodeBlock).Lines.ToString();
            }

            var walkthroughArea = document.FirstOrDefault(x =>
                x is HeadingBlock && 
                ((HeadingBlock) x).Level == 1 &&
                ((LiteralInline)((HeadingBlock) x).Inline.FirstChild).Content.Match("Walkthrough-Area")
            );

            var learningMaterialArea = document.FirstOrDefault(x =>
                x is HeadingBlock && 
                ((HeadingBlock) x).Level == 1 &&
                ((LiteralInline)((HeadingBlock) x).Inline.FirstChild).Content.Match("Learning-Material-Area")
            );

            if (learningMaterialArea == null)
            {
                PrintError(filePath, null, "Learning-Material-Area is required. Is it a heading 1 like this: '# Learning-Material-Area'?");
                errored = true;
            }

            if (walkthroughArea == null)
            {
                PrintError(filePath, null, "Walkthrough-Area is required. Is it a heading 1 like this: '# Walkthrough-Area'?");
                errored = true;
            } 
            else if(learningMaterialArea != null)
            {

                var rawMarkdownLearningMaterialArea = rawMarkdown
                    .Skip(learningMaterialArea.Line + 1);
                    // .Take(nextArea.Line - 1 - learningMaterialArea.Line); // @TODO find better way to find next element line

                exercise.LearningMaterial = Markdown.Normalize(rawMarkdownLearningMaterialArea.Aggregate("", (acc, x) => acc + x + '\n'));

                Block walkthroughStepsBlock = null;
                if(walkthroughArea != document.Last()) 
                {
                    walkthroughStepsBlock = document[document.IndexOf(walkthroughArea) + 1];
                }

                if(walkthroughStepsBlock == null || !(walkthroughStepsBlock is ListBlock))
                {
                    PrintError(
                        filePath,
                        walkthroughStepsBlock != null ? walkthroughStepsBlock : walkthroughArea,
                        "Walkthrough-Area content must be a single list"
                    );
                    errored = true;
                }
                else
                {
                    List<WalkthroughStep> steps = new List<WalkthroughStep>();

                    int position = 0;
                    foreach(ListItemBlock walkthroughStepBlock in (ListBlock) walkthroughStepsBlock)
                    {
                        var stepTitleBlock = walkthroughStepBlock.FirstOrDefault();
                        string title = null;
                        
                        if(
                            stepTitleBlock is HeadingBlock &&
                            ((HeadingBlock) stepTitleBlock).Level == 1
                        )
                        {
                            if(((HeadingBlock) stepTitleBlock).Inline.Count() > 1) 
                            {
                                PrintError(filePath, stepTitleBlock, "Walkthrough title formatting is not supported");
                            } else
                            {
                                title = ((LiteralInline)((HeadingBlock) stepTitleBlock).Inline.FirstChild).Content.ToString();
                            }
                        }
                        else
                        {
                            stepTitleBlock = null;
                        }

                        IEnumerable<string> rawMarkdownWalkthroughStepArea = rawMarkdown;

                        int nextElementLine = 0;

                        if(position == ((ListBlock)walkthroughStepsBlock).Count() - 1)
                        {
                            nextElementLine = learningMaterialArea.Line; // @TODO find better way to find next element line
                        }
                        else
                        {
                            nextElementLine = ((ListBlock)walkthroughStepsBlock).ElementAt(position + 1).Line;
                        }

                        if(walkthroughStepBlock.Count() == 0)
                        {
                            PrintError(filePath, walkthroughStepBlock, "Walkthrough steps list cannot be without content");
                            errored = true;
                            continue;
                        }

                        if(stepTitleBlock != null)
                        {
                            if(walkthroughStepBlock.Count() < 2)
                            {
                                PrintError(filePath, walkthroughStepBlock[0], "Walkthrough steps list content cannot be a title without content");
                                errored = true;
                                continue;
                            }
                            else
                            {
                                rawMarkdownWalkthroughStepArea = rawMarkdownWalkthroughStepArea
                                    .Skip(walkthroughStepBlock[1].Line)
                                    .Take(nextElementLine - 1 - (walkthroughStepBlock[1].Line));
                            }
                        }
                        else
                        {
                            rawMarkdownWalkthroughStepArea = rawMarkdownWalkthroughStepArea
                                .Skip(walkthroughStepBlock.First().Line)
                                .Take(nextElementLine - 1 - (walkthroughStepBlock.First().Line - 1));
                        }

                        rawMarkdownWalkthroughStepArea = rawMarkdownWalkthroughStepArea            
                            .Select((x, i) => 
                            {
                                if(i == 0) 
                                {
                                    return x.Substring(walkthroughStepBlock.First().Column);
                                }
                                else if(
                                    x.Count() >= 4 && !String.IsNullOrWhiteSpace(x.Substring(0, 4)) ||
                                    x.Count() > 0 && x.Count() < 4 && !String.IsNullOrWhiteSpace(x)
                                )
                                {
                                    Console.WriteLine(x.Substring(0, 4));
                                    // Console.WriteLine(x.Count() >= 4 && !String.IsNullOrWhiteSpace(x.Substring(0, 4)));
                                    // Console.WriteLine(x.Count() > 0 && x.Count() < 4 && !String.IsNullOrWhiteSpace(x));
                                    var j = 0;
                                    for(j = 0; j < walkthroughStepBlock.Count; j++)
                                    {
                                        if(walkthroughStepBlock[j].Line >= walkthroughStepBlock.First().Line + i)
                                        {
                                            break;
                                        }
                                    }
                                    j--;
                                    PrintError(filePath, walkthroughStepBlock[j], "Walkthrough steps list content must be indented with 4 spaces or a tab");
                                    errored = true;
                                }
                                else if(x.Count() >= 4)
                                {
                                    return x.Substring(4);
                                }
                                return x;
                            });

                        steps.Add(new WalkthroughStep()
                        {
                            Title = title,
                            Step = Markdown.Normalize(rawMarkdownWalkthroughStepArea.Aggregate("", (acc, x) => acc + x + '\n'))
                        });

                        position++;
                    }
                    exercise.WalkthroughSteps = steps;
                }
            }


            if(errored) return null;
            return exercise;
        }


        static async Task StageExercise(DatabaseContext dbContext, Exercise exercise)
        {
            var existingExercise = await dbContext.Exercises
                .Include(x => x.Requirements)
                .Include(x => x.WalkthroughSteps)
                .FirstOrDefaultAsync(x => x.Id == exercise.Id);

            if(existingExercise == null)
            {
                dbContext.Exercises.Add(exercise);
            }
            else
            {
                existingExercise.Body = exercise.Body;
                existingExercise.Title = exercise.Title;
                existingExercise.Slug = exercise.Slug;
                existingExercise.Code = exercise.Code;
                existingExercise.HiddenCode = exercise.HiddenCode;
                existingExercise.LearningMaterial = exercise.LearningMaterial;

                var existingRequirements = existingExercise.Requirements.ToList();
                existingExercise.Requirements = exercise.Requirements.Select((x, i) =>
                {
                    if(i < existingRequirements.Count())
                    {
                        existingRequirements[i].Requirement = x.Requirement;
                        existingRequirements[i].CompletionChecker = x.CompletionChecker;
                        existingRequirements[i].CompletionCheckerLanguage = x.CompletionCheckerLanguage;
                        return existingRequirements[i];
                    }
                    return x;
                }).ToList();

                var existingWalkthroughSteps = existingExercise.WalkthroughSteps.ToList();
                existingExercise.WalkthroughSteps = exercise.WalkthroughSteps.Select((x, i) => 
                {
                    if(i < existingWalkthroughSteps.Count())
                    {
                        existingWalkthroughSteps[i].Step = x.Step;
                        existingWalkthroughSteps[i].Title = x.Title;
                        return existingWalkthroughSteps[i];
                    }
                    return x;
                }).ToList();

                dbContext.Exercises.Update(existingExercise);
            }
        }

        static void PrintError(string filePath, MarkdownObject invalidNode, string message)
        {
            if(invalidNode != null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Error at: {filePath}:{invalidNode.Line + 1}:{invalidNode.Column + 1}");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Error at: {filePath}");
            }
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine($"    {message}");
        }
    }
}
