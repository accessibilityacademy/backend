#!/bin/bash

chown -R user:user /app

exec su user --session-command="$@"