class AccessibilityElement {
    constructor(acc) {
        Object.assign(this, acc);
    }

    forEach(callback) {
        const stack = [this];
        while(stack.length !== 0) {
            const element = stack.pop();
            stack.push(...element.children.slice().reverse());
            callback(element);
        }
    }

    find(predicate) {
        const stack = [this];
        while(stack.length !== 0) {
            const element = stack.pop();
            stack.push(...element.children.slice().reverse());
            if(predicate(element)) {
                return element;
            }
        }
    }

    findAll(predicate) {
        const results = [];
        const stack = [this];
        while(stack.length !== 0) {
            const element = stack.pop();
            stack.push(...element.children.slice().reverse());
            if(predicate(element)) {
                results.push(element);
            }
        }
        return results;
    }

    isParentOf(childElement) {
        const stack = [this];
        while(stack.length !== 0) {
            const element = stack.pop();
            stack.push(...element.children.slice().reverse());
            if(element === childElement) {
                return true;
            }
        }
        return false;
    }

    isAbove(compareWith) {

        if(!compareWith) {
            return false;
        }

        let root = this;
        while(root.parent !== null) {
            root = root.parent;
        }

        let preOrderList = [];
        
        root.forEach((element) => {
            preOrderList.push(element);
        });

        return preOrderList.indexOf(this) < preOrderList.indexOf(compareWith);
    }
}


rootAccNode.parent = null;
const acc = new AccessibilityElement(rootAccNode);
const stack = [acc];
while(stack.length !== 0) {
    const element = stack.pop();
    for(let i = 0; i < element.children.length; i++) {
        element.children[i].parent = element;
        element.children[i] = new AccessibilityElement(element.children[i]);
    }
    stack.push(...element.children);
}


// https://stackoverflow.com/a/9733420/7868639
function luminance(r, g, b) {
    var a = [r, g, b].map(function (v) {
        v /= 255;
        return v <= 0.03928
            ? v / 12.92
            : Math.pow( (v + 0.055) / 1.055, 2.4 );
    });
    return a[0] * 0.2126 + a[1] * 0.7152 + a[2] * 0.0722;
}

function contrast(rgb1, rgb2) {
    var lum1 = luminance(rgb1[0], rgb1[1], rgb1[2]);
    var lum2 = luminance(rgb2[0], rgb2[1], rgb2[2]);
    var brightest = Math.max(lum1, lum2);
    var darkest = Math.min(lum1, lum2);
    return (brightest + 0.05)
            / (darkest + 0.05);
}