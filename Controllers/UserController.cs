using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AccessibilityAcademy.Data;
using AccessibilityAcademy.DTOs;
using AccessibilityAcademy.Models;
using AccessibilityAcademy.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AccessibilityAcademy.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly UserService _userService;

        public UserController(ILogger<UserController> logger, UserService userService)
        {
            _logger = logger;
            _userService = userService;
        }

        [HttpPost]
        public async Task<ActionResult> RegisterUser(UserRegistration user)
        {
            await _userService.CreateOne(user);
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Email, user.Email)
            };
            await HttpContext.SignInAsync(new ClaimsPrincipal(new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme)), new AuthenticationProperties
            {
                IsPersistent = true,
                ExpiresUtc = DateTimeOffset.UtcNow.AddMonths(2)
            });
            return Ok();
        }

        [HttpGet]
        public async Task<ActionResult<DTOs.User>> Login([FromQuery] string email)
        {
            var user = await _userService.GetOneByEmail(email);
            if(user != null)
            {
                var claims = new List<Claim>()
                {
                    new Claim(ClaimTypes.Email, email)
                };
                await HttpContext.SignInAsync(new ClaimsPrincipal(new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme)), new AuthenticationProperties
                {
                    IsPersistent = true,
                    ExpiresUtc = DateTimeOffset.UtcNow.AddMonths(2)
                });
            }
            return Ok(user);
        }

        [HttpDelete]
        public async Task<ActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return Ok();
        }
    }
}
