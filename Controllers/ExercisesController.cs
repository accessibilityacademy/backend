﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AccessibilityAcademy.Models;
using AccessibilityAcademy.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AccessibilityAcademy.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ExercisesController : ControllerBase
    {
        private readonly ILogger<ExercisesController> _logger;
        private readonly ExerciseService _exerciseService;
        private readonly UserService _userService;

        public ExercisesController(ILogger<ExercisesController> logger, ExerciseService exerciseService, UserService userService)
        {
            _logger = logger;
            _exerciseService = exerciseService;
            _userService = userService;
        }

        [HttpGet]
        public async Task<ActionResult<IList<DTOs.Exercise>>> GetAllExercises()
        {
            var exercises = await _exerciseService.GetAllExercises();
            return Ok(exercises);
        }

        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<DTOs.Exercise>> GetExercise(int id)
        {
            var exercise = await _exerciseService.GetOneById(id);
            if(exercise == null) {
                return NotFound();
            }
            await _userService.AnalyticsLoadExercise(HttpContext.User.FindFirstValue(ClaimTypes.Email), id);
            return Ok(exercise);
        }

        [HttpGet("{id}/walkthrough")]
        [Authorize]
        public async Task<ActionResult<IList<WalkthroughStep>>> GetExerciseWalkthrough(int id)
        {
            var walkthroughSteps = await _exerciseService.GetWalkthroughByExerciseId(id);
            if(walkthroughSteps == null) {
                return NotFound();
            }
            await _userService.AnalyticsWalkthrough(HttpContext.User.FindFirstValue(ClaimTypes.Email), id);
            return Ok(walkthroughSteps);
        }

        [HttpPost("{id}/answer")]
        [Authorize]
        public async Task<ActionResult<DTOs.AnswerResult>> SubmitExercise(int id, [FromBody] DTOs.UserAnswer answer)
        {
            if(!await _exerciseService.Exists(id))
            {
                return NotFound(new { Message = "Exercise not found" });
            }
            var answerResult = await _exerciseService.CheckAnswer(HttpContext.User.FindFirstValue(ClaimTypes.Email), id, answer.Body);
            if(answerResult == null)
            {
                return BadRequest(new { Message = "Answer validation timed out" });
            }
            return Ok(answerResult);
        }

        [HttpPost("{id}/feedback")]
        [Authorize]
        public async Task<ActionResult<DTOs.AnswerResult>> SubmitFeedback(int id, [FromBody] DTOs.UserFeedback feedback)
        {
            Console.WriteLine(feedback.EditorState);
            if(!await _exerciseService.Exists(id))
            {
                return NotFound(new { Message = "Exercise not found" });
            }
            await _userService.Feedback(HttpContext.User.FindFirstValue(ClaimTypes.Email), id, feedback);
            return Ok();
        }
    }
}
