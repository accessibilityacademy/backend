using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AccessibilityAcademy.Models;
using AccessibilityAcademy.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AccessibilityAcademy.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CoursesController : ControllerBase
    {

        private readonly ILogger<CoursesController> _logger;
        private readonly CourseService _courseService;

        public CoursesController(ILogger<CoursesController> logger, CourseService courseService)
        {
            _logger = logger;
            _courseService = courseService;
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IList<DTOs.Course>>> GetAllCourses()
        {
            var courses = await _courseService.GetAllCourses(HttpContext.User.FindFirstValue(ClaimTypes.Email));
            return Ok(courses);
        }

        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<DTOs.CourseDetails>> GetCourse(int id)
        {
            var course = await _courseService.GetOneById(HttpContext.User.FindFirstValue(ClaimTypes.Email), id);
            if(course == null) {
                return NotFound();
            }
            await _courseService.StartCourse(HttpContext.User.FindFirstValue(ClaimTypes.Email), id);
            return Ok(course);
        }
    }
}
