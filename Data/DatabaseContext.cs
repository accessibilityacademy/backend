using System;
using AccessibilityAcademy.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using Microsoft.AspNetCore.DataProtection.EntityFrameworkCore;

namespace AccessibilityAcademy.Data {
    public class DatabaseContext : DbContext, IDataProtectionKeyContext 
    { 

        private IConfiguration Configuration { get; }

        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<ExerciseAnswer> Answers { get; set; }
        public DbSet<ExerciseRequirement> ExerciseRequirement { get; set; }
        public DbSet<Course> Courses { get; set; }

        public DbSet<User> Users { get; set; }
        public DbSet<Analytics> Analytics { get; set; }
        public DbSet<Feedback> Feedback { get; set; }
        public DbSet<DataProtectionKey> DataProtectionKeys { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options, IConfiguration configuration): base(options)
        {
            this.Configuration = configuration;
        }

        public DatabaseContext(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Exercise>()
                .HasMany(e => e.Requirements)
                .WithOne()
                .IsRequired();

            modelBuilder.Entity<ExerciseAnswer>()
                .HasMany(a => a.RequirementResults)
                .WithOne(r => r.Answer)
                .IsRequired();

            modelBuilder.Entity<ExerciseAnswer>()
                .HasOne<Exercise>(a => a.Exercise)
                .WithMany()
                .IsRequired();

            modelBuilder.Entity<RequirementResult>()
                .HasOne(r => r.Requirement)
                .WithMany()
                .HasForeignKey(r => r.RequirementId)
                .IsRequired();

            modelBuilder.Entity<Course>()
                .HasMany(c => c.Exercises)
                .WithMany(e => e.Courses)
                .UsingEntity<CourseExercise>(
                    x => x
                        .HasOne(x => x.Exercise)
                        .WithMany(x => x.ExerciseCourses)
                        .HasForeignKey(x => x.ExerciseId),
                    x => x
                        .HasOne(x => x.Course)
                        .WithMany(x => x.CourseExercises)
                        .HasForeignKey(x => x.CourseId)
                );

            modelBuilder.Entity<Analytics>()
                .HasOne(a => a.User)
                .WithMany(u => u.Analytics);

            modelBuilder.Entity<ExerciseAnswer>()
                .HasOne(a => a.User)
                .WithMany(u => u.Answers);

            modelBuilder.Entity<User>()
                .HasIndex(u => u.Email)
                .IsUnique();

            modelBuilder.Entity<Feedback>()
                .HasOne(f => f.User)
                .WithMany();

            modelBuilder.Entity<User>()
                .HasMany(u => u.UserCourses)
                .WithOne(uc => uc.User);
            
            modelBuilder.Entity<UserCourse>()
                .HasOne(uc => uc.Course)
                .WithMany(c => c.UserCourses);

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if(!optionsBuilder.IsConfigured) // Used by the secondary apps
            {
                string databaseHost = Configuration["DB_HOST"];
                if(databaseHost == null) 
                {
                    databaseHost = "mysql";
                }
                optionsBuilder.UseMySql(
                    $"server={databaseHost};user=root;password={Configuration["DB_PASSWORD"]};database={Configuration["DB_DATABASE"]}",
                    new MySqlServerVersion(new Version(8, 0, 22))
                )
                .EnableSensitiveDataLogging()
                .EnableDetailedErrors();
            }
        }
    }
}